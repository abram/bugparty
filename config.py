

from ConfigParser import SafeConfigParser
import logging, os


class Config(object):
    '''
    A singleton configuration class.
    getInstance() will not make a new instance, it will only get the
    singleton instance. A new instance must be created using the
    add_args() and build() static methods.

    The config is assembled from three sets of data, which take precedence
    in this order:
        command-line args > config file values > defaults

    Eg. --workdir will override [bugparty]workdir from the config file.

    config parameters can be accessed with the values member, which
    is a ConfigParser instance.
    '''

    _instance = None
    #,'fti_prefix': 'http://%(couchdb_port_5984_tcp_addr)s:5984/{project}/_fti/_design/'

    DEFAULTS = {
        'workdir': '/var/lib/bugparty'
        ,'bugparty_source_dir': os.path.dirname(os.path.abspath(__file__))
        ,'couchdb_port_5984_tcp_addr': \
            os.getenv('COUCHDB_PORT_5984_TCP_ADDR', 'localhost')
        ,'es_port_9200_tcp_addr': \
            os.getenv('ES_PORT_9200_TCP_ADDR', 'localhost')
        ,'fti_prefix': 'http://%(couchdb_port_5984_tcp_addr)s:5984/_fti/local/{project}/_design/'
        ,'fastrep_folder': '/csvfiles'
        ,'fastrep_root': '%(bugparty_source_dir)s/../fastrep'
        ,'bug_deduper_root': '%(bugparty_source_dir)s/../bug-deduplication'
        ,'vowpal_wabbit_run': '%(bugparty_source_dir)s/vowpal_wabbit/vowpalwabbit/vw'
        ,'logfile': '%(workdir)s/logs/bugparty.log'
        ,'loglevel': 'INFO'
        ,'lda_low_word_threshold': 2
        ,'lda_high_word_threshold': 0.95
    }

    def __init__(self, args, fp, init_logging=True):
        '''fp = filepath or file pointer'''
        self.parser = SafeConfigParser(Config.DEFAULTS)
        self.parser.add_section('bugparty')

        if type(fp) is str:
            self.parser.read(fp)
        else:
            self.parser.readfp(fp)

        if args and args.workdir:
            self.parser.set('bugparty', 'workdir', args.workdir)
        if args and args.loglevel:
            self.parser.set('bugparty', 'loglevel', args.loglevel)
        if args and args.logfile:
            self.parser.set('bugparty', 'logfile', args.logfile)

        self.values = self.parser

        if init_logging:
            logging.basicConfig(
                    level=self.values.get('bugparty', 'loglevel'),
                    filename=self.values.get('bugparty', 'logfile'),
                    format='%(levelname)s %(asctime)s :  %(message)s')

    @property
    def workdir(self):
        return self.get('workdir')

    def workdir_path(self, *parts):
        return os.path.join(self.workdir, *parts)

    def get(self, key):
        return self.values.get('bugparty', key)

    def path(self, key):
        return os.path.abspath(os.path.expanduser(self.get(key)))

    @staticmethod
    def add_args(argparser):
        argparser.add_argument('--ini', action='store',
                help='path to .ini configuration file',
                default='bugparty.ini')
        argparser.add_argument('--workdir', action='store', default=None)
        argparser.add_argument('--logfile', action='store', default=None)
        argparser.add_argument('--loglevel', action='store', default=None)

    @staticmethod
    def build(args, fp=None, init_logging=True):
        Config._instance = Config(args, fp or args.ini, init_logging)
        return Config._instance

    @staticmethod
    def getInstance():
        assert Config._instance
        return Config._instance

    @staticmethod
    def clearInstance():
        '''clears the singleton instance.'''
        Config._instance = None
