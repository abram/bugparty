#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
This file implements a task queue and queue worker, backed by a database.

When TaskQueue is started, it will attempt to read through the DB and
recover any tasks that are recorded there.

To create a task, you should subclass Task.

If a task fails with an exception, the exception will be logged to the
errors document in the task queue database.

The basic flow of a task is
::
                    TaskQueue
                       ---------------> new QueueWorker
                       |                        |
        TaskMaker      |                        |
            |          |                        |
            --------->[ ]                       |
            |add_task [ ]------->DBQueue        |
            |         [ ]-add_task>[ ]          |
            |         [ ]<---------[ ]          |
            |         [ ]--async add_task----->[ ]
            |<--------[ ]           |          [ ]<-> run_task
                                   [ ]<--------[ ]
                                   [ ]  remove [ ]
                                   [ ]  task   [ ]
                                   [ ]-------->[ ]
'''


import datetime, json, logging, traceback
from multiprocessing import Process, Queue, Lock

from databases import ElasticSearch


class DBQueue(object):
    def __init__(self, db_lock, doc_prefix):
        self.db = ElasticSearch() \
                .connect_to_bugparty_db(ElasticSearch.TASKS)
        self.db_lock = db_lock

        self.DOC_NAME = doc_prefix + 'queue'
        self.ERROR_DOC_NAME = doc_prefix + 'errors'

    def register_error(self, task, traceback):
        prepared = task.for_json()
        now = datetime.datetime.now()

        with self.db_lock:
            errors = self.db.get(self.ERROR_DOC_NAME, {})
            errors.setdefault('errors', []).insert(0, {
                'task': prepared
                ,'traceback': traceback
                ,'when': str(now)
            })
            self.db[self.ERROR_DOC_NAME] = errors

    def get_errors(self):
        with self.db_lock:
            return self.db.get(self.ERROR_DOC_NAME, {}).get('errors', [])

    def add_task(self, task):
        prepared = task.for_json()
        prepared['added'] = str(datetime.datetime.now())

        with self.db_lock:
            q = self._get_tasks_doc()
            q['tasks'].append(prepared)
            self.db[self.DOC_NAME] = q

    def starting_task(self):
        with self.db_lock:
            q = self._get_tasks_doc()
            q['tasks'][0]['started'] = str(datetime.datetime.now())
            self.db[self.DOC_NAME] = q

    def task_done(self):
        with self.db_lock:
            q = self._get_tasks_doc()
            q['tasks'] = q['tasks'][1:]
            self.db[self.DOC_NAME] = q

    def get_tasks(self):
        return self._get_tasks_doc()['tasks']

    def set_tasks(self, tasks):
        with self.db_lock:
            q = self._get_tasks_doc()
            q['tasks'] = [task.for_json() for task in tasks]
            self.db[self.DOC_NAME] = q

    def clear_errors(self):
        with self.db_lock:
            errors = self.db.get(self.ERROR_DOC_NAME, {})
            errors['errors'] = []
            self.db[self.ERROR_DOC_NAME] = errors

    def _get_tasks_doc(self):
        return self.db.get(self.DOC_NAME, {'tasks': []})

class QueueWorker(object):
    '''runs stuff on a thread'''

    def __init__(self, q, db_lock, doc_prefix):
        self.q = q
        self.db = DBQueue(db_lock, doc_prefix)
        self.finished = False

    def run(self):
        while not self.finished:
            task = self.q.get()
            # TODO: write this as a context manager
            try:
                self.db.starting_task()
                task.run(self)
                print 'task complete ("{}")'.format(task.name)
            except Exception as e:
                tb = traceback.format_exc()
                logging.error('task failed %s %s', task.name, tb)
                print tb
                self.db.register_error(task, tb)
            self.db.task_done()

    def finish(self):
        self.finished = True


class TaskQueue(object):
    '''Represents a task queue which:
        * runs tasks in a separate process (using multiprocessing)
        * saves task information in the database to avoid problems with
            starts/stops leading to incomplete tasks
    '''

    class MetaTask(type):
        ''' metaclass for task with recoverability info'''
        tasks_by_name = {}

        def __new__(cls, name, bases, attrs):
            attrs.setdefault('name', name)
            attrs.setdefault('recoverable', False)
            attrs.setdefault('recover', lambda x: None)
            return super(TaskQueue.MetaTask, cls).__new__(cls, name, bases,
                                                        attrs)
        def __init__(self, name, bases, attrs):
            super(TaskQueue.MetaTask, self).__init__(name, bases, attrs)
            TaskQueue.MetaTask.tasks_by_name[self.name] = self


    def __init__(self, doc_prefix=''):
        self.q = Queue()
        self.db_lock = Lock()
        self.doc_prefix = doc_prefix
        self.db = DBQueue(self.db_lock, doc_prefix)

        tasks = self.db.get_tasks()
        recovered_tasks = [self.recover_task(task) for task in tasks]
        for i in range(len(tasks)):
            if recovered_tasks[i] is None:
                logging.error('unrecoverable task lost: ' +
                        json.dumps(tasks[i]))
            else:
                logging.info('task recovered: ' + json.dumps(tasks[i]))

        recovered_tasks = filter(None, recovered_tasks)
        self.db.set_tasks(recovered_tasks)
        for task in recovered_tasks:
            self.q.put(task)

    def add_task(self, task):
        self.db.add_task(task)
        self.q.put(task)

    def get_tasks(self):
        return self.db.get_tasks()

    def get_errors(self):
        return self.db.get_errors()

    def clear_errors(self):
        self.db.clear_errors()

    def clear_tasks(self):
        self.db.set_tasks([])

    def run(self):
        def start_worker(q, db_lock):
            worker = QueueWorker(q, db_lock, self.doc_prefix)
            worker.run()

        self.worker_process = Process(target=start_worker,
                                args=[self.q, self.db_lock])
        self.worker_process.daemon = True
        self.worker_process.start()

    def join(self):
        '''wait until all tasks have run'''
        self.add_task(FinishTask())
        self.worker_process.join()

    @staticmethod
    def recover_task(task):
        task = json.loads(task) if type(task) is str else task
        task_class = TaskQueue.MetaTask.tasks_by_name[task['task_name']]

        if task_class.recoverable:
            try:
                return task_class.recover(task.get('properties', {}))
            except:
                return None
        return None


class Task(object):
    '''inherit your tasks from this class'''
    __metaclass__ = TaskQueue.MetaTask

    def for_json(self):
        if self.recoverable:
            properties = self.__dict__.copy()
        else:
            properties = {}
        return {
            'task_name': self.__class__.name
            ,'properties': properties
        }


class FinishTask(Task):
    ''' task that ends a worker queue '''
    recoverable = False

    def run(self, worker):
        worker.finish()


class FunctionTask(Task):
    '''task that runs a function on the worker queue'''
    def __init__(self, f):
        self.f = f

    def run(self, worker):
        self.f()
