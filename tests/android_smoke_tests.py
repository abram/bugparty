#  Copyright (C) 2014 Alex Wilson
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import json, shlex, subprocess, unittest, urllib2, uuid
from nose.plugins.attrib import attr
from tests.test_utils import *

from config import Config
from databases import ElasticSearch, ESType
from bug import Bug
from lda_loader import LDATask
import elasticsearch

import common, project, urls, time

class AnrdoidMongrelSmokeTests(unittest.TestCase):

    PROJECT = 'android'
    DATA_FILE_PATH = '../util/android.json'  # Generalize

    @classmethod
    def setUpClass(cls):
        fn = os.path.join(os.path.dirname(__file__), cls.DATA_FILE_PATH)
        with open(fn) as json_file:
            android_bugs = json.load(json_file)
            for bug in android_bugs:
                bug_data = bug
                bug_id = str(bug_data['bugid'])
                Bug.clean_bug_data(bug_data)
        cls.docs_sample = android_bugs

        load_test_config()

    def setUp(self):
        load_test_config()
        self.project = project.Project(self.PROJECT)
        self.client = ElasticSearch(self.project.name)

        self.projects_ctrl = project.ProjectsController()
        self.projects_ctrl.delete_project(self.PROJECT)
        self.projects_ctrl.create_project(self.PROJECT)

        db = self.client.connect_to_db()
        for bug in self.docs_sample:
            db.update({ bug['bugid'] : bug })
        self.client.refresh()

        self.bugparty_url = 'http://localhost:8080/bugparty/android'
        self.topics_url = 'http://localhost:8080/bugparty/android/topics'

    def tearDown(self):
        load_test_config()
        self.projects_ctrl.delete_project(self.PROJECT)

    @attr(smoke=True, require_server=True, android=True)
    def test_search_title(self):
        result = self.bugparty('search_title', 'android')
        assert len(result) > 0
        assert 'bugid' in result[0]
        assert 'score' in result[0]

    @attr(smoke=True, require_server=True, android=True)
    def test_search_text(self):
        result = self.bugparty('search_text', 'keyboard')
        assert len(result) > 0
        assert 'bugid' in result[0]
        assert 'title_score' in result[0]
        assert 'desc_score' in result[0]

    @attr(smoke=True, require_server=True, android=True)
    def test_similar(self):
        # ldat = LDATask(self.project, 100)
        # ldat.run()
        LDATask(self.PROJECT, 100).run()
        result = self.topics('similar', '9999')
        assert len(result) > 0
        assert result[0]['bugid'] == '9999'
        assert result[0]['distance'] == 0.0

    def topics(self, action, data):
        return self.http_get_json('/'.join([self.topics_url, action, data]))

    def bugparty(self, action, data):
        return self.http_get_json('/'.join([self.bugparty_url, action, data]))

    @staticmethod
    def http_get_json(url):
        return json.loads(urllib2.urlopen(url).read())
