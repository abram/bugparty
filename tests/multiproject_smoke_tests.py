#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import json, shlex, subprocess, unittest, urllib2, uuid

from config import Config
import databases, urls
from project import Project, ProjectsController
from tests.test_utils import  *

import requests
from nose.plugins.attrib import attr


class DatabasesSmokeTest(unittest.TestCase):
    def setUp(self):
        self.project = 'bugparty_smoke_test'
	load_test_config()
        self.client = databases.ElasticSearch(self.project)

        self.projects = ProjectsController()
        self.projects.delete_project(self.project)
        self.projects.create_project(self.project)

        self.bug = RandomBugFactory(self.client).create_bugs(1)[0]
        self.bug_id = str(self.bug['bugid'])
        self.client.refresh() # add bug to search results

    def tearDown(self):
        self.projects.delete_project(self.project)

    @attr(smoke=True, require_server=True)
    def test_options_methods(self):
        url = 'http://localhost:8080' + urls.bugs(self.project)
        response = requests.options(url)
        self.assertEqual(response.status_code, 200)
        methods = response.headers['Access-Control-Allow-Methods'].split(', ')
        self.assertEqual(set(methods), {'GET', 'POST'})

    @attr(smoke=True)
    def test_create_database(self):
        self.client.client.indices.exists(self.project)
        self.client.client.indices.exists_type(self.project,
                                        databases.ElasticSearch.BUGS)
        self.client.client.indices.exists_type(self.project,
                                        databases.ElasticSearch.TOPICS)

    @attr(smoke=True, require_server=True)
    def test_list_projects(self):
        projects = self.http_get_json('http://localhost:8080/bugparty/projects')
        assert len(projects) > 0
        assert self.project in {p['name'] for p in projects}

    @attr(smoke=True, require_server=True)
    def test_delete_project(self):
        self.projects.delete_project(self.project)

        projects = self.http_get_json('http://localhost:8080/bugparty/projects')
        assert self.project not in {p['name'] for p in projects}

    @attr(smoke=True, require_server=True)
    def test_bug_queries(self):
        data = self.http_get_json(
            'http://localhost:8080/bugparty/{}/similar_title/{}'
            .format(self.project, self.bug_id)
        )
        self.assertEqual(len(data), 1)

        data = self.http_get_json(
            'http://localhost:8080/bugparty/{}/similar_description/{}'
            .format(self.project, self.bug_id)
        )
        self.assertEqual(len(data), 1)

    @attr(smoke=True, require_server=True)
    def test_display_bug(self):
        for url_part in ['bugs', 'display']:
            data = self.http_get_json(
                'http://localhost:8080/bugparty/{}/{}/{}'
                .format(self.project, url_part, self.bug_id)
            )
            self.assertEqual(data['title'], self.bug['title'])
            self.assertEqual(data['description'], self.bug['description'])
    
    @staticmethod
    def http_get_json(url):
        response = urllib2.urlopen(url).read()
        return json.loads(response)
