#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from config import Config
from databases import ElasticSearch
from project import ProjectsController
from topics_controller import TopicsController
from custom_topics import CustomTopicTask

import unittest, shlex, subprocess
from nose.plugins.attrib import attr

from tests.test_utils import *


class CustomTopicsTests(unittest.TestCase):
    PROJECT = 'bugparty_testing'

    def setUp(self):
        load_test_config()

        ElasticMock.clear()
        clientMock = ElasticMock()
        self.db = clientMock.connect_to_db()
        self.topics_db = clientMock.connect_to_topics_db()
        self.controller = TopicsController(clientMock)
        
        self.topic_name = 'test topic'
        self.topic_words = 'test topic words and so forth'.split()

    @attr(unit=True)
    def testCreateTopicIDs(self):
        '''gives each topic a unique, sequential id'''
        id = self.controller.create_topic(self.topic_name, self.topic_words)
        assert type(id) == int
        assert self.controller.create_topic('other one', ['such']) == id + 1

    @attr(unit=True)
    def testCreateAndRetrieveTopic(self):
        '''can create and retrieve a topic'''
        id = self.controller.create_topic(self.topic_name, self.topic_words)
        topic = self.controller.get_topic(id)

        assert self.topic_name == topic['name']
        assert self.topic_words == topic['words'].split()

    @attr(unit=True)
    def testCanGetAllTopicWords(self):
        '''we can get the words of every topic'''
        names = 'a b c d e f g h i j k l m n o p'.split()
        for i in range(len(names)):
            words = [str(i)]
            words.extend(names)
            self.controller.create_topic(names[i], words=words)

        topics = self.controller.get_topics()
        assert len(topics) == len(names)
        for x in zip(topics, names): print x
        for i in range(len(topics)):
            self.assertEquals(topics[i]['topic_id'], i)
            self.assertEquals(topics[i]['name'], names[i])
            self.assertEquals(topics[i]['words'], str(i) +' '+' '.join(names))

    @attr(unit=True)
    def testCanRenameTopics(self):
        id = self.controller.create_topic(self.topic_name, self.topic_words)
        self.controller.rename_topic(id, 'new name')
        assert self.controller.get_topic(id)['name'] == 'new name'


class CustomTopicsSmokeTests(unittest.TestCase):
    PROJECT = 'bugparty_testing'

    def setUp(self):
	load_test_config()
        self.client = ElasticSearch(self.PROJECT)
        ProjectsController().delete_project(self.PROJECT)
        ProjectsController().create_project(self.PROJECT)

        bug_db = self.client.connect_to_db()
        self.bug = RandomBugFactory(self.client).create_bugs(150)[3]
        self.client.refresh()

    def tearDown(self):
        ProjectsController().delete_project(self.PROJECT)

    @attr(smoke=True)
    def testTopicScoring(self):
        topicsController = TopicsController(self.client)
        topic_id = topicsController.create_topic('example', self.bug['title'])

        CustomTopicTask(self.PROJECT, topic_id).run()
        bug_id = self.bug['bugid'].encode('utf-8')
        print bug_id
        bug = self.client.connect_to_db()[bug_id]
        score = bug['topics'][str(topic_id)]
        self.assertNotEqual(score, 0)
