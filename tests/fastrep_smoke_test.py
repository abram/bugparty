#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import common, project, urls, time

from fastrep_handler import BugDedupTask, FastREPTask, InsertSimsTask
from config import Config
from databases import ElasticSearch, ESType
from tests.test_utils import *

import itertools, json, shlex, subprocess, unittest, urllib2, uuid
from multiprocessing import Process
from nose.plugins.attrib import attr
import requests


class FastREPSmokeTests(unittest.TestCase):
    PROJECT = 'bugparty_testing'

    @classmethod
    def setUpClass(cls):
        cls.docs_sample = RandomBugFactory(None).create_bugs(20)
        cls.dupe = cls.docs_sample[0].copy()

        cls.dupe_id = cls.dupe['bugid'] + 'dupe'
        cls.dupe['_id'] = cls.dupe_id
        cls.dupe['bugid'] = cls.dupe_id

        load_test_config()

    def setUp(self):
        load_test_config()
        self.project = project.Project(self.PROJECT)
        self.client = ElasticSearch(self.project.name)

        self.projects_ctrl = project.ProjectsController()
        self.projects_ctrl.delete_project(self.PROJECT)
        self.projects_ctrl.create_project(self.PROJECT)

        self.client.connect_to_db().update({
                bug['bugid'] : bug
            for bug in self.docs_sample })
        self.client.refresh()

    def add_dupe(self):
        self.client.connect_to_db()[self.dupe_id] = self.dupe
        self.client.refresh()

    def tearDown(self):
        load_test_config()
        self.projects_ctrl.delete_project(self.PROJECT)

    @attr(smoke=True, require_server=True)
    def test_redo_dupes(self):
        self.add_dupe()
        BugDedupTask(self.PROJECT, incremental=False).run(None)
        self.client.refresh()

        self.check_is_match(self.docs_sample[0]['bugid'], self.dupe_id)

    @attr(smoke=True, require_server=True)
    def test_update_dupes(self):
        BugDedupTask(self.PROJECT, incremental=False).run(None)
        self.client.refresh()

        self.add_dupe()
        BugDedupTask(self.PROJECT, incremental=True).run(None)
        self.client.refresh()

        self.check_is_match(self.docs_sample[0]['bugid'], self.dupe_id)


    @attr(smoke=True, require_server=True)
    def test_matches(self):
        # setUp
        similarities = RandomBugFactory(None).create_similarities(5)
        self.client.connect_to_sim_db().update({
                sim['id1'] + '_' + sim['id2'] : sim 
            for sim in similarities })
        self.client.refresh()

        url = 'http://localhost:8080/bugparty/%s/fastrep/matches/' %self.PROJECT
        response = requests.get(url)

        print response
        assert response.ok

    @attr(smoke=True, require_server=True)
    def test_unknown_context(self):
        """tests that bugparty can handle when a similarity is missing
            the context"""

        # make one fake_ctx similarity so that ES doesn't die
        #   (which is a different error)
        similarity = {
            'id1': 'wow',
            'id2': 'cool',
            'fake_ctx': 10
        }
        self.client.connect_to_sim_db().update({'wow_cool': similarity})
        self.client.refresh()        

        bugid = self.docs_sample[0]['bugid']
        url = urls.fastrep_matches(self.PROJECT, bugid, 'fake_ctx', '1')
        response = requests.get('http://localhost:8080' + url)
        assert response.ok

    def check_is_match(self, id1, id2, ctx='rep'):
        url = 'http://localhost:8080' + urls.fastrep_matches(self.PROJECT, id1, ctx, '1')

        response = requests.get(url)
        assert response.ok

        bugs = response.json()
        bugids = {b['bugid'] for b in bugs}
        assert id2 in bugids
