#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import json, os, tempfile, unittest
from multiprocessing import Manager

from nose.plugins.attrib import attr

from http import HTTPError
from task_handler import TaskHandler
from task_queue import FunctionTask, TaskQueue, Task
from tests.test_utils import RequestMock, load_test_config

import custom_topics, lda_loader


class DoubleTask(Task):
    name = 'DoubleIntegerTask'
    recoverable = False

    def __init__(self, num):
        self.num = num

    def run(self, worker):
        self.num.value *= 2

class SubtractTask(Task):
    name = 'DoubleIntegerTask'
    recoverable = False

    def __init__(self, num, sub):
        self.num = num
        self.sub = sub

    def run(self, worker):
        self.num.value -= self.sub

class PIDRecordingTask(Task):
    name = 'DoubleIntegerTask'
    recoverable = False

    def __init__(self, store):
        self.store = store

    def run(self, worker):
        self.store.value = os.getpid()

def troublesome_function():
    raise Exception('I HATE BACKGROUND PROCESSING!')


class RecoverableTestTask(Task):
    recoverable = True

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    @staticmethod
    def recover(data):
        return RecoverableTestTask(*data['args'], **data['kwargs'])

    def run(self, worker):
        pass


class WriteToFileTask(Task):
    recoverable = True

    def __init__(self, msg, filename):
        self.msg = msg
        self.filename = filename

    @staticmethod
    def recover(data):
        return WriteToFileTask(data['msg'], data['filename'])

    def run(self, worker):
        with open(self.filename, 'w') as f:
            f.write(self.msg)


class TaskQueueTests(unittest.TestCase):
    def setUp(self):
	load_test_config()
        self.q = TaskQueue(doc_prefix='auto-tests-')
        self.q.db.set_tasks([])
        self.manager = Manager()

    def tearDown(self):
        self.q.db.set_tasks([])
        self.q.db.clear_errors()

    def run_tasks(self, *tasks):
        self.q.run()
        for task in tasks:
            self.q.add_task(task)
        self.q.join()

    def test_add_task(self):
        num = self.manager.Value(int, 54)
        self.run_tasks(DoubleTask(num))
        self.assertEqual(num.value, 54 * 2)

    def test_tasks_run_multiprocessed(self):
        num = self.manager.Value(int, 0)
        self.run_tasks(PIDRecordingTask(num))
        assert num.value != 0
        assert os.getpid() != num.value

    def test_tasks_run_in_order(self):
        num = self.manager.Value(int, 20)
        self.run_tasks(SubtractTask(num, 20), DoubleTask(num))

        assert num.value == 0

    def test_tasks_can_raise(self):
        num = self.manager.Value(int, 20)

        self.run_tasks(FunctionTask(troublesome_function),
                        SubtractTask(num, 20))
        assert num.value == 0

    def test_task_dumping(self):
        def id(a, *args, **kwargs):
            return a

        task = RecoverableTestTask('test', 'dumping', cool='stuff')
        dumped = task.for_json()['properties']

        assert 'args' in dumped
        assert 'kwargs' in dumped
        assert dumped['args'] == task.args
        assert dumped['kwargs'] == task.kwargs

    def test_task_recovery(self):
        task = RecoverableTestTask('test', 'recovery')
        recovered = TaskQueue.recover_task(
                        json.dumps(task.for_json(), skipkeys=True))

        assert isinstance(recovered, RecoverableTestTask)
        assert recovered.args == ('test', 'recovery')

    @attr(unit=True)
    def test_lda_recovery(self):
        self.check_recovery(lda_loader.LDATask, topic_count=100,
                        project='bugparty-testing')

    @attr(unit=True)
    def test_lda_incremental_recovery(self):
        self.check_recovery(lda_loader.LDAIncrementalTask,
                        project='bugparty-testing')

    @attr(unit=True)
    def test_custom_topics_recovery(self):
        self.check_recovery(custom_topics.CustomTopicTask, topic_id=4,
                        project='bugparty-testing')

    @staticmethod
    def check_recovery(task_cls, **kwargs):
        print 'test ' + str(task_cls)
        task = task_cls(**kwargs)
        task_json = json.dumps(task.for_json(), skipkeys=True)
        recovered = TaskQueue.recover_task(task_json)

        assert isinstance(recovered, task_cls) 
        for (name, val) in kwargs.items():
            assert getattr(recovered, name) == val

    def test_db_recovery(self):
        with tempfile.NamedTemporaryFile() as tempf:
            message = 'resumed task!!!!'
            task = WriteToFileTask(message, tempf.name)

            # add the task but don't start it
            self.q.add_task(task)

            # now make another queue that will pick up our
            # saved tasks
            other_q = TaskQueue(self.q.doc_prefix)
            other_q.run()
            other_q.join()

            file_contents = tempf.read()
            print 'file contents:', file_contents
            assert file_contents == message

class TaskRESTSmokeTests(unittest.TestCase):
    def setUp(self):
        self.q = TaskQueue(doc_prefix='auto-tests-')

    def tearDown(self):
        self.q.db.set_tasks([])
        self.q.db.clear_errors()

    def fakeRequest(self, path, method='GET'):
        self.request  = RequestMock(path, method)
        self.handler = TaskHandler(self.request, self.q)

        return self.handler.respond()

    @attr(REST=True)
    def test404NoTasks(self):
        try:
            self.fakeRequest('/bugparty/tasks/at/10')
        except HTTPError as err:
            self.assertEqual(err.code, 404)
            return
        self.assertTrue(False, 'should have thrown a 404')
