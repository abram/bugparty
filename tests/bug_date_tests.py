#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from bug import Bug

import unittest, nose.tools
from nose.plugins.attrib import attr


class DateHandlingTests(unittest.TestCase):
    @attr(unit=True)
    def test_all_the_dates(self):
        correct = '2010-07-25T23:38:57+03:00'

        assert Bug.clean_bug_data({
            'when': correct
        })['openedDate'] == correct

        assert Bug.clean_bug_data({
            'created': correct
        })['openedDate'] == correct

        assert Bug.clean_bug_data({
            'created_at': correct
        })['openedDate'] == correct

        assert Bug.clean_bug_data({
            'openedDate': correct
            ,'when': None
        })['openedDate'] == correct

    @attr(unit=True)
    def test_fix_date_str_keeps_correct_datestrings(self):
        correct = '2010-07-25T23:38:57+03:00'
        self.assertEquals(Bug.fix_date_str(correct), correct)

    @attr(unit=True)
    def test_fix_date_fixes_other_formats(self):
        correct = '2010-07-25T23:38:57+00:00'
        self.assertEquals(
                Bug.fix_date_str('Sun, 25 Jul 2010 23:38:57 +0000'),
                correct)
        self.assertEquals(Bug.fix_date_str('25/7/2010 23:38:57'), correct)
        self.assertEquals(Bug.fix_date_str('25/7/2010 23:38:57'), correct)
        self.assertEquals(Bug.fix_date_str('7/25/10 23:38:57 +0000'),
                                    correct)
        self.assertEquals(Bug.fix_date_str('2010-07-25 23:38:57'), correct)
        self.assertEquals(Bug.fix_date_str('07/25/2010 11:38:57 PM'),
                                    correct)

        correct = '2010-07-25T00:00:00+00:00'
        self.assertEquals(Bug.fix_date_str('7/25/2010'), correct)

        correct = '2010-07-25T23:38:57+07:00'
        # self.assertEquals(fix_date_str('2010-07-25 11:38:57 PM Americas/Edmonton'), correct)

    @nose.tools.raises(ValueError)
    def test_die(self):
        Bug.fix_date_str("apple pie")
