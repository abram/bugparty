from http import HttpMultipartRequest

import unittest
from nose.plugins.attrib import attr

class MultipartTests(unittest.TestCase):
    
    @attr(unit=True)
    def test_get_boundary_content_type(self):
        multi_request = HttpMultipartRequest({}, '')
        
        boundary = multi_request.get_value_from_content_type(
            'some type; value=blah', 'boundary')
        self.assertEqual(boundary, '')

        boundary = multi_request.get_value_from_content_type(
            'some type; boundary=blah', 'boundary')
        self.assertEqual(boundary, 'blah')

    @attr(unit=True)
    def test_get_name_from_content_disposition(self):
        multi_request = HttpMultipartRequest({}, '')
        
        name = multi_request.get_value_from_content_type(
            'form-data; name="bug"', 'name')
        self.assertEqual(name, 'bug')

    @attr(unit=True)
    def test_parse_body_single(self):
        header = {'content-type': 'some type; boundary=thisisaboundary'}
        data = ('--thisisaboundary\r\nContent-Disposition: '
                'form-data; name="bug"'
                '\r\n\r\nlovely data\r\n--thisisaboundary--')
        multi_request = HttpMultipartRequest(header, data)

        multi_request.parse_body()
        self.assertEqual(multi_request.data['bug']['payload'], 'lovely data') 

    @attr(unit=True)
    def test_parse_body_double(self):
        header = {'content-type': 'some type; boundary=thisisaboundary'}
        data = ('--thisisaboundary\r\nContent-Disposition: '
                'form-data; name="bug"'
                '\r\n\r\nlovely data\r\n'
                '--thisisaboundary\r\nContent-Disposition: '
                'form-data; name="file"'
                '\r\n\r\nmore lovely data\r\n'
                '--thisisaboundary--')
        multi_request = HttpMultipartRequest(header, data)

        multi_request.parse_body()
        self.assertEqual(multi_request.data['bug']['payload'], 'lovely data')
        self.assertEquals(multi_request.data['file']['payload'], 
                          'more lovely data')

    @attr(unit=True)
    def test_parse_headers(self):
        multi_request = HttpMultipartRequest({}, '')
        part = 'Content-Disposition: form-data; name="bug"\r\n\r\nlovely data'

        headers = multi_request.parse_headers(part, part.find('\r\n\r\n'))
        self.assertEqual(headers, 
                         {'Content-Disposition': 'form-data; name="bug"'})
