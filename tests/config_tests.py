#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import argparse, StringIO, unittest
from config import Config
from nose.plugins.attrib import attr


class ConfigTests(unittest.TestCase):
    def setUp(self):
        Config.clearInstance();
        self.workdir = '/some/fake/dir'


    def test_commandline_args(self):
        parser = argparse.ArgumentParser()
        Config.add_args(parser)
        parsed = parser.parse_args(['--workdir', self.workdir])
        config = Config.build(parsed, fp=StringIO.StringIO(),
                                init_logging=False)

        self.assertEquals(config.values.get('bugparty', 'workdir'),
                            self.workdir)

    def test_ini_reading(self):
        parser = argparse.ArgumentParser()
        Config.add_args(parser)
        parsed = parser.parse_args([])

        config_file = '[bugparty]\nworkdir={}'.format(self.workdir)
        config = Config.build(parsed, fp=StringIO.StringIO(config_file),
                                init_logging=False)

        self.assertEquals(config.values.get('bugparty', 'workdir'),
                            self.workdir)

    def test_overrides(self):
        parser = argparse.ArgumentParser()
        Config.add_args(parser)
        parsed = parser.parse_args(['--workdir', self.workdir])

        config_file = '[bugparty]\nworkdir=/override/this'
        config = Config.build(parsed, fp=StringIO.StringIO(config_file),
                                init_logging=False)

        self.assertEquals(config.values.get('bugparty', 'workdir'),
                            self.workdir)
