#  Copyright (C) 2014 Alex Wilson
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import common, lda, project, random, urls, cluster
from cluster import ClusterTask
from config import Config
from databases import ElasticSearch
from lda_loader import LDATask, LDAIncrementalTask
from tests.test_utils import *

import itertools, json, shlex, subprocess, unittest, urllib2, uuid
from nose.plugins.attrib import attr
import requests


class RandomBugsSmokeTests(unittest.TestCase):
    PROJECT = 'bugparty_testing'

    @classmethod
    def setUpClass(cls):
        cls.docs_sample = RandomBugFactory(None).create_bugs(150)
        load_test_config()

    def setUp(self):
        load_test_config()
        self.project = project.Project(self.PROJECT)
        self.client = ElasticSearch(self.project.name)
        self.topic_count = 30

        self.projects_ctrl = project.ProjectsController()
        self.projects_ctrl.delete_project(self.PROJECT)
        self.projects_ctrl.create_project(self.PROJECT)

        self.client.connect_to_db().update({
                bug['bugid'] : bug
            for bug in self.docs_sample })
        self.client.refresh()

    def tearDown(self):
        load_test_config()
        self.projects_ctrl.delete_project(self.PROJECT)

    @attr(dolda=True)
    @with_config
    def test_loader(self):
        '''do an lda analysis and check a few things to see if it maybe
            worked'''
        loader, summary = LDATask(self.PROJECT, self.topic_count).run()
        self.client.refresh()

        topics_db = self.client.connect_to_topics_db()

        token_count = len(loader.lda_model.token_ids)
        assert token_count > 0

        for i in range(self.topic_count):
            topic_name = str(i)
            assert topic_name in topics_db
            topic = topics_db[topic_name]
            assert topic['method'] == 'lda'
            assert 'words' in topic

    def do_lda(self):
        loader, summary = LDATask(self.PROJECT, self.topic_count).run()
        self.client.refresh()

    @with_config
    def test_lda_clustering(self):
        '''very minimal clustering test'''
        self.do_lda()
        task = cluster.ClusterTask(self.PROJECT, 10)
        task.run()

        generated_db = self.client.connect_to_generated_db()
        assert task.doc_id in generated_db
        report = generated_db[task.doc_id]
        assert report['generator'] == 'lda-cluster'
        assert report['clusters'] == 10

    @attr(doinf=True)
    @with_config
    def test_inference(self):
        self.do_lda()
        # now we take a topic, pick all of its words, and then
        # put them in a bug
        topics_db = self.client.connect_to_topics_db()
        topic_words = topics_db['5']['words'].strip()

        assert topic_words
        bug = {
            '_id': 'in-topic-5'
            ,'owner': 'cary%and...@gtempaccount.com'
            ,'reportedBy': 'toyota...@gmail.com'
            ,'title': topic_words
            ,'description': topic_words
            ,"openedDate": common.utc_now().isoformat()
        }

        self.client.connect_to_db()['in-topic-5'] = bug
        self.client.refresh()

        print 'now running inference'
        loader, summary = LDAIncrementalTask(self.PROJECT).run()

        # we only added one document
        self.assertEquals(loader.lda_model.doc_count, 1)

        # now check if that bug got inferred for the correct topic
        bug = self.client.connect_to_db()['in-topic-5']
        best_topic = max(range(self.topic_count),
                key=lambda i: bug['topics'][str(i)])
        assert best_topic == 5

    @attr(smoke=True, require_server=True)
    def test_304(self):
        future = str(common.utc_now().replace(year=2050))
        host = 'http://localhost:8080'
        response = requests.get(host + urls.alldates(self.PROJECT), headers={ 'If-Modified-Since': future })


class RandomBugsOneLDARunTests(unittest.TestCase):
    '''do a single run of LDA and then test various things about it'''
    PROJECT = 'bugparty_testing'
    HOST = 'http://localhost:8080'

    @classmethod
    def setUpClass(cls):
        load_test_config()
        client = ElasticSearch(cls.PROJECT)

        cls.project = project.Project(cls.PROJECT)

        cls.projects_ctrl = project.ProjectsController()
        cls.projects_ctrl.delete_project(cls.PROJECT)
        cls.projects_ctrl.create_project(cls.PROJECT)

        cls.docs_sample = RandomBugFactory(client).create_bugs(350)
        client.refresh()

        cls.topic_count = 30
        LDATask(cls.PROJECT, cls.topic_count).run()

    @classmethod
    def tearDownClass(cls):
        load_test_config()
        cls.projects_ctrl.delete_project(cls.PROJECT)

    @attr(smoke=True, require_server=True)
    def test_topics_rename(self):
        '''can we rename topic 0'''
        name = 'new name for topic 0'
        url = 'http://localhost:8080/bugparty/{}/topics/0' \
                    .format(self.PROJECT)
        response = requests.patch(url, data=json.dumps({'name': name}))
        assert response.ok

        response = requests.get(url)
        assert response.ok
        assert response.json()['name'] == name

    @attr(smoke=True, require_server=True)
    def test_searches(self):
        '''can we do searches without getting errors?'''
        bugid = self.docs_sample[0]['bugid']

        searches = [
                '/similar_title/' + bugid
                ,'/similar_text/' + bugid
                ,'/similar_description/' + bugid
                ,'/search_title/wow'
                ,'/search_text/wow'
                ,'/search_description/wow'
        ]

        for search in searches:
            response = requests.get('http://localhost:8080/bugparty/{}{}' \
                        .format(self.PROJECT, search))
            print search, response
            assert response.ok

    @attr(smoke=True, require_server=True)
    def test_flann_worked(self):
        for bug in random.sample(self.docs_sample, 5):
            path = urls.bug_topic_similar(self.PROJECT, bug['bugid'])
            response = requests.get(self.HOST + path)

            print response.json()
            assert response.ok
            assert type(response.json()) is list

    @attr(smoke=True, require_server=True)
    def test_empty_text_search(self):
        url = 'http://localhost:8080/bugparty/{}/search_text/?q='
        response = requests.get(url.format(self.PROJECT))
        assert response.ok
        assert len(response.json()) == 0

    @attr(smoke=True, require_server=True)
    def test_304(self):
        future = str(common.utc_now().replace(year=2050))
        host = 'http://localhost:8080'
        response = requests.get(host + urls.alldates(self.PROJECT), headers={
                'If-Modified-Since': future
            })

        assert response.status_code == 304
