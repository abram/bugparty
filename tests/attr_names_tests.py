from bug import BugVerifier

import unittest
from nose.plugins.attrib import attr

class MultipartTests(unittest.TestCase):
    
    @attr(unit=True)
    def test_change_attributes(self):
        bug = {'changeMe': 'butNotMe', 'attr': 'value'}
        attr = {'properName': 'changeMe'}
        
        bug_verifier = BugVerifier()
        changed_json = bug_verifier.change_attributes(bug, attr)

        self.assertTrue('properName' in changed_json)
        self.assertTrue(changed_json['properName'] == 'butNotMe')
        self.assertTrue(changed_json['attr'] == 'value')

    @attr(unit=True)
    def test_change_all(self):
        bugs = [{'changeMe': 'butNotMe1', 'attr': 'value1'},
                {'changeMe': 'butNotMe2', 'attr': 'value2'}]
        attr = {'properName': 'changeMe'}

        bug_verifier = BugVerifier()
        changed_json = bug_verifier.change_all_attributes(bugs, attr)

        new_bugs = [{'properName': 'butNotMe1', 'attr': 'value1'},
                {'properName': 'butNotMe2', 'attr': 'value2'}]
        self.assertEqual(changed_json, new_bugs)
