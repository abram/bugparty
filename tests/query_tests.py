#  Copyright (C) 2016 Alex Wilson, Andrea McIntosh
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import json, shlex, subprocess, unittest, urllib2, uuid
from nose.plugins.attrib import attr
from tests.test_utils import *

from config import Config
from databases import ElasticSearch, ESType
from bug import Bug
from lda_loader import LDATask
import elasticsearch, requests

import common, project, urls, time

class QueryTests(unittest.TestCase):

    PROJECT = 'queries_test'
    DATA_FILE_PATH = './test_bugs.json'  # Generalize

    @classmethod
    def setUpClass(cls):
        fn = os.path.join(os.path.dirname(__file__), cls.DATA_FILE_PATH)
        with open(fn) as json_file:
            test_bugs = json.load(json_file)
            for bug in test_bugs:
                bug_data = bug
                bug_id = str(bug_data['bugid'])
                Bug.clean_bug_data(bug_data)
        cls.docs_sample = test_bugs

        load_test_config()

    def setUp(self):
        load_test_config()
        self.project = project.Project(self.PROJECT)
        self.client = ElasticSearch(self.project.name)

        self.projects_ctrl = project.ProjectsController()
        self.projects_ctrl.delete_project(self.PROJECT)
        self.projects_ctrl.create_project(self.PROJECT)

        db = self.client.connect_to_db()
        for bug in self.docs_sample:
            db.update({ bug['bugid'] : bug })
        self.client.refresh()

        self.bugparty_url = 'http://localhost:8080/bugparty/queries_test'

    def tearDown(self):
        load_test_config()
        self.projects_ctrl.delete_project(self.PROJECT)

    # TODO: Test properly

    @attr(smoke=True, require_server=True)
    def test_tag_search(self):
        # [=%5B and ]=%5D
        search = '/search_text?q=%5Bcat bat%5D'

        response = requests.get('http://localhost:8080/bugparty/{}{}' \
                    .format(self.PROJECT, search))
        print response
        assert response.ok

    @attr(smoke=True, require_server=True)
    def test_path_search(self):
        search = '/search_text?q=src/bugparty/docs.txt'

        response = requests.get('http://localhost:8080/bugparty/{}{}' \
                    .format(self.PROJECT, search))
        print response
        assert response.ok

    @attr(smoke=True, require_server=True)
    def test_tagandpath_search(self):
        # [=%5B and ]=%5D
        search = '/search_text?q=%5Bcat bat%5D src/bugparty/docs.txt'
        response = requests.get('http://localhost:8080/bugparty/{}{}' \
                    .format(self.PROJECT, search))
        print response
        assert response.ok

        search = '/search_text?q=%5Bcat bat%5D src/bugparty/docs.txt %5Bsat mat%5D'
        response = requests.get('http://localhost:8080/bugparty/{}{}' \
                    .format(self.PROJECT, search))
        print response
        assert response.ok

    @attr(smoke=True, require_server=True)
    def test_search_no_results(self):
        fail_search = '/search_text?q=%5Bby joves%5D'
        response = requests.get('http://localhost:8080/bugparty/{}{}' \
                    .format(self.PROJECT, fail_search))
        print response
        assert response.text == '[]'

    def bugparty(self, action, data):
        return self.http_get_json('/'.join([self.bugparty_url, action, data]))

    @staticmethod
    def http_get_json(url):
        return json.loads(urllib2.urlopen(url).read())
