#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import requests

import lda, project
from lda_loader import LDATask
from cluster import ClusterTask
from config import Config
from common import *
from databases import ElasticSearch
from tests.test_utils import delete_dbs_for_project, with_config, load_test_config, RandomBugFactory

import itertools, json, shlex, subprocess, unittest, urllib2, uuid
from nose.plugins.attrib import attr


class ReportHandlerTests(unittest.TestCase):
    PROJECT = 'bugparty_testing'

    @classmethod
    def setUpClass(cls):
        load_test_config()
        client = ElasticSearch(cls.PROJECT)

        cls.project = project.Project(cls.PROJECT)

        cls.projects_ctrl = project.ProjectsController()
        cls.projects_ctrl.delete_project(cls.PROJECT)
        cls.projects_ctrl.create_project(cls.PROJECT)

        docs_sample = RandomBugFactory(client).create_bugs(150)
        client.refresh()

        cls.topic_count = 30
        LDATask(cls.PROJECT, cls.topic_count).run()
        client.refresh()
        ClusterTask(cls.PROJECT, 10).run()
        client.refresh()

        cls.HOST = 'http://localhost:8080'
        cls.REPORTS_URL = '{}/bugparty/{}/reports'.format(cls.HOST, cls.PROJECT)

    @classmethod
    def tearDownClass(cls):
        load_test_config()
        cls.projects_ctrl.delete_project(cls.PROJECT)

    def get_reports(self):
        result = requests.get(self.REPORTS_URL)
        assert result.status_code == 200
        data = result.json()
        assert len(data) == 2

        return sorted(data, key=lambda x: x['generator'])

    def test_makes_lda_report(self):
        cluster_doc, lda_doc = self.get_reports()

        assert lda_doc['generator'] == 'lda-run'
        assert int(lda_doc['topics']) == self.topic_count
        assert parse_date_str(lda_doc['when'])

    def test_sets_svg_content_type(self):
        cluster_doc, lda_doc = self.get_reports()
        svg_url = cluster_doc['silhouette.svg']
        
        result = requests.get(self.HOST + svg_url)
        assert result.status_code == 200

        assert result.headers['Content-Type'] == 'image/svg+xml'
        assert parse_date_str(result.headers['Last-Modified'])

    def test_sets_csv_content_type(self):
        cluster_doc, lda_doc = self.get_reports()
        csv_url = cluster_doc['cluster.csv']
        
        result = requests.get(self.HOST + csv_url)
        assert result.status_code == 200
        assert result.headers['Content-Type'] == 'text/csv'
        assert parse_date_str(result.headers['Last-Modified'])

    def test_sets_last_modified_on_list(self):
        result = requests.get(self.REPORTS_URL)
        assert result.status_code == 200
        assert parse_date_str(result.headers['Last-Modified'])
