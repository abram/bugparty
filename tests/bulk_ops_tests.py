#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import common
from databases import ElasticSearch, BulkOps
from tests.test_utils import ElasticMock, RandomBugFactory
from nose.plugins.attrib import attr

import itertools, unittest


class BulkOpsTest(unittest.TestCase):
    def setUp(self):
        self.client = ElasticMock()
        RandomBugFactory(self.client).create_bugs(20)
        self.bulk_ops = BulkOps(self.client, ElasticSearch.BUGS)

    @attr(unit=True)
    def test_delays_write(self):
        for bug_id in self.client.get_all_bugids():
            self.bulk_ops.replace(bug_id, {
                'test': 'wow'
            })

        db = self.client.connect_to_db()
        for bug_id in self.client.get_all_bugids():
            self.assertNotIn('test', db[bug_id])


    @attr(unit=True)
    def test_insert_doc(self):
        self.bulk_ops.replace('new-doc', {
            'this': 'is new'
        })
        self.bulk_ops.apply()
        self.assertTrue('new-doc' in self.client.connect_to_db())

    @attr(unit=True)
    def test_batch_iter(self):
        self.assertEqual(list(itertools.chain(*list(common.iter_batches(range(1000000),
            300)))), range(1000000))
