#  Copyright (C) 2014 Alex Wilson
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import unittest, shlex, subprocess, common, project
from nose.plugins.attrib import attr

from cluster import ClusterTask
from lda_loader import LDATask, LDAIncrementalTask
from project import ProjectsController
from topics_controller import TopicsController
from tests.test_utils import *


class LDAUnitTests(unittest.TestCase):
    PROJECT = 'bugparty_testing'

    def setUp(self):
        load_test_config()

        ElasticMock.clear()
        self.client = ElasticMock()
        ProjectsController(ElasticMock).create_project(self.PROJECT)

        self.started_bugs_at = 30
        self.bug_factory = RandomBugFactory(self.client,
                            self.started_bugs_at)

        self.bugs = list(self.bug_factory.create_bugs(150))
        self.topic_count = 20

        self.client.refresh()

    @attr(unit=True)
    @with_config
    def test_makes_topic_docs(self):
        LDATask(self.PROJECT, self.topic_count).run(client=self.client)

        self.client.connect_to_topics_db().refresh()
        topics = TopicsController(self.client).get_topics()
        self.assertEquals(len(topics), self.topic_count)

        for i in range(len(topics)):
            topic = topics[i]
            assert topic['method'] == 'lda'
            assert topic['words']

    @attr(unit=True)
    @with_config
    def test_dtm_order_does_not_change(self):
        '''test that adding new things to the dtm keeps the order correct'''
        LDATask(self.PROJECT, self.topic_count).run(client=self.client)

        # TODO: we could change the topic scores so that the bugs
        # have uniquely identifiable scores
        bug_ids = self.client.get_all_bugids()
        dtm = TopicsController(self.client).get_dtm()

        ## make a map from bug_id to dtm row
        # Old: dtm_map = dict(zip(bug_ids, dtm))
        dtm_map = dict(zip(dtm[1], dtm[0]))

        # now we add some new bugs, do an inference, and then see if
        # the new dtm is still in the right order
        self.bug_factory.next_id = 1
        self.bug_factory.create_bugs(20)
        LDAIncrementalTask(self.PROJECT).run(client=self.client)

        bug_ids = self.client.get_all_bugids()
        dtm = TopicsController(self.client).get_dtm()

        ## now check that each bug has the same scores as it used to
        # Old: for bug_id, topic_scores in zip(bug_ids, dtm):
        for bug_id, topic_scores in zip(dtm[1], dtm[0]):
            if bug_id in dtm_map:
                self.assertEquals(dtm_map[bug_id].all(), topic_scores.all())

    @with_config
    def test_inference(self):
        LDATask(self.PROJECT, self.topic_count).run(client=self.client)
        self.client.refresh()

        # now we take a topic, pick all of its words, and then
        # put them in a bug
        topics_db = self.client.connect_to_topics_db()
        topic_words = topics_db['5']['words']
        assert topic_words
        bug = {
            'bugid': 'in-topic-5'
            ,'owner': 'cary%and...@gtempaccount.com'
            ,'reportedBy': 'toyota...@gmail.com'
            ,'title': topic_words
            ,'description': topic_words
            ,"openedDate": "Mon, 12 Nov 2007 22:46:48 +0000"
        }

        self.client.connect_to_db()['in-topic-5'] = bug
        self.client.refresh()

        print 'now running inference'
        loader, summary = LDAIncrementalTask(self.PROJECT) \
                            .run(client=self.client)
        self.client.refresh()

        # we only added one document
        assert loader.lda_model.doc_count == 1

        # now check if that bug got inferred for the correct topic
        bug_info = self.client.connect_to_db()['in-topic-5']

        best_topic = max(range(self.topic_count),
                key=lambda i: bug_info['topics'][str(i)])
        assert best_topic == 5

    @with_config
    def test_lda_clustering(self):
        '''very minimal clustering test'''
        LDATask(self.PROJECT, self.topic_count).run(client=self.client)

        task = ClusterTask(self.PROJECT, 10)
        task.run(client=self.client)
        self.client.refresh()

        generated_db = self.client.connect_to_generated_db()
        assert task.doc_id in generated_db
        report = generated_db[task.doc_id]
        assert report['generator'] == 'lda-cluster'
        assert report['clusters'] == 10
