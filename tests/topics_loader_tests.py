#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import itertools, unittest
from nose.plugins.attrib import attr

from topics_controller import *


class TopicsLoaderTests(unittest.TestCase):
    def setUp(self):
        pass

    @attr(unit=True)
    def test_add_documents_to_dtm(self):
        old_dtm = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
        ids = '1 2 3 4 5'.split()
        old_docs = ids[:3]
        new_docs = ids[3:]
        topics = [0, 1, 2]

        self.run = TopicsRun('test-loader')
        self.topics_loader = TopicsLoader(self.run, ids, topics)
        self.topics_loader.set_document_topic_associations([9, 10, 11])
        self.topics_loader.set_document_topic_associations([12, 13, 14])

        self.topics_loader.ids = new_docs
        new_dtm = list(self.topics_loader.merge_document_topic_matrix(
                                                        old_dtm, old_docs))
        expected_dtm =  [[0, 1, 2]
                        ,[3, 4, 5]
                        ,[6, 7, 8]
                        ,[9, 10, 11]
                        ,[12, 13, 14]]
        self.assertEqual(new_dtm, expected_dtm)

    @attr(unit=True)
    def test_add_topics_to_dtm(self):
        old_dtm = [[0, 1, 2], [5, 6, 7], [10, 11, 12]]
        ids = '1 2 3'.split()
        topics = [3, 4]

        self.run = TopicsRun('test-loader')
        self.topics_loader = TopicsLoader(self.run, ids, topics)
        self.topics_loader.set_document_topic_associations([3, 4])
        self.topics_loader.set_document_topic_associations([8, 9])
        self.topics_loader.set_document_topic_associations([13, 14])

        self.topics_loader.ids = ids
        new_dtm = list(self.topics_loader.merge_document_topic_matrix(
                        old_dtm, ids))
        expected_dtm =  [[0,  1,  2,  3,  4]
                        ,[5,  6,  7,  8,  9]
                        ,[10, 11, 12, 13, 14]]
        self.assertEqual(new_dtm, expected_dtm)

    @attr(unit=True)
    def test_add_topics_and_docs_to_dtm(self):
        old_dtm = [[0, 1, 2], [5, 6, 7], [10, 11, 12]]
        ids = '1 2 3 4 5'.split()
        old_ids = ids[:3]
        new_ids = ids[3:]

        topics = [3, 4]

        self.run = TopicsRun('test-loader')
        self.topics_loader = TopicsLoader(self.run, ids, topics)
        self.topics_loader.set_document_topic_associations([18, 19])
        self.topics_loader.set_document_topic_associations([23, 24])

        self.topics_loader.ids = new_ids
        new_dtm = list(self.topics_loader.merge_document_topic_matrix(
                        old_dtm, old_ids))
        expected_dtm =  [[0, 1, 2, -1, -1]
                        ,[5, 6, 7, -1, -1]
                        ,[10, 11, 12, -1, -1]
                        ,[-1, -1, -1, 18, 19]
                        ,[-1, -1, -1, 23, 24]]
        self.assertEqual(new_dtm, expected_dtm)

    @attr(unit=True)
    def test_can_merge_with_empty(self):
        ids = '1 2 3 4 5'.split()
        self.run = TopicsRun('test-loader')
        self.topics_loader = TopicsLoader(self.run, ids, [0, 1,2,3,4])
        self.topics_loader.ids = ids

        expected_dtm =  [[0, 1, 2, 1, 100]
                        ,[5, 6, 7, 104, 120]
                        ,[10, 11, 12, 110000, 2]
                        ,[291, 7, 7, 18, 19]
                        ,[7, 2, 6, 23, 24]]
        for row in expected_dtm:
            self.topics_loader.set_document_topic_associations(row)

        new_dtm = self.topics_loader.merge_document_topic_matrix([],[])
        self.assertEqual(list(new_dtm), expected_dtm)

