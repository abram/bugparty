#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from tests.test_utils import *

from project import Project, ProjectsController


import datetime, unittest
from dateutil.tz import tzutc
from nose.plugins.attrib import attr

from tests.test_utils import *


class ProjectTests(unittest.TestCase):
    PROJECT = 'bugparty_testing'

    def setUp(self):
        ElasticMock.clear()
        self.client = ElasticMock()
        self.project = Project(self.PROJECT, self.client)
        self.projects = ProjectsController(ElasticMock)

    @attr(unit=True)
    @with_config
    def test_timestamp_roundtrip(self):
        self.projects.create_project(self.PROJECT)

        timestamp_name = 'fake_time'
        self.assertIsNone(self.project.get_timestamp(timestamp_name))
        self.project.update_timestamps([timestamp_name])
        self.assertIsNotNone(self.project.get_timestamp(timestamp_name))

    @attr(unit=True)
    @with_config
    def test_timestamp_roundtrip(self):
        self.projects.create_project(self.PROJECT)

        now = datetime.datetime.now(tzutc())
        self.project.update_timestamps(['today'], now)
        self.project.update_timestamps(['yesterday'],
                    now + datetime.timedelta(days=-1))

        print self.project.get_timestamp('today')
        print self.project.get_timestamp('yesterday')
        print self.project.get_timestamp('does not exist')

        self.assertEquals(self.project.get_most_recent_timestamp(
            ['today', 'yesterday', 'does not exist']), now)
