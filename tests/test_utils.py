#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import datetime, functools, random, string, StringIO, os

from common import TempDir
from config import Config
from databases import BulkOps, ElasticSearch
from uuid import uuid4

import elasticsearch


def delete_dbs_for_project(client, project):
    print 'deleting dbs for {}'.format(project)
    client.client.indices.delete(project, ignore=[400, 404])


class RequestMock(object):
    def __init__(self, path, method):
        self.path = path
        self.headers = {
            'METHOD': method
        }

def es_merge(x, y):
    '''recursively merge dicts, like ES partial updates'''
    for k in y.keys():
        if k in x and type(x[k]) is dict and type(y[k]) is dict:
            es_merge(x[k], y[k])
        else:
            x[k] = y[k]


class ESTypeMock(dict):
    def update(self, docs):
        ''' bulk update docs '''
        es_merge(self, docs)
        return True

    def update_doc(self, doc_id, data):
        self.update({doc_id: data})

    def refresh(self):
        pass

    def __setitem__(self, key, value):
        return super(ESTypeMock, self).__setitem__(key, value)

    def bulk(self, inserts=[], replacements={}, updates={}):
        map(self.index, inserts)
        super(ESTypeMock, self).update(replacements)
        self.update(updates)

    def count(self):
        return len(self)

    def index(self, doc):
        self[uuid4().hex] = doc

    def mget(self, docids, **kwargs):
        for i in docids:
            yield i, self[i]


class ElasticMock(object):
    bugparty_dbs = {}

    @classmethod
    def clear(cls):
        cls.bugparty_dbs = {}

    def __init__(self, *args):
        self.db = ESTypeMock()
        self.topics_db = ESTypeMock()
        self.generated_db = ESTypeMock()

    @staticmethod
    def connect_to_bugparty_db(name):
        if not name in ElasticMock.bugparty_dbs:
            ElasticMock.bugparty_dbs[name] = ESTypeMock()
        return ElasticMock.bugparty_dbs[name]

    def create_dbs_for_project(self):
        pass

    def connect_to_generated_db(self):
        return self.generated_db

    def connect_to_topics_db(self):
        return self.topics_db

    def connect_to_db(self):
        return self.db

    def get_all_revs(self, db):
        db = self.connect(db)
        return {
            k: v['_rev']
            for k, v in db.iteritems()
        }

    def get_all_bugids(self, db=ElasticSearch.BUGS):
        return sorted(self.connect(db).keys())

    def get_all_docs(self, db=ElasticSearch.BUGS, **kwargs):
        return self.connect(db)

    def get_docs(self, ids, db=ElasticSearch.BUGS):
        docs = self.connect(db)
        return [docs[k] for k in ids]

    def connect(self, db):
        return {
            ElasticSearch.BUGS: self.db
            ,ElasticSearch.TOPICS: self.topics_db
        }[db]

    def refresh(self):
        pass


class RandomBugFactory(object):
    def __init__(self, client, next_id=1):
        self.client = client
        self.next_id = next_id

    def create_similarities(self, count):
        '''
        creates similarities
        '''
        similarities = []
        for i in range(count):
            id1 = str(self.next_id) + 'ouch'
            for i in range(count + 1):
                id2 = str(self.next_id) + 'ouch'
                similarity = {
                    'id1': id1,
                    'id2': id2,
                    'rep': ''.join(self.make_words(1)).encode('utf-8')
                    }
                similarities.append(similarity)
        return similarities

    def create_bugs(self, count, dates=None):
        '''creates bugs and adds them to the db.
            each bug will have some random words generated for the
            title/description.

            bugs will have dates assigned by the values returned by
            the dates generator. If this is not provided, they will
            all get today's date'''
        dates = dates or RandomBugFactory.rangedDates(20)
        bugs = []
        for i in range(count):
            # make weird bug ids for testing
            bug_id = str(self.next_id) + 'ouch'
            self.next_id += 1

            # we purposefully introduce various problematic things to our bugs
            # problems:yes : text of this form was causing search issues
            # unichr(233).encode('utf-8') : we need to be sure we can deal with
            # non-ascii chars
            bug = {
                '_id': bug_id
                ,'bugid': bug_id
                ,'title': 'problems:yes/' + ' '.join(self.make_words(20)) + ',' + unichr(233).encode('utf-8')
                ,'description': 'problems:99/' + ' '.join(self.make_words(100)) + ',' + unichr(233).encode('utf-8')
                ,'openedDate': next(dates).isoformat()
            }

            bug['title'] = bug['title'].decode('utf-8')
            bug['description'] = bug['description'].decode('utf-8')
            bugs.append(bug)

        if self.client:
            bulk = BulkOps(self.client, ElasticSearch.BUGS)
            for bug in bugs:
                bulk.replace(bug['_id'], bug)
            bulk.apply()
        return bugs

    @staticmethod
    def sameDate(date=None):
        date = date or datetime.datetime.now()
        while True:
            yield date

    @staticmethod
    def loopDates(dates):
        while True:
            for date in dates:
                yield date

    @staticmethod
    def rangedDates(steps, size=10):
        half_range = steps / 2 * size
        dates = [datetime.datetime.now() + datetime.timedelta(days=i)
                    for i in range(-half_range, half_range, size)]
        return RandomBugFactory.loopDates(dates)

    @staticmethod
    def make_words(count):
        ''' generates count ascii words '''
        for i in range(count):
            word_length = random.randrange(3, 10)
            yield ''.join(random.sample(string.ascii_letters, word_length))


def load_test_config():
    Config.clearInstance()
    Config.build(None, 'tests/bugparty.ini', init_logging=False) 

def with_config(test_func):
    '''decorator that sets up a Config instance with a temporary workdir
        for the test'''
    @functools.wraps(test_func)
    def wrapped(*args, **kwargs):
        Config.clearInstance()

        import argparse
        parser = argparse.ArgumentParser('unit tests')
        Config.add_args(parser)
        with TempDir() as td:
            parsed_args = parser.parse_args(['--workdir', td.path])

            os.mkdir(os.path.join(td.path, 'logs'))

            Config.build(parsed_args, 'tests/bugparty.ini') 
            test_func(*args, **kwargs)
            Config.clearInstance()

    return wrapped
