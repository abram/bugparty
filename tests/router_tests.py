#  Copyright (C) 2014 Alex Wilson
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from common import SimpleRouter

import unittest
from nose.plugins.attrib import attr


class RouterTests(unittest.TestCase):
    def setUp(self):
        self.router = SimpleRouter()


    @attr(unit=True)
    def test_index_route(self):
        @self.router.add_route('/')
        def index():
            pass

        params, view_func = self.router.route('/')
        assert params == {}
        assert view_func == index

    @attr(unit=True)
    def test_nested_routes(self):
        @self.router.add_route('/stuff/a')
        def a():
            pass

        @self.router.add_route('/stuff/b')
        def b():
            pass

        assert self.router.route('/stuff/a')[1] == a
        assert self.router.route('/stuff/b')[1] == b

    @attr(unit=True)
    def test_dynamic(self):

        @self.router.add_route('/$stuff/and/$other')
        def view(stuff, other):
            pass

        params, view_func = self.router.route('/neat/and/cool')
        assert params == {'stuff': 'neat', 'other': 'cool'}
        assert view_func == view

    @attr(unit=True)
    def test_not_found(self):
        params, view_func = self.router.route('/wowie')
        assert params is None
        assert view_func is None

    @attr(unit=True)
    def test_method_routing(self):
        @self.router.add_route('/$stuff/and/$other', methods=['PUT', 'GET'])
        def get_or_put(stuff, other):
            pass

        @self.router.add_route('/$stuff/and/$other', methods=['POST'])
        def post(stuff, other):
            pass

        params, view_func = self.router.route('/neat/and/cool', 'PUT')
        assert view_func == get_or_put

        params, view_func = self.router.route('/neat/and/cool', 'GET')
        assert view_func == get_or_put

        params, view_func = self.router.route('/neat/and/cool', 'POST')
        assert view_func == post
