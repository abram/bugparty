// ==UserScript==
// @name        BugParty
// @namespace   http://code.google.com/p/android/issues/
// @include     http://code.google.com/p/android/issues/*
// @include     https://code.google.com/p/android/issues/*
// @version     1
// ==/UserScript==
var bugid = "";
var querystring = location.search.replace( '?', '' ).split( '&' );
var queries = {};
for ( var i = 0; i < querystring.length ; i++ ) {
	var v = querystring[i].split('=');
	var name = v[0];
	var value = v[1];
	queries[name] = value;
}
bugid = queries["id"] || 0;

function text(t) {
    return document.createTextNode( t );
}
function entity(t) {
    return document.createElement( t );
}


if (bugid > 0) {
    var meta = document.getElementById("issuemeta");
    var similartextcontent =  document.createElement("DIV")
    var ldacontent =  document.createElement("DIV")
    var topiccontent =  document.createElement("DIV")

    meta.appendChild( similartextcontent );
    meta.appendChild( ldacontent );
    meta.appendChild( topiccontent );
	
    function getResults(title, urlprefix, resultkey, contentdiv, callback) {
        GM_xmlhttpRequest({
            method: "GET",
            url: urlprefix+bugid,
            headers: {
                "User-Agent": "Mozilla/5.0",    // If not specified, navigator.userAgent will be used.
                "Accept": "text/json"            // If not specified, browser defaults will be used.
            },
            onload: function(response) {
                var reply = JSON.parse( response.responseText  );
                var similar = reply[resultkey];
                
                contentdiv.appendChild(text(title));
                contentdiv.appendChild(entity("BR"));
                
                
                for (var i = 0; i < similar.length; i++) {
                    var mya = document.createElement("a");
                    var s = similar[i]
                    mya.setAttribute("href","https://code.google.com/p/android/issues/detail?id=" + s["id"]);
	            mya.appendChild( document.createTextNode(s["id"]) );
                    contentdiv.appendChild( mya );
                    contentdiv.appendChild( document.createTextNode(" ") );
                    
                }
                if (callback!=false) {
                    callback(reply);
                }
            }
        });
    };

    var ldaTopics = function( result ) {
        var topics = result["topics"];
        var out = []
        
        topiccontent.appendChild(text("Topics:"));
        topiccontent.appendChild(entity("BR"));
        
        for (var i = 0; i < topics.length; i++) {
            if (topics[i] > 1.0) {
                out.push([i,topics[i]]);
            }
        }
        out.sort(function(a,b) { return b[1] - a[1]; })
        
        if (out.length > 0) {
            GM_xmlhttpRequest({
                method: "GET",
                url: "http://localhost:8080/bugparty/android/lda/topicsummaries/",
                headers: {
                    "User-Agent": "Mozilla/5.0",    // If not specified, navigator.userAgent will be used.
                    "Accept": "text/json"            // If not specified, browser defaults will be used.
                },
                onload: function(response) {
                    var reply = JSON.parse( response.responseText  );
                    var result = reply;
                    var summary = result["summary"];

                    for (var i = 0; i < out.length; i++) {
                        var v = out[i];
                        var mya = document.createElement("a");
                        mya.setAttribute("href","http://localhost:8080/bugparty/android/lda/topicsummary/"+v[0]);
                        mya.setAttribute("title", summary[out[i][0]].join(" ") );

	                mya.appendChild( document.createTextNode("Topic "+v[0]) );
                        topiccontent.appendChild( mya );
                        topiccontent.appendChild( document.createTextNode(" ") );
                    }
                }
            });
        }
    };

    getResults("LDA Similar:",  "http://localhost:8080/bugparty/android/lda/similar/", "similar", ldacontent, ldaTopics);
    getResults("Similar Text:",  "http://localhost:8080/bugparty/android/similar_text/", "rows", similartextcontent, false);

}
