# Recommended Setup 

Bugparty setup should be handled by its docker project, avilable 
at https://bitbucket.org/abram/bugparty-docker. Following the setup 
instructions for that project will automatically download this repo and 
all required dependencies, so that bugparty can be launched from a docker
container.

### Logging

By default, mongrel will log to /var/lib/bugparty/logs/error.log and
access.log. The python application will log to
/var/lib/bugparty/logs/bugparty.log. This behaviour can be changed with
command line arguments to front\_controller.py.

```mkdir /var/lib/bugparty/logs``` and make sure it is writeable by bugparty.

### CLI commands

create a new project with all of its databases:

    python project.py --create <project name>
    
load an array of bugs into a project from a .json file: 

    python project.py --loadjson <path_to_bugs.json> <project name>

after bugs have been added, run LDA analysis:

    python lda_loader.py <project name> --topics <topic count>

after more bugs have been added, run LDA inference for the new bugs:

    python lda_loader.py <project name> --incremental

run the bucket loader (this will update the data for crossfilter)

    python bucket_loader.py <project>

rerun a custom topic analysis (eg. after more bugs have been added)

    python custom_topics.py <project> <topic id (eg. 10)>

run the bug deduper over all bugs

    bash util/runBugDedupFastREP.sh <project name> ~/bugparty/fastrep<project name>.csv


## Web Interface

The web interface is provided by a Mongrel2 web server and a python mongrel
handler. Both need to be running to use the web interface.

The server can be run with

    make start

and the python handler with

    python front_controller.py


Alternatively, you can run

    python util/dev_server.py

to run front_controller.py and have it automatically restart when one of the
python files changes.


#### REST urls

The available REST urls can be listed with the following command:

    python front_controller.py --list

### Web apps

For an up-to-date listing of available UI components in, bugparty you can
browse to ```http://localhost:8080/bugparty```.

## Running the tests

    sudo easy_install nose

Before running the tests, you should run front_controller.py with the
tests/bugparty.ini file as its config.

    python front_controller.py --ini tests/bugparty.ini

run all tests:

    nosetests

run only smoke tests (smoke tests will require mongrel/couch to be running)

    nosetests -a smoke

run only unit tests

    nosetests -a unit

##### Elastic Search structure
  * each project has one index, with the following types:
      * bugs : topics[topic_id] -> score
      * similarbug : child doc of a bug with LDA nearest neighbour info
      * topic: like topic-summary-X
      * reports:
          like generated db, with eg clustering reports?
          OR we could do those as child documents, with a clustering
          run id and cluster id, which would allow for some cool queries...
  * we will also need a bugparty-specific index, with the types
    * task:
      * like the stuff currently in bugparty_internal/queue
      * (we can get the queue by sorting by date)
    * task_error
      * like the stuff currently in bugparty_internal/errors
    * project
      * name, date created,... 

  other things: (do these even need to be in ES?)
    * lda-words (word -> token_id)
    * lda-topics / lda-topic-{topic-id} (topic -> token)
    * lda-similar-documents: similarities for docs based on nn of LDA data

##### LDA Databse Contents

 * lda-words : ```words[token]``` token\_id for token
 * lda-topics : ```topics[topic_id][token_id]``` association of topic to token
 * lda-topic-{topic_id} : ```wordvector[token_id]``` association of topic to token
 * -topic-summary : ```summary[topic_id]``` top 25 words for this topic
 * -topic-summary-{topic_id} : ```words``` top 25 words for this topic
 * -topic-matrix : ```document_topic_matrix[document_index][topic_id]``` topic association for the document
 * -topic-map: ```[document_id][topic_id]``` topic associations for the document
 * lda-similar-documents:
        ```[document_id] -> [{'id': other_doc_id, 'r': distance, 'i': index in this list}]``` top 25 most similar documents
 * {document_id}:
   * ```topics[topic_id]``` -> topic association for this document
   * ```similar``` similar documents array as in lda-similar-documents

##### Buckets Databse Contents

 * {date}: ```topics[i] -> list of bugs and relevancy for that date```


## Misc. Git things

[kibana 3.0](http://github.com/elasticsearch/kibana) is included in
static/kibana as a subtree. Upstream changes can be merged in with

    git fetch kibana
    git merge --squash -s subtree kibana/3.0

See the [git-scm book](http://git-scm.com/book/ch6-7.html) for more on subtrees.

## Expected Bug Schema
Bugs must have:
  * bugid: string
  * title: string
  * description: string
  * openedDate: date string in format: "E, dd MMM yyyy HH:mm:ss Z"
        (soon to be in iso format)

Bugs can have: (these will be used if present but)
  * owner: string
  * status: string
  * reportedBy: string
  * comments : an array of objects with
  * author: optional string
  * what: optional string

## Old Setup

After cloning, you must install vowpal wabbit and flann. These are
contained in git submodules within this project, and can be installed
with the following command:

    make deps

R stuff:
    sudo apt-get install littler

```/var/lib/bugparty``` should exist and be writeable by both mongrel
and the python processes.
