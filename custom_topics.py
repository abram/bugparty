#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012-14 Abram Hindle
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import os, shlex, subprocess

import project, task_queue
from config import Config
from databases import ElasticSearch
from topics_controller import *


TOPIC_ANALYSER_COMMAND = 'make project={} topic={} custom-topic-analysis'

class CustomTopicTask(task_queue.Task):
    recoverable = True

    def __init__(self, project, topic_id):
        self.project = project
        self.topic_id = topic_id

    def run(self, worker=None):
        run = TopicsRun(self.project)
        loader = TopicsLoader(run, run.client.get_all_bugids(),
                                [self.topic_id])
        scores = self.run_analysis()
        scores = sorted(scores) # sort scores by bugid

        print scores
        for (bugid, score) in scores:
            loader.set_document_topic_associations([score])

        loader.ids = loader.all_ids
        loader.update_summaries_in_database()
        project.Project(self.project).update_timestamps([
                    project.Project.TOPIC_SCORES_TIMESTAMP])

    def run_analysis(self):
        command = TOPIC_ANALYSER_COMMAND.format(self.project, self.topic_id)
	analyser_folder = Config.getInstance().get('bug_deduper_root')	
        current_dir = os.getcwd()
        try:
            os.chdir(analyser_folder)
            output = subprocess.check_output(shlex.split(command))
            for score in output.split('\n'):
                score = score.strip()
                if score:
                    assert ':' in score, \
                        'you need to update bug-deduplication'
                    bugid, score = score.split(':')
                    yield bugid, float(score)
        finally:
            os.chdir(current_dir)

    @staticmethod
    def recover(data):
        return CustomTopicTask(data['project'], data['topic_id'])

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('project', help='project name')
    parser.add_argument('topic', help='topic id', type=int)

    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    task = CustomTopicTask(args.project, args.topic)
    task.run()
