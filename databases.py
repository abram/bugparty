#  Copyright (C) 2017 Abram Hindle
#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import elasticsearch, elasticsearch.helpers
import sys, os

import common
from config import Config

# important performance related constants

BULK_READ_SIZE = 1000
BULK_WRITE_SIZE = 100

# TODO
PAGE_SIZE = 100 # from static/list_bugs/app.js

class BulkOps(object):
    def __init__(self, client, doc_type):
        self.client = client
        self.doc_type = doc_type
        self.db = client.connect(doc_type)

        self.updates = {}
        self.replacements = {}

    def replace(self, id, doc):
        self.replacements[id] = doc

        if id in self.updates:
            del self.updates[id]

    def update(self, id, doc):
        self.updates[id] = doc

    def apply(self):
        result = self.db.bulk(replacements=self.replacements,
                    updates=self.updates)
        self.updates = {}
        self.replacements = {}

        return result


class ESType(object):
    def __init__(self, index, doc_type, client):
        assert index
        assert doc_type
        self.doc_type = doc_type
        self.index = index
        self.client = client
        self.kwargs = {
            'index': self.index
            ,'doc_type': self.doc_type
        }

    def refresh(self):
        self.client.indices.refresh(self.index)

    def __delitem__(self, key):
        self.client.delete(id=key, ignore=[404], **self.kwargs)

    def __setitem__(self, key, value):
        response = self.client.index(id=str(key), body=value, **self.kwargs)

    def update_doc(self, doc_id, updates):
        body = {'doc': updates, 'doc_as_upsert':True}
        return self.client.update(id=doc_id, body=body, **self.kwargs)

    def update(self, docs):
        return self.bulk(updates=docs)

    def bulk(self, inserts=[], replacements={}, updates={}):
        actions = [{'_op_type': 'index', '_source': body} for body in inserts]
        actions.extend([{'_id': id, '_op_type': 'index','_source': body}
                        for (id, body) in replacements.iteritems()])
        actions.extend([{'_id': id, '_op_type': 'update',
                        'doc_as_upsert': True, 'doc': body}
                        for (id, body) in updates.iteritems()])
        results = elasticsearch.helpers.bulk(self.client,
                actions, chunk_size=BULK_WRITE_SIZE, **self.kwargs)

        if results[1]:
            raise Exception(results[1])
        return True

    def index(self, doc):
        return self.client.index(body=doc, **self.kwargs)

    def __contains__(self, item):
        return self.client.exists(id=item, **self.kwargs)

    def get(self, key, default=None):
        try:
            response = self.client.get(id=key, **self.kwargs)
            return response['_source']
        except ElasticSearch.Error404:
            return default

    def __getitem__(self, key, fields=None):
        key = str(key)
        if fields:
            response = self.client.get(id=key, fields=fields, **self.kwargs)
            return response['fields']
        else:
            response = self.client.get(id=key, **self.kwargs)
            return response['_source']

    def __iter__(self):
        return iter(self.keys())

    def count(self):
        return int(self.client.count(q='*', **self.kwargs)['count'])

    def search(self, body, exclude_topics=True, **user_kwargs):
        kwargs = self.kwargs.copy()
        kwargs.update({'size': 30}) # default query size
        kwargs.update(user_kwargs)

        if exclude_topics:
            kwargs.setdefault('_source_exclude', []).append('topics.*')

        response = self.client.search(body=body, **kwargs)
        return response['hits']['hits']

    # for list_bug_ids
    def scan(self, body, exclude_topics=True, **user_kwargs):
        '''good for very big request like getting all the docs'''
        kwargs = self.kwargs.copy()
        # TODO: See how different sizes affect performance
        kwargs.update({'size': BULK_READ_SIZE}) # BULK_READ_SIZE = 1000
        # kwargs.update({'timeout': 1000})
        # kwargs.update({'from_': 0})
        kwargs.update(user_kwargs)

        if exclude_topics:
            kwargs.setdefault('_source_exclude', []).append('topics.*')

        response = elasticsearch.helpers.scan(self.client, query=body, **kwargs)
        return response

    def get_slice(self, body, exclude_topics=True, **user_kwargs):
        kwargs = self.kwargs.copy()
        kwargs.update(user_kwargs)

        if exclude_topics:
            kwargs.setdefault('_source_exclude', []).append('topics.*')

        response = self.client.search(body=body, **kwargs)
        return response['hits']['hits'], response['hits']['total']

    def mget(self, docids, exclude_topics=True, **user_kwargs):
        '''returns a generator of (id, document) pairs, one for
            each id in docids'''
        kwargs = self.kwargs.copy()
        kwargs.update(user_kwargs)
        if exclude_topics:
            kwargs.setdefault('_source_exclude', []).append('topics.*')

        for batch in common.iter_batches(docids, BULK_READ_SIZE):
            results = self.client.mget({'ids': list(batch)}, **kwargs)
            for r in results['docs']:
                yield r['_id'], r['_source']

    # for list_bug_ids
    def keys(self):
        response = self.scan({
            'query': {
                'match_all': {}
            }
        }, fields=[])

        return sorted([hit['_id'] for hit in response])

    def keys_slice(self, pagenum):
        start = str(int(pagenum) * PAGE_SIZE)

        response, num_bugs = self.get_slice({
            'from': start,
            'size': PAGE_SIZE,
            'query': {
                'match_all': {}
            }
        }, fields=[])

        return sorted([hit['_id'] for hit in response]), num_bugs

    def values(self):
        response = self.scan({
            'query': {
                'match_all': {}
            }
        }, _source=True)

        sorted_hits = sorted(response, key=lambda x: x['_id'])
        return [hit['_source'] for hit in sorted_hits]

    def items(self, **kwargs):
        response = self.scan({
            'query': {
                'match_all': {}
            }
        }, _source=True, **kwargs)

        return [(hit['_id'], hit['_source']) for hit in response]



class ElasticSearch(object):
    Error404 = elasticsearch.exceptions.NotFoundError

    # types
    BUGS = 'bug'
    TOPICS = 'topic'
    GENERATED = 'generated'
    SIMILARITIES = 'similarities'

    
    BUGPARTY_INDEX_NAME = 'bugparty_internal'
    TASKS = 'tasks'
    PROJECTS = 'project'
    FEEDBACK = 'feedback'

    
    def __init__(self, project=None, hosts=None):
        self.project = project
        self.hosts = hosts or [Config.getInstance() \
            .get('es_port_9200_tcp_addr') + ':9200']
        self.client = elasticsearch.Elasticsearch(self.hosts)

    def refresh(self):
        self.client.indices.refresh(self.project)

    def delete_dbs_for_project(self):
        self.client.indices.delete(self.project, ignore=404)

    def create_feedback_db(self):
        self.client.indices.create(ElasticSearch.FEEDBACK, ignore=400)
        
    def create_dbs_for_project(self):
        assert self.project
        self.client.indices.create(self.project, ignore=400)
        bug_mapping = {
            "bug": {
                "_id": {
                  "path": "bugid"
                },
                "_timestamp": {
                  "enabled": True,
                  "path": "openedDate",
                  "format": "dateOptionalTime"
                },
                "properties": {
                  "openedDate": {
                    "type": "date",
                    "format": "dateOptionalTime"
                  }
                }
              }
            }
        self.client.indices.put_mapping(index=self.project,
            doc_type=ElasticSearch.BUGS, body=bug_mapping)
        generated_mapping = {
            "generated" : {
                "properties" : {
                    "data.tar.gz" : {
                        "type" : "string",
                        "index" : "no",
                        "norms" : {
                            "enabled" : False
                        }
                    }
                }
            }
        }
        self.client.indices.put_mapping(index=self.project,
            doc_type=ElasticSearch.GENERATED, body=generated_mapping)

    # for list_bug_ids
    def get_all_bugids(self):
        return ESType(self.project, self.BUGS, self.client).keys()

    def get_some_bugids(self, pagenum):
        return ESType(self.project, self.BUGS, self.client).keys_slice(pagenum)

    def get_all_revs(self, db=None):
        return {id: '' for id in
                ESType(self.project, db, self.client).keys()}

    def get_all_docs(self, db=None, **kwargs):
        db = db or self.BUGS
        return {id: body for (id, body) in
                    ESType(self.project, db, self.client).items(**kwargs)}

    def count_all_docs(self, db=None):
        return ESType(self.project, db).count()

    def connect(self, db):
        return ESType(self.project, db, self.client)

    def connect_to_db(self):
        return self.connect(ElasticSearch.BUGS)

    def connect_to_sim_db(self):
        return self.connect(ElasticSearch.SIMILARITIES)

    def connect_to_topics_db(self):
        return self.connect(ElasticSearch.TOPICS)

    def connect_to_buckets_db(self):
        return self.connect(ElasticSearch.BUCKETS)

    def connect_to_generated_db(self):
        return self.connect(ElasticSearch.GENERATED)

    def connect_to_bugparty_db(self, type_name):
        return ESType(self.BUGPARTY_INDEX_NAME, type_name, self.client)

    def connect_to_feedback_db(self, type_name):
        return ESType(self.FEEDBACK, type_name, self.client)
