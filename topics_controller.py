#  Copyright (C) 2014 Alex Wilson
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
a topic has:
 * a name
 * an id (this is sequentially created when the topic is added)
 * a method (eg. LDA or BM25F)

for all topics, we can ask for
 * a set of words that define the topic (stored in topic-summary and
    topic-summary-{id})
   | LDA: top 25 generated words
   | BM25F: the topic words
 * a name

for LDA topics, we can ask for
 * the association of all tokens in our dictionary to this topic

across all topics, we can ask for
 * the topic words (summaries) and names for all topics
 * the dtm
"""


import re, traceback
import task_queue
from databases import BulkOps, ElasticSearch

import scipy.spatial.distance
import pyflann
import scipy
import numpy

import nltk
from nltk import RegexpTokenizer



class TopicsRun(object):
    def __init__(self, project, client=None):
        if client is None:
            client = ElasticSearch(project)
        self.project = project
        self.client = client


class TopicsLoader(object):
    ''' a combination of summarizer and database updater,
        can be subclassed to add additional data'''

    def __init__(self, run, all_ids, topics):
        ''' topic = list of topics that are being analyzed
            all_ids = list of all document ids in corpus
        '''
        self.run = run
        self.project = self.run.project
        self.all_ids = sorted(all_ids)
        self.topics = topics
        self.partial_dtm = [] # partial document_topic_matrix

    def set_document_topic_associations(self, associations):
        '''adds a new topic score vector (row) to the WIP
            document-topic-matrix

            :param associations: list of topic scores for a document
        '''
        self.partial_dtm.append(associations)

    def merge_document_topic_matrix(self, old_dtm, bugs_in_dtm):
        '''merge self.partial_dtm and old_dtm so that:

          * rows are in the order of self.all_ids
          * self.partial_dtm is assumed to have rows for self.ids
          * the resulting dtm contains info for all of self.ids as well
                as the old data
          * all rows are the same length

        :returns: a generator of document-topic-matrix rows'''

        new_docs = set(self.ids)
        bugs_in_dtm = set(bugs_in_dtm)
        next_old = 0
        next_new = 0

        topic_count = max(max(self.topics) + 1,
                len(old_dtm[0]) if len(old_dtm) else 0)

        for i in self.all_ids:
            result = []
            if i in bugs_in_dtm:
                # result = old_dtm[next_old][:].copy()
                result = old_dtm[next_old][:]
                next_old += 1

            if len(result) < topic_count:
                result.extend([-1] * (topic_count - len(result)))

            if i in new_docs:
                for t, s in zip(self.topics, self.partial_dtm[next_new]):
                    result[t] = s
                next_new += 1
            yield result

    def update_summaries_in_database(self, db=None):
        '''update summaries with newly-inferred stuff'''
        print "Updating Topic Document Map"
        db = db or self.run.client
        topics_updater = BulkOps(db, ElasticSearch.TOPICS)
        bugs_updater = BulkOps(db, ElasticSearch.BUGS)
        topics_db = db.connect_to_topics_db()

        old_dtm, ids_in_dtm = TopicsController(db).get_dtm()
        merged_dtm = self.merge_document_topic_matrix(old_dtm, ids_in_dtm)
        merged_dtm = list(merged_dtm)

        print "updating similar documents"
        similar = {}
        try:
            similar = self.nn(merged_dtm, self.all_ids)
        except:
            print traceback.format_exc()
            print 'failed to calculate nearest neighbour'

        print "Now saving per document similarity and topics"
        must_update = set(self.ids)
        count = 0
        for bug_i in range(len(self.all_ids)):
            id = self.all_ids[bug_i]
            if id not in self.ids:
                continue

            bugs_updater.update(id, {
                'topics': {
                    str(i): merged_dtm[bug_i][i]
                    for i in range(len(merged_dtm[bug_i]))
                }
                ,'similar': similar.get(id, None)
            })
            count = count + 1
            if (count >= 300):
                bugs_updater.apply()
                print 'updated {} bugs'.format(count)
                count = 0

        if (count > 0):
            bugs_updater.apply()

        bugs_updater.apply()
        topics_updater.apply()

    @staticmethod
    def nn(dtm, ids, topn = 25, distance = 'kl'):
        ''' compute nearest neighbors for bugs from document-topic matrix

            :param dtm: document topic matrix
            :param ids: document ids

            :returns: a dict from bugids to list of bug infos
        '''
        pyflann.set_distance_type('kl')
        flann = pyflann.FLANN()
        # TODO: should we do any kind of normalization here?
        # some columns are for LDA, some are for BM25F, so the scores
        # are not exactly comparable

        # force everything to be floats, otherwise we can get
        # floating point exceptions that just kill everything
        dtm = map(lambda x: map(float, x), dtm)
        topn = min(len(dtm), topn)

        result, dists = flann.nn(scipy.array(dtm), scipy.array(dtm), topn,
                log_level="info")#,algorithm='kmeans')
        out = {}
        for ielm in range(0, len(dtm)):
            indices = result[ielm]
            v = dists[ielm]
            ol = [{"id":ids[indices[i]],"i":i,"r":v[i]} for i in range(0,len(indices))]
            out[ids[ielm]] = ol
        return out


class TopicsController(object):
    def __init__(self, client):
        '''Controller for topics for a single project.

        Mostly handles talking to the database to get/create topics and
        related data.

        :param client: database client to use
        :type client: ElasticSearch or similar
        '''
        self.client = client
        self.topics_db = client.connect_to_topics_db()

    def create_topic(self, name, words):
        '''creates a new BM25F topic

        :returns: int -- the topic id
        '''
        if type(words) is list:
            words = ' '.join(words)
        topic_id = self.topics_db.count()
        summary = {
            'name': name
            ,'method': 'BM25F'
            ,'words': words
            ,'topic_id': topic_id
        }
        key = str(topic_id)
        self.topics_db[key] = summary
        return topic_id

    def count_topics(self):
        return self.topics_db.count()

    def get_topics(self):
        '''Get summaries for all topics in this project'''
        return sorted(self.topics_db.values(), key=lambda t: t['topic_id'])

    def get_topic(self, topic_id):
        '''get a single topic'''
        return self.topics_db.get(str(topic_id), None)

    def rename_topic(self, topic_id, name):
        '''rename a topic

        :returns: topic summary
        '''
        topic = self.topics_db[str(topic_id)]
        topic['name'] = name
        self.topics_db[str(topic_id)] = topic

        return topic

    def get_dtm(self):
        '''get the document-topic matrix.

        :returns: 2D array of topic scores, a list of bugids (one per row)
        '''
        bugs = self.client.get_all_docs(ElasticSearch.BUGS,
                exclude_topics=False, _source_include=["topics.*"])
        bug_ids = sorted(bugs.keys())
        bugs = [bugs[bug_id] for bug_id in bug_ids]
        max_topic = -1
        for bug in bugs:
            if 'topics' in bug:
                topics = map(int, bug.get('topics', {}).keys())
                max_topic = max(max_topic, max(topics or [-1]))

        if max_topic == -1:
            return [[] for i in range(len(bug_ids))], bug_ids

        dtm = numpy.zeros(shape=(len(bugs), max_topic + 1))

        topics = zip(range(max_topic + 1), map(str, range(max_topic + 1)))
        for i in range(len(bugs)):
            for topic_i, topic in topics:
                scores = bugs[i].get('topics', {})
                dtm[i][topic_i] = scores.get(topic, 0)
        return dtm, bug_ids

    def normalized_dtm(self):
        '''get the dtm, with per-column normalization

        :returns: 2D array of topic scores.
        '''
        dtm, bug_ids = self.get_dtm()
        dtm = numpy.array(dtm)
        column_sums = dtm.sum(axis = 0)
        for i in range(len(column_sums)):
            if column_sums[i] != 0:
                dtm[i] /= column_sums

        return dtm
