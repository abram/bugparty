#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import json, urllib, urllib2
from uuid import uuid4

from databases import ElasticSearch
from bug import BugListFormatter
from common import HandlerControllerBase, SimpleRouter
from custom_topics import CustomTopicTask
from http import *
from project import Project
from topics_controller import TopicsController

import lda, urls


router = SimpleRouter()
HandlerControllerBase.static_routes(router, {
    '/bugparty/$project/topics/crossfilter': 'crossfilter.html'
    ,'/bugparty/$project/topics/topics.html': 'topics/index.html'
})
class TopicsHandler(HandlerControllerBase):
    def __init__(self, request, task_queue):
        super(TopicsHandler, self).__init__(request, task_queue)

    def route_resolved(self, func, **kwargs):
        ''' hook called before our routes '''
        if 'project' in kwargs:
            self.project = Project(kwargs['project'])
            try:
                self.db = ElasticSearch(self.project.name)
                self.controller = TopicsController(self.db)
            except ElasticSearch.Error404:
                self.abort(404, 'this project does not exist')

    @router.add_route('/bugparty/$project/topics/update', ['POST'])
    def update_topics(self, project):
        self.project.schedule_topics_update(self.task_queue)

    @router.add_route('/bugparty/$project/topics/recreate_lda', ['POST'])
    def recreate_lda_topics(self, project):
        self.project.schedule_recompute(self.task_queue)

    @router.add_route('/bugparty/$project/topics/similar/$bug_id')
    def similar(self, project, bug_id):
        '''get ids and scores of similar bugs'''
        timestamp = self.project.get_timestamp(Project.TOPIC_SCORES_TIMESTAMP)
        if self.should_304(modified=timestamp):
            return respond_with_304(modified=timestamp)

        bugs_db = self.db.connect_to_db()
        bug_info = bugs_db.get(bug_id, {}).get('similar')
        if not bug_info:
            return HTTPError(404, 'this bug has not been analysed')

        formatted_result = BugListFormatter(project).format({
            'bugid': sim['id']
            ,'distance': sim['r']
        } for sim in bug_info)
        return json_response(formatted_result).last_modified(timestamp)


    @router.add_route('/bugparty/$project/topics')
    def topics_index(self, project):
        '''list all topic summaries'''
        timestamp = self.project.get_timestamp(Project.TOPICS_TIMESTAMP)
        if self.should_304(modified=timestamp):
            return respond_with_304(modified=timestamp)

        topics = self.controller.get_topics()
        topics = [self.topic_summary(i, topics[i]) for i in range(len(topics))]
        return json_response(topics).last_modified(timestamp)


    @router.add_route('/bugparty/$project/topics', ['POST'])
    def create_topic(self, project):
        '''create a new topic'''
        topic_data = json.loads(self.request.body)
        topic_id = self.controller.create_topic(topic_data['name'],
                                topic_data['words'])
        self.project.update_timestamps([Project.TOPICS_TIMESTAMP])

        self.task_queue.add_task(CustomTopicTask(self.project.name, topic_id))
        return self.get_topic(project, topic_id)

    @router.add_route('/bugparty/$project/topics/$topic_id', ['GET'])
    def get_topic(self, project, topic_id):
        '''Get the summary of a single topic.'''
        timestamp = self.project.get_timestamp(Project.TOPICS_TIMESTAMP)
        if self.should_304(modified=timestamp):
            return respond_with_304(modified=timestamp)

        topic = self.controller.get_topic(topic_id)
        if topic is None:
            self.abort(404, 'topic {} not found'.format(topic_id))

        return json_response(self.topic_summary(topic_id, topic)) \
                    .last_modified(timestamp)

    @router.add_route('/bugparty/$project/topics/$topic_id', ['PATCH'])
    def change_topic_name(self, project, topic_id):
        '''Change the name of a topic.'''
        name = json.loads(self.request.body)['name']
        topic = self.controller.rename_topic(topic_id, name)
        timestamp = self.project.update_timestamps([Project.TOPICS_TIMESTAMP])

        return json_response(self.topic_summary(topic_id, topic)) \
                    .last_modified(timestamp)

    def topic_summary(self, topic_id, topic):
        '''format topic summary with URI, defaulted name'''
        return {
            'name': topic.get('name') or 'topic {}'.format(topic_id)
            ,'id': topic_id
            ,'location': '/bugparty/{}/topics/{}'.format(self.project.name,
                                                        topic_id)
            ,'method': topic['method']
            ,'words': topic['words']
        }
        
    @staticmethod
    def topic_summary_doc_id(topic_id):
        return 'lda-topic-summary-{}'.format(topic_id)

TopicsHandler.router = router
