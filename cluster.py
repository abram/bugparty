# Copyright (c) 2014 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import csv, os, shutil, tempfile, uuid
from StringIO import StringIO

import common, task_queue
from config import Config
from databases import ElasticSearch
from topics_controller import TopicsController

SCRIPT_PATH = os.path.join(os.getcwd(), 'do_clustering.R')


class UTFEncodingCSVWriter():
    """This class is needed because of some inconvenient python2
       unicode handling. Basically, csv.writer NEEDS utf-8 or ascii.
       So we encode all input as utf-8 to the csv writer."""
    def __init__(self, f):
        self.writer = csv.writer(f)

    def writerow(self, row):
        try:
            self.writer.writerow([self.safe_encode(x) for x in row])
        except UnicodeError:
            # decode windows encoding and try again
            row = [x.decode('cp1252', 'replace') for x in row]
            self.writer.writerow([x.encode('utf-8') for x in row])

    def safe_encode(self, item):
        if hasattr(item, 'encode'):
            return item.encode('utf-8')
        return item

    def writerows(self, rows):
        for r in rows:
            self.writerow(r)


class Clusterer(object):
    def __init__(self, client, doc_id):
        self.client = client
        self.doc_id = doc_id
        self.db = client.connect_to_topics_db()

    def do_lda_cluster(self, clusters, bug_ids):
        controller =  TopicsController(self.client)
        topics = controller.get_topics()
        lda_topics_count = len(filter(lambda x: x['method'] == 'lda',
                                        topics))

        # TODO: would be more efficient to modify get_dtm() to
        #       take a list of bugs for which to build a dtm
        dtm, dtm_bug_ids = controller.get_dtm()
        if bug_ids:
            bug_ids = set(bug_ids)
            dtm = [row for (row, bug_id) in zip(dtm, dtm_bug_ids) \
                if bug_id in bug_ids]
        else:
            bug_ids = set(dtm_bug_ids)


        with common.TempDir() as td:
            print td.path
            with open(td.join('dtm.csv'), 'w') as in_file:
                writer = csv.writer(in_file)
                for row in dtm:
                    writer.writerow(row[:lda_topics_count])

            # TODO: we could do this async while we load documents
            common.run_command([SCRIPT_PATH, td.path, str(clusters)])

            return self.add_results_to_db(td, clusters, bug_ids)

    def get_initial_doc(self, clusters):
        return {
            'id': self.doc_id
            ,'status': 'waiting to run'
            ,'when': common.utc_now().isoformat()
            ,'generator': 'lda-cluster'
            ,'clusters': clusters
        }

    def add_results_to_db(self, run_folder, clusters, bug_ids):
        lines = iter(open(run_folder.join('clusters.txt')))
        next(lines) # skip first line (header)
        cluster_ids = [int(l.strip()) for l in lines if l]

        clusterCSV = StringIO()
        UTFEncodingCSVWriter(clusterCSV).writerows(
                self.join_cluster_data_with_bugs(cluster_ids, bug_ids)
            )

        data = self.get_initial_doc(clusters)
        data.update({
            'when': common.utc_now().isoformat()
            ,'status': 'success'
            ,'cluster.csv': clusterCSV.getvalue()
            ,'silhouette.svg': open(run_folder.join('silhouette.svg')) \
                                    .read()
        })

        db = self.client.connect_to_generated_db()
        db[self.doc_id] = data
        return self.doc_id

    def join_cluster_data_with_bugs(self, cluster_ids, bug_ids):
        bugs = self.client.get_all_docs()
        cluster_ids = iter(cluster_ids)
        for bug_id in sorted(bugs.keys()):
            if bug_id.startswith('_') or bug_id not in bug_ids:
                continue
            bug = bugs[bug_id]
            yield (bug_id, next(cluster_ids), bug['description'])


class ClusterTask(task_queue.Task):
    def __init__(self, project, clusters, bug_ids = None, doc_id = None):
        self.project = project
        self.clusters = clusters
        self.bug_ids = bug_ids
        self.doc_id = doc_id or uuid.uuid4().hex

    def get_initial_doc(self):
        return Clusterer(ElasticSearch(self.project), self.doc_id) \
                    .get_initial_doc(self.clusters)

    def run(self, worker=None, client=None):
        client = client or ElasticSearch(self.project)
        report_db = client.connect_to_generated_db()

        try:
            report_db.update_doc(self.doc_id, {
                'status': 'running'
                ,'when': common.utc_now().isoformat()
                ,'id': self.doc_id
            })
            Clusterer(client, self.doc_id) \
                .do_lda_cluster(self.clusters, self.bug_ids)

        except:
            report_db.update_doc(self.doc_id, {
                'status': 'failed'
                ,'when': common.utc_now().isoformat()
                ,'id': self.doc_id
            })
            raise

    @staticmethod
    def recover(data):
        return ClusterTask(data['project'], data['clusters'],
                    data['bug_ids'], data['doc_id'])
        

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('LDA Clusterer')
    parser.add_argument('project', help='project name')
    parser.add_argument('clusters', type=int, help='number of clusters')

    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    print ClusterTask(args.project, args.clusters).run()
