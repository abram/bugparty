PACKAGES="build-essential libzmq-dev python-zmq couchdb couchdb-bin python-couchdb python-couchdbkit uuid-runtime libuuid1 libjson-xs-perl libwww-perl openjdk-6-jdk vim git maven libboost-program-options-dev curl python-nltk"
sudo apt-get update
sudo aptitude install $PACKAGES
cd /opt
sudo chown root.vagrant /opt
sudo chmod g+rw /opt
git clone https://github.com/rnewson/couchdb-lucene.git
cd couchdb-lucene
mvn && \
tar zxvf target/*SNAPSHOT*.tar.gz && \
ln -s `pwd`/couchdb-lucene-0.9.0-SNAPSHOT  `pwd`/couchdb-lucene
sudo cp /vagrant/local.ini /etc/couchdb/local.ini
sudo cp /vagrant/rc.local /etc/rc.local
sudo /etc/rc.local
sudo /etc/init.d/couchdb restart
# does it work
GET http://localhost:5984
# make bugparty
curl -X PUT http://localhost:5984/bugparty/

