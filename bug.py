#  Copyright (C) 2014 Michelle Naylor
#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from dateutil.parser import parse as parse_date_str
from dateutil.tz import tzoffset


import common, urls, datetime


class Bug(object):
    @staticmethod
    def clean_bug_data(bug_data):
        Bug.fix_nulls(bug_data)

        if 'status' in bug_data:
            bug_data['status'] = bug_data['status'].lower()
        Bug.fix_openedDate(bug_data)

        return bug_data


    @staticmethod
    def fix_openedDate(bug_data):
        date = None
        for date_key in ('openedDate', 'when', 'created_at', 'created'):
            if date_key in bug_data:
                try:
                    date = Bug.fix_date_str(bug_data[date_key])
                    break
                except:
                    pass

        bug_data['openedDate'] = date or datetime.datetime.utcnow().isoformat()
        return bug_data['openedDate']

    @staticmethod
    def fix_nulls(bug_data):
        ''' recursively replace null-like values with real nulls.
            currently these are: 'null', {}
        '''
        for k, v in bug_data.iteritems():
            if bug_data[k] == 'null' or bug_data[k] == {}:
                bug_data[k] = None
            elif type(bug_data[k]) == dict:
                Bug.fix_nulls(bug_data[k])

    @staticmethod
    def fix_date_str(date_string):
        parsed = common.parse_date_str(date_string)
        if parsed.tzinfo is None:
            parsed = parsed.replace(tzinfo=tzoffset(None, 0))
        return parsed.isoformat()


class BugListFormatter(object):
    """formats a list of dicts with HATEOA info and a common format.
        This way we can be sure that every JSON endpoint that returns
        a list of bugs can provide roughly the same output"""

    def __init__(self, project, bug_db=None):
        self.project = project

    def format_ids(self, bug_ids):
        """format a list of bug ids"""
        return self.format({'id': i for i in bug_ids}, 'id')

    def format(self, bugs, id_key='bugid'):
        """format a list of dicts, where each dict represents a bug
            and has a bug id in the field id_key. Any extra info
            on the bug will be copied over to the list."""
        return [self.format_one(b, id_key) for b in bugs]

    def format_one(self, bug_data, id_key):
        bug_id = bug_data[id_key]

        bug = {
            'bugid': bug_id
            ,'location': urls.bug(self.project, bug_id)
        }

        for (k, v) in bug_data.iteritems():
            if k != id_key:
                bug[k] = v
        return bug

    def format_with_total(self, bugs, total_num, id_key='bugid'):
        """format a list of dicts, where each dict represents a bug
            and has a bug id in the field id_key. Any extra info
            on the bug will be copied over to the list."""
        return [self.format_one_with_total(b, total_num, id_key) for b in bugs]

    def format_one_with_total(self, bug_data, total_num, id_key):
        bug_id = bug_data[id_key]

        bug = {
            'bugid': bug_id
            ,'location': urls.bug(self.project, bug_id)
            ,'num_bugs': total_num
        }

        for (k, v) in bug_data.iteritems():
            if k != id_key:
                bug[k] = v
        return bug

class BugVerifier(object):
    """
    Verifies a list of new bugs contains the correct fields, and
    renames fields where necessary.
    """
    def __init__(self):
        pass

    def change_all_attributes(self, all_bugs, attr_names):
        for (new_name, old_name) in attr_names.iteritems():
            print 'json attribute %s changed to %s' %(old_name, new_name)
        return [self.change_attributes(bug, attr_names) for bug in all_bugs]

    def change_attributes(self, bug, attr_names):
        """
        Changes a bug's attribute names.
        Ex. attr_names = {name we want: name given}
        bug_in = {name given: value}
        bug_out = {name we want: value}
        """
        for (new_name, old_name) in attr_names.iteritems():
            if old_name in bug:
                bug[new_name] = bug.pop(old_name)
        return bug
