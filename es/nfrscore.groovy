// context -- the context key
// cv -- an array of the key
// discount -- 
// len = doc['description'].value.size(); return _score * len * discount * threshold + 666;
s = 0;
//if(!doc.containsKey('contextMap')) { return  _score; }
if(!_source.containsKey('contextMap')) { return  _score * 0.1; }
l = _source.contextMap[context];
ab = 0;
a = 0;
b = 0;
for (int i = 0; i < l.size(); i++) {
    ab += l[i] * cv[i];
    a += l[i]*l[i];
    b += cv[i]*cv[i];
}
if (a == 0 || b == 0 || ab == 0) {
    return _score + 0.0;
}
cd = ab / ((a**0.5) * (b**0.5));
return _score + discount * cd;
