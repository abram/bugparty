config: mongrel2.sqlite

mongrel2.sqlite: mongrel2.conf
	m2sh load -db mongrel2.sqlite  -config mongrel2.conf

reload: mongrel2.conf mongrel2.sqlite 
	m2sh reload -db mongrel2.sqlite  -config mongrel2.conf -host localhost

start:	mongrel2.conf mongrel2.sqlite 
	mkdir run || echo lol
	#m2sh start -db mongrel2.sqlite -host 0.0.0.0
	m2sh start -db mongrel2.sqlite -host localhost

start_all: mongrel2.conf mongrel2.sqlite 
	mkdir run || echo lol
	m2sh start -db mongrel2.sqlite -host localhost &
	python2.7 front_controller.py &

dev_server: mongrel2.conf mongrel2.sqlite
	mkdir run
	m2sh start -db mongrel2.sqlite -host localhost &
	python util/dev_server.py &

deps:
	git submodule init
	git submodule update
	cd vowpal_wabbit; make && sudo make install && git clean -fd
	mkdir flann_build
	cd flann_build; cmake ../flann && make && sudo make install && git clean -fd
	cd bug-deduplication; make
	cpan install 'Heap::Simple::XS'



clean:
	find . -name '*.pyc' -delete

bugquery: _design_bugquery.json.installed

_design_bugquery.json.installed: _design_bugquery.json
	curl -X PUT -d @_design_bugquery.json http://localhost:5984/bugparty/_design/bugquery
	touch _design_bugquery.json.installed
