#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import json
import os
from uuid import uuid4
import urllib
import urllib2

import common
from bug import BugListFormatter
from config import Config
from http import json_response, respond_with_304
from project import Project
import urls

def get_fastrep_folder():
    return Config.getInstance().get('fastrep_folder')

def safe_join(*path):
    fastrep_folder = get_fastrep_folder()
    path = os.path.join(fastrep_folder, *path)
    path = os.path.abspath(os.path.expanduser(path))

    if os.path.commonprefix([path, fastrep_folder]) != fastrep_folder:
        raise ValueError('path leaves fastrep folder!')
    return path

# build filenames before chdir
BUGDUP_SEARCH_PATH = os.path.join(os.getcwd(), 'bugDupSearch.sh')
BUGDEDUP_RUN_PATH = os.path.join(os.getcwd(), 'util',
                                'runBugDedup.sh')
BUGDEDUP_RUN_FOLDER = os.getcwd()
FASTREP_RUN_FOLDER = os.path.join(os.getcwd(), '..', 'fastrep/')
FASTREP_RUN_PATH = os.path.join(os.getcwd(), '..', 'fastrep/', 'mklogit.sh')

INSERT_SIMS_RUN_PATH = os.path.join(os.getcwd(), 'util',
                                    'runInsertSimilarities.sh')

from common import HandlerControllerBase, SimpleRouter, ChDir
from databases import ElasticSearch, BulkOps
from task_queue import Task


router = SimpleRouter()
HandlerControllerBase.static_routes(router, {
    '/bugparty/$project/fastrep/matches.html': 'fastrep_list/index.html'
})
class FastrepController(HandlerControllerBase):
    def __init__(self, request, task_queue):
        super(FastrepController, self).__init__(request, task_queue)

    def set_project(self, project):
        self.project = project

    @router.add_route('/bugparty/$project/dupes/update', ['POST'])
    def update_dupes(self, project):
        self.task_queue.add_task(BugDedupTask(project, incremental=True))
        #self.run_fastrep(project)

    @router.add_route('/bugparty/$project/dupes/recreate', ['POST'])
    def redo_dupes(self, project):
        self.task_queue.add_task(BugDedupTask(project, incremental=False))
        #self.run_fastrep(project)

    @router.add_route('/bugparty/$project/context/recreate', ['POST'])
    def redo_context(self, project):
        self.task_queue.add_task(AddContextTask(project, incremental=False))

    @router.add_route('/bugparty/$project/context/update', ['POST'])
    def update_context(self, project):
        self.task_queue.add_task(AddContextTask(project, incremental=True))

    def run_fastrep(self, project):
        # rep and all contexts are treated special
        self.task_queue.add_task(FastREPTask(project, 'REP', 0))
        self.task_queue.add_task(FastREPTask(project, 'ALL', 0, 1))
        self.task_queue.add_task(FastREPTask(project, 'ALL', 1, 1))

        fastrep_contexts = ['architecture', 'lda', 'junk', 'nfr', 'user']
        for context in fastrep_contexts:
            self.task_queue.add_task(FastREPTask(project, context, 0))
            self.task_queue.add_task(FastREPTask(project, context, 1))

        self.task_queue.add_task(InsertSimsTask(project))

    @router.add_route('/bugparty/$project/fastrep/matches/', ['GET'])
    @router.add_route('/bugparty/$project/fastrep/matches/'
                      '$bugid/$context/$rep/$limit', ['GET'])
    def matches(self, project, bugid='1', context='rep', rep='1', limit=25):
        self.project = project

        if bugid == 'undefined':
            return
        if rep == '0':
            context = 'norep_' + context

        mtime = Project(self.project).get_most_recent_timestamp([
                                            Project.DEDUP_TIMESTAMP])
        if self.should_304(modified=mtime):
            return respond_with_304(modified=mtime)

        db = ElasticSearch(project).connect_to_sim_db()
        query = {'query': { 'filtered': {
                     'query': {
                       'term': {'_type': 'similarities'}
                     },
                     'filter' : {
                       'or' : [
                         {'term' : { 'id1' : bugid }},
                         {'term' : { 'id2' : bugid }}
                       ]
                    }}}}

        results = db.search(query, _source_include = ['id1', 'id2', context],
                            size = limit, sort = context + ':desc')

        matchingBugs = []
        for bug in results:
            sim_bugid = str(bug['_source']['id1'])
            if  sim_bugid == bugid:
                sim_bugid = str(bug['_source']['id2'])
            if (context in bug['_source']):
                similarity = bug['_source'][context]
                matchingBugs.append({'bugid': sim_bugid, 'similarity': similarity,
                                 'location': urls.bug(self.project, sim_bugid)})

        return json_response(matchingBugs).last_modified(mtime)


FastrepController.router = router


class BugCleaner(object):
    '''when doing non-incremental, we need to clean out all the data
        that has been precomputed and stored in elastic search'''
    def __init__(self, project):
        self.project = project

    def clean_bugs(self):
        client = ElasticSearch(self.project)
        bulk = BulkOps(client, ElasticSearch.BUGS)

        all_bugs = client.get_all_docs(ElasticSearch.BUGS)
        count = 0

        for bug_id, bug in all_bugs.items():
            self.del_fields(bug, 'ngram1', 'ngram2', 'contextMap')
            bulk.replace(bug_id, bug)
            count += 1

            if (count >= 300):
                bulk.apply()
                print 'cleaned {} bugs'.format(count)
                count = 0

        if (count > 0):
            bulk.apply()
            print 'cleaned {} bugs'.format(count)

    def delete_csv(self):
        try:
            dedup_output = safe_join(self.project + '.csv')
            if os.path.exists(dedup_output):
                os.remove(dedup_output)
        except OSError:
            raise
        print 'deleted deduplication file {}'.format(dedup_output)

    @staticmethod
    def del_fields(bug, *fields):
        for field in fields:
            if field in bug:
                del bug[field]


class BugDedupTask(Task):
    recoverable = True

    def __init__(self, project, incremental=True):
        self.project = project
        self.incremental = incremental

    def run(self, worker=None):
        config = Config.getInstance()
        print 'incremental?', self.incremental
        if not self.incremental:
            BugCleaner(self.project).delete_csv()
            BugCleaner(self.project).clean_bugs()

        # the script needs to run in the bugparty folder
        with ChDir(BUGDEDUP_RUN_FOLDER):
            common.run_command(['mkdir', '-p', safe_join(self.project)])
            output_file = safe_join(self.project + '.csv')
            command = ['bash', BUGDEDUP_RUN_PATH, self.project,
                       config.path('bug_deduper_root'), str(self.incremental)]
            common.run_command(command)
        Project(self.project).update_timestamps([Project.DEDUP_TIMESTAMP])

    @staticmethod
    def recover(self, data):
        return BugDedupTask(data['project'], data['incremental'])


# Task for adding context (no deduping)
class AddContextTask(Task):
    recoverable = True

    def __init__(self, project, incremental=True):
        self.project = project
        self.incremental = incremental

    def run(self, worker=None):
        config = Config.getInstance()
        print 'incremental?', self.incremental
        if not self.incremental:
            BugCleaner(self.project).delete_csv()
            BugCleaner(self.project).clean_bugs()

        # the script needs to run in the bugparty folder
        with ChDir(BUGDEDUP_RUN_FOLDER):
            command = ['bash',
                config.path('bugparty_source_dir') + '/util/runJustContext.sh',
                self.project, config.path('fastrep_folder'), str(self.incremental)]
            common.run_command(command)

    @staticmethod
    def recover(self, data):
        return AddContextTask(data['project'])


class FastREPTask(Task):
    recoverable = True

    def __init__(self, project, column, rep, context = 0):
        self.project = project
        self.column = column
        self.rep = '' if rep == 0 and column != 'ALL'  else str(rep)
        self.context = '' if context == 0 else str(context)

    def run(self, worker=None):
        config = Config.getInstance()

        # the script needs to run in the fastrep folder
        with ChDir(FASTREP_RUN_FOLDER):
            input_file = get_fastrep_folder() + self.project + '.csv'
            if self.rep == '1':
                output_file = os.path.join(get_fastrep_folder(), self.project,
                                           'norep_' + self.column + '.csv')
            else:
                output_file = os.path.join(get_fastrep_folder(), self.project,
                                           self.column + '.csv')
            output_file = output_file.lower()

            if self.column != 'ALL' and self.column != 'REP':
                self.column = 'cosine_' + self.column

            command = ['bash', '-x', FASTREP_RUN_PATH, input_file, output_file,
                       self.column, self.rep, self.context, '0']
            common.run_command(command)

    @staticmethod
    def recover(self, data):
        return FastREPTask(data['project'], data['column'], data['rep'])


class InsertSimsTask(Task):
    recoverable = True

    def __init__(self, project):
        self.project = project

    def run(self, worker=None):
        config = Config.getInstance()

        # the script needs to run in the bugparty folder
        with ChDir(BUGDEDUP_RUN_FOLDER):
            command = ['bash', INSERT_SIMS_RUN_PATH, self.project,
                       config.path('bug_deduper_root'),
                       config.path('fastrep_root')]
            common.run_command(command)

    @staticmethod
    def recover(self, data):
        return InsertSimsTask(data['project'])


if __name__ ==  '__main__':
    import argparse
    parser = argparse.ArgumentParser('bug deduper')
    parser.add_argument('project', help='project name')
    parser.add_argument('--incremental', help='do an incremental analysis',
            action='store_true', default=False)

    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    print 'running bug deduper on {}'.format(args.project)
    BugDedupTask(args.project, args.incremental).run()
