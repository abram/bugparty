#  Copyright (C) 2017 Abram Hindle
#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Corey Hunt
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import datetime, json, time, calendar, random, sys, string


from bug import Bug
from config import Config
from databases import BulkOps, ElasticSearch
from topics_controller import TopicsController

import common, custom_topics, lda_loader


class Feedback(object):

    def __init__(self, name, client=None):
        self.name = name
        self.client = client or ElasticSearch(name)
        self.controller = FeedbackController(type(self.client))

    def delete(self):
        """convenience: just calls FeedbackController.delete"""
        FeedbackController(type(self.client)).delete_project(self.name)

    def get(self):
        db = self.client.connect_to_feedback_db(ElasticSearch.FEEDBACK)
        return db[self.name]

    def update(self, data):
        db = self.client.connect_to_feedback_db(ElasticSearch.FEEDBACK)
        db[self.name] = data



class FeedbackController(object):
    PROJECTS_DOC = ''

    def __init__(self, client_class=ElasticSearch):
        self.client_class = client_class
        self.client = client_class('')
        ElasticSearch().create_feedback_db()

    def list_feedback(self):
        db = self.client.connect_to_feedback_db(ElasticSearch.FEEDBACK)
        feedbacks = db.values()
        return feedbacks

    def get_feedback(self, name):
        return Feedback(name,self.client)

    def make_id(self, project=None):
        now = calendar.timegm(time.gmtime())
        token = "".join([random.choice(string.ascii_uppercase) for i in range(0,24)])
        name = (project or '') + "-" + str(now) + "-" +  token
        return name

    def create_feedback(self, feedback, project, path, query, postbody):
        ElasticSearch(ElasticSearch.FEEDBACK).create_feedback_db()
        db = self.client.connect_to_feedback_db(ElasticSearch.FEEDBACK)
        name = self.make_id(project)
        now = calendar.timegm(time.gmtime())
        db[name] = {
          'name': name,
          'feedback':feedback,
          'when': now,
          'project':project,
          'path':path,
          'query':query,
          'body':postbody
        }
        db.refresh()
        return name
    
    def delete_feedback(self,name):
        self.get_feedback(name).delete()

if __name__ == '__main__':
    '''
A good test case is:

echo $RANDOM | python feedback.py --create --path / --query query --project ddd  
python feedback.py --list
    '''
    import argparse

    parser = argparse.ArgumentParser(description='Manage Bugparty Feedback')
    parser.add_argument('--create', action='store_true',
        help='create the necessary databases for a project use stdin')
    parser.add_argument('--delete', action='store_true',
        help='delete a feedback and its data')
    parser.add_argument('--list', action='store_true',
        help='list feedback')
    parser.add_argument('--path', default="/", help="Path")
    parser.add_argument('--query', default="?", help="Query String")
    parser.add_argument('--feedback', default="negative", help="feedback")
    parser.add_argument('--name', default=None, help="name to delete")
    
    parser.add_argument('--project', default=None,
        help='Limit to a particular project')

    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    if args.create and args.delete:
        print 'not going to create and delete the same feedback..'
        sys.exit(1)

    if args.create:
        print('Reading body from stdin CTRL-D to end')
        data = sys.stdin.read()
        name = FeedbackController().create_feedback(
            args.feedback,
            args.project,
            args.path,
            args.query,
            data
        )
        print json.dumps(FeedbackController().get_feedback(name).get()), 'created'
    if args.delete:
        FeedbackController().delete_feedback(args.name)
        print args.name, 'deleted'
    if args.list:
        print json.dumps(FeedbackController().list_feedback(),indent=1)
