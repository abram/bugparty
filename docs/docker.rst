Running Bug Party in Docker
===========================

..  attention:: This document pertains mostly to using docker as a development
    environment. With a little extra work, however you could use the scripts
    referenced here to deploy docker in production.

After installing `docker <http://docker.io>`_, check out the Bug Party
`docker repo <https://bitbucket.org/michnaylor/docker>`_.
::
        git clone https://bitbucket.org/michnaylor/docker.git

Next, copy your private ssh key to the docker folder, as it will be used to
check out various git repos.

::
        cp ~/.ssh/id_rsa docker/

Before continuing any further, you should clone fastrep, bugparty,
and bug-deduplication next to each other somewhere on your computer.

::
        git clone https://bitbucket.org/abram/bugparty.git some_folder/bugparty
        git clone https://bitbucket.org/michnaylor/bug-deduplication.git some_folder/bug-deduplication
        git clone https://bitbucket.org/abram/fastrep.git --branch elasticsearch some_folder/fastrep

You should then edit the docker/devenv file to set the following variables:
 * database: should be one of 'couchdb' or 'es'
 * username: used while tagging the docker images
 * source_host_dir: should be set to the folder that contains bugparty and
   bug-deduplication (eg. some_folder above).

Once the variables have all been set to your liking, you can bring up the
system by going to the docker directory and running::

    ./devenv build
    ./devenv up
    ./devenv serve tests

You can then run the bugparty test suite in another shell with the following
command::

    ./devenv run nosetests

.. note:: ``./devenv serve tests`` will use util/dev_server.py to run the
    server, as documented in :ref:`dev-server-py`.
