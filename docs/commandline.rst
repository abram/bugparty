CLI commands
============

All of the commands listed below can also be used with ``-h`` to get a help
listing.

Directly Interacting With Bug Party
-----------------------------------

project.py
~~~~~~~~~~

create a new project with all of its databases::

    python project.py --create <project name>
    
load an array of bugs into a project from a .json file::

    python project.py --loadjson <path_to_bugs.json> <project name>

delete a project and all of its databases::

    python project.py --delete <project name>

lda_loader.py
~~~~~~~~~~~~~

after bugs have been added, run LDA analysis::

    python lda_loader.py <project name> --topics <topic count>

after more bugs have been added, run LDA inference for the new bugs::

    python lda_loader.py <project name> --incremental

custom_topics.py
~~~~~~~~~~~~~~~~
rerun a custom topic analysis (eg. after more bugs have been added)::

    python custom_topics.py <project> <topic id (eg. 10)>


bucket_loader.py
~~~~~~~~~~~~~~~~
run the bucket loader (this will update the data for crossfilter)::

    python bucket_loader.py <project>


runBuDedup.sh
~~~~~~~~~~~~~
run the bug deduper over all bugs::

    bash util/runBugDedup.sh <project name> ~/bugparty/fastrep<project name>.csv

cluster.py
~~~~~~~~~~
Cluster bugs by topic scores::

    python cluster.py <project> <number of clusters>


Running the Web UI
--------------------


front_controller.py
~~~~~~~~~~~~~~~~~~~~~~
Run the python server that connects to mongrel and serves the stuff::

    python front_controller.py

Normally, front_controller.py will log exceptions but keep running. If you want
it to die when an exception occurs, you can call::

    python front_controller.py --die

To print out all the REST endpoints without actually running the server, you can
use ::

    python front_controller.py --list

.. _dev-server-py:

util/dev_server.py
~~~~~~~~~~~~~~~~~~

Uses Linux inotifications to watch for changes to .py files and then restarts
front_controller.py when the files change. Requires Linux Kernel 2.6.13 or
newer.::

    python util/dev_server.py
