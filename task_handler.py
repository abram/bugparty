#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import json, urllib, urllib2

from itertools import chain, ifilterfalse

from mongrel2 import handler
from uuid import uuid4


from common import HandlerControllerBase, SimpleRouter


router = SimpleRouter()
HandlerControllerBase.static_routes(router, {
    '/bugparty/tasks.html': 'tasks/index.html'
})
class TaskHandler(HandlerControllerBase):
    def __init__(self, request, task_queue):
        super(TaskHandler, self).__init__(request, task_queue)
    
    @router.add_route('/bugparty/tasks')
    def index(self):
        """list all tasks"""
        return self.task_queue.get_tasks()

    @router.add_route('/bugparty/tasks/clear', ['POST'])
    def clear_queue(self):
        """clear all tasks"""
        self.task_queue.clear_tasks()
        return ''

    @router.add_route('/bugparty/tasks/at/$task_i')
    def display(self, task_i):
        """display a task"""
        i = int(task_i)
        try:
            return self.task_queue.get_tasks()[i]
        except IndexError:
            self.abort(404, "couldn't find task at " + task_i)

    @router.add_route('/bugparty/tasks/errors')
    def list_errors(self):
        """list all errors"""
        return self.task_queue.get_errors()

    @router.add_route('/bugparty/tasks/errors/clear', ['POST'])
    def clear_errors(self):
        """clears all errors"""
        return self.task_queue.clear_errors()

TaskHandler.router = router
