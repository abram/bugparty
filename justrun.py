#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import datetime, json


from bug import Bug
from config import Config
from databases import BulkOps, ElasticSearch
from topics_controller import TopicsController

import common, custom_topics, lda_loader, sys


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Manage Bugparty projects')

    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    sys.exit(1)
