#  Copyright (C) 2014 Alex Wilson
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from urllib import quote


def _format(pattern, *args):
    return pattern.format(*map(quote, map(str, args)))

def bugs(project):
    return _format('/bugparty/{}/{}/bugs/', project, pagenum)

def bug(project, bug_id):
    return _format('/bugparty/{}/bugs/{}', project, bug_id)

def bug_topic_similar(project, bug_id):
    return _format('/bugparty/{}/topics/similar/{}', project, bug_id)

def bug_text_similar(project, bug_id):
    return _format('/bugparty/{}/similar_text/{}', project, bug_id)

def alldates(project):
    return _format('/bugparty/{}/topics/alldates', project)

def reports(project):
    return _format('/bugparty/{}/reports', project)

def lda_report(project, report_id):
    return _format('/bugparty/{}/reports/lda/{}', project, report_id)

def lda_report_file(project, report_id, file):
    return _format('/bugparty/{}/reports/lda/{}/{}', project, report_id, file)

def cluster_report(project, report_id):
    return _format('/bugparty/{}/reports/cluster/{}', project, report_id)

def cluster_csv(project, report_id):
    return _format('/bugparty/{}/reports/cluster/{}/cluster.csv',
                    project, report_id)

def cluster_svg(project, report_id):
    return _format('/bugparty/{}/reports/cluster/{}/silhouette.svg',
                    project, report_id)

def fastrep_matches(project, bug_id, context, rep, count = 25):
    return _format('/bugparty/{}/fastrep/matches/{}/{}/{}/{}',
                    project, bug_id, context, rep, count)
