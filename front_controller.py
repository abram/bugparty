#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



import itertools, json, logging, os, signal, sys, traceback, textwrap

from mongrel2 import handler

from config import Config

from bugparty_handler import BugPartyController
from fastrep_handler import FastrepController
from report_handler import ReportHandler
from task_handler import TaskHandler
from topics_handler import TopicsHandler

import startup_tasks

# HACK to make sure that all Task classes are loaded by the time
# our TaskQueue is made
import lda, lda_loader

HANDLERS = [BugPartyController, TopicsHandler, FastrepController,
            TaskHandler, ReportHandler]


from common import SimpleRouter
from http import *
from task_queue import TaskQueue


class FrontController(object):
    '''FrontController handles interfacing with mongrel. It distributes
        requests to the correct handler and deals with errors as well.
    '''

    def __init__(self, conn):
        self.conn = conn
        self.task_queue = TaskQueue()

    def loop(self, die=False):
        '''handle mongrel requests forever. If ``die == True`` then this
        instance will just die'''

        self.task_queue.run()
        startup_tasks.add_startup_tasks_to_queue(self.task_queue)

        while True:
            print "WAITING FOR REQUEST"

            req = self.conn.recv()
            print "I got something!"

            if req.is_disconnect():
                logging.info('disconnect')
                print "DISCONNECT"
                continue

            req_method = req.headers['METHOD']

            req_ip = req.headers.get('x-forwarded-for')
            try:
                result = self.dispatch(req)
            except HTTPError as he:
                result = he.as_response()
            except Exception as e:
                formatted_tb = traceback.format_exc()
                print formatted_tb
                print 'route was', req.path
                result = HTTPError(503, "Failure" + '\n' + formatted_tb) \
                            .as_response()
                if die:
                    result.reply(self.conn, req)
                    logging.error('%s %s %s %s', req_method, req.path,
                                req_ip, result.get_body())
                    raise

            logging.info('%s %s %s %s', req_method, req.path, req_ip,
                            result.code)
            result.reply(self.conn, req)

    def get_handler(self, path, method):
        '''gets the correct handler for the given path and method.

        :param method: an http method string, eg. "GET"
        :returns: a HandlerControllerBase subclass
        '''
        for handler in HANDLERS:
            if handler.router.route(path, method)[1] is not None:
                return handler

    def dispatch(self, req):
        '''dispatch a mongrel request to the correct handler, and reply
          to mongrel with the result.'''
        method = req.headers['METHOD']
        if method == 'OPTIONS':
            supported = set()
            for handler in HANDLERS:
                supported |= set(handler.router.allows(req.path))
            if supported:
                return HTTPResponse(200, '').allow_methods(list(supported))

        target = self.get_handler(req.path, method)
        if target:
            result = target(req, self.task_queue).respond()
            result = HTTPResponse.wrap(result)
        else:
            result = HTTPError(404,
                "You did not enter a valid command ({})".format(req.path))
            print result.message
            result = result.as_response()
        return result


def dump_endpoints():
    '''prints a list of method/route/endpoints to stdout'''
    def sort_key(rme):
        route, method, endpoint = rme
        return (endpoint.__code__.co_filename, route, method)

    endpoints = list(itertools.chain(*[h.router.list() for h in HANDLERS]))
    endpoints = sorted(endpoints, key=sort_key)

    doc_string_formatter = textwrap.TextWrapper(initial_indent="\n\t\t| ",
                                    width=60, subsequent_indent="\t\t| ")
    for e in endpoints:
        route, method, endpoint = e
        doc_string = textwrap.dedent(endpoint.__doc__ or '')
        if doc_string:
            doc_string = doc_string_formatter.fill(doc_string)
        print '{}\t{}{}\n\t\t{}() {}:{}\n'.format(method, route,
                doc_string,
                endpoint.__code__.co_name,
                endpoint.__code__.co_filename,
                endpoint.__code__.co_firstlineno)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('BugParty front controller')
    parser.add_argument('--die', action='store_true',
                        help="die on exceptions")
    parser.add_argument('--list', action='store_true',
                        help='list all endpoints')
    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    if args.list:
        dump_endpoints()
        exit()

    os.chdir(config.workdir)

    # Add stop words to correct directory, so they can be filtered out during topic creation
    os.system('cp /src/bugparty/stop_words /var/lib/bugparty/stop_words')

    # Start cron
    os.system('service cron start')

    sender_id = 'f772c984-329f-425f-ac53-a3737ea594ef'
    conn = handler.Connection(sender_id, "tcp://127.0.0.1:9997",
        "tcp://127.0.0.1:9996")

    def sigterm_handler(*args):
        # close zmq context
        print 'caught SIGTERM'; sys.stdout.flush()
        handler.CTX.destroy()
        print 'sys.exit now'; sys.stdout.flush()
        sys.exit(0)

    signal.signal(signal.SIGTERM, sigterm_handler)

    controller = FrontController(conn)
    controller.loop(die=args.die)
