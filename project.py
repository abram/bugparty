#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import datetime, json


from bug import Bug
from config import Config
from databases import BulkOps, ElasticSearch
from topics_controller import TopicsController

import common, custom_topics, lda_loader


class Project(object):
    TOPIC_SCORES_TIMESTAMP = 'topic-scores'
        # records most recent topic scores update
    TOPICS_TIMESTAMP = 'topics'
        # records most recent topics modification
    BUCKETS_TIMESTAMP = 'buckets'
    BUGS_TIMESTAMP = 'bugs' # updated when bugs are added
    DEDUP_TIMESTAMP = 'dedup' # updated when deduper is run

    def __init__(self, name, client=None):
        self.name = name
        self.client = client or ElasticSearch(name)
        self.controller = ProjectsController(type(self.client))

    def delete(self):
        """convenience: just calls ProjectsController.delete"""
        ProjectsController(type(self.client)).delete_project(self.name)

    def schedule_recompute(self, task_queue):
        # TODO: we should not just assume topic_count is 100
        task_queue.add_task(lda_loader.LDATask(self.name, topic_count=100))

    def schedule_topics_update(self, task_queue):
        topics = TopicsController(self.client).get_topics()
        custom_topic_ids = [i for i in range(len(topics)) \
                            if topics[i]['method'] == 'BM25F']
        task_queue.add_task(lda_loader.LDAIncrementalTask(self.name))
        for t in custom_topic_ids:
            task_queue.add_task(custom_topics.CustomTopicTask(self.name, t))

    def get_project_data(self):
        db = self.client.connect_to_bugparty_db(ElasticSearch.PROJECTS)
        return db[self.name]

    def update_project_data(self, data):
        db = self.client.connect_to_bugparty_db(ElasticSearch.PROJECTS)
        db[self.name] = data

    def update_timestamps(self, timestamp_names, when=None):
        '''set a timestamp to now in UTC'''
        when = when or common.utc_now().replace(microsecond=0)
        project = self.get_project_data()
        if project is None:
            raise ValueError('project "{}" does not exist!'.format(self.name))

        timestamps = project.get('times', {})
        for timestamp_name in timestamp_names:
            timestamps[timestamp_name] = when.isoformat()

        project['times'] = timestamps
        self.update_project_data(project)
        return when

    def get_most_recent_timestamp(self, names):
        '''get the most recent timestamp of a list, or one hour ago,
            if none of the timestamps are set'''
        project_data = self.get_project_data()
        times = project_data.get('times', {})

        # return most recent date after filtering out missing/malformed
        stamps = map(common.parse_date_str, filter(None, map(times.get, names)))
        if stamps:
            return max(stamps)

        # if there are no timestamps, we will try to provide some caching
        # anyway. If the data is updated, then presumably the timestamps will
        # also be updated, so this fallback should be ok.
        return common.utc_now() + datetime.timedelta(hours=-1)

    def get_timestamp(self, timestamp_name):
        '''get a timestamp'''
        return self.get_most_recent_timestamp([timestamp_name])

    def load_bugs(self, bugs):
        bulk = BulkOps(self.client, ElasticSearch.BUGS)

        print 'cleaning bugs'
        for bug in bugs:
            Bug.clean_bug_data(bug)
            bulk.replace(bug['bugid'], bug)

        print 'bugs cleaned, starting bulk update'
        bulk.apply()

        self.update_timestamps([Project.BUGS_TIMESTAMP])


class ProjectsController(object):
    PROJECTS_DOC = 'projects'

    def __init__(self, client_class=ElasticSearch):
        self.client_class = client_class
        self.client = client_class('')

    def list_projects(self):
        db = self.client.connect_to_bugparty_db(ElasticSearch.PROJECTS)
        projects = db.values()
        return projects

    def create_project(self, name):
        db = self.client.connect_to_bugparty_db(ElasticSearch.PROJECTS)

        ElasticSearch(name).create_dbs_for_project()
        db[name] = {'name': name}
        db.refresh()

    def delete_project(self, name):
        self.client_class(name).delete_dbs_for_project()
        projects_db = self.client.connect_to_bugparty_db(ElasticSearch.PROJECTS)
        del projects_db[name]
        projects_db.refresh()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Manage Bugparty projects')
    parser.add_argument('project', help='the project name')
    parser.add_argument('--create', action='store_true',
        help='create the necessary databases for a project')
    parser.add_argument('--delete', action='store_true',
        help='delete a project and its data')
    parser.add_argument('--loadjson', action='store',
        help='load an array of bugs from a .json file')

    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    if args.create and args.delete:
        print 'not going to create and delete the same project..'
        sys.exit(1)

    if args.create:
        ProjectsController().create_project(args.project)
        print args.project, 'created'
    if args.delete:
        ProjectsController().delete_project(args.project)
        print args.project, 'deleted'

    if args.loadjson:
        with open(args.loadjson) as f:
            data = json.load(f)
            Project(args.project).load_bugs(data)
