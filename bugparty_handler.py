#  Copyright (C) 2014 Michelle Naylor
#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012 Abram Hindle
#  Copyright (C) 2012 Corey Hunt
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import json, urllib, urllib2, urlparse, email, re

from itertools import ifilter, chain, ifilterfalse, izip_longest

from mongrel2 import handler
from uuid import uuid4

from bug import Bug, BugListFormatter, BugVerifier
from common import HandlerControllerBase, SimpleRouter, safe_decode_str
from config import Config
from databases import ElasticSearch
from http import *
from project import Project, ProjectsController
import urls
from feedback import Feedback, FeedbackController

router = SimpleRouter()
HandlerControllerBase.static_routes(router, {
    '/bugparty/$project/bugs/new.html': 'new_bug/index.html'
    ,'/bugparty/$project/bugs/newMult.html': 'new_bug_mult/index.html'
    ,'/bugparty/$project/$pagenum/bugs.html': 'list_bugs/index.html'
    ,'/bugparty/$project/bugs/$bugid/similar.html': 'view_bug/index.html'
    ,'/bugparty/projects.html': 'projects/index.html'
    ,'/bugparty': 'index.html'
})
class BugPartyController(HandlerControllerBase):
    def __init__(self, request, task_queue):
        super(BugPartyController, self).__init__(request, task_queue)

    def route_resolved(self, *args, **kwargs):
        self.project = kwargs.get('project')

    @router.add_route('/bugparty/projects/')
    def list_projects(self):
        return ProjectsController().list_projects()

    @router.add_route('/bugparty/$project', ['POST'])
    def create_project(self, project):
        return ProjectsController().create_project(project)

    @router.add_route('/bugparty/$project', ['DELETE'])
    def delete_project(self, project):
        return Project(project).delete()

    @router.add_route('/bugparty/$project/display/$bug_id')
    def display(self, project, bug_id):
        """retrieve a bug directly from elasticsearch (deprecated)"""
        bug = self.get_bug_or_404(bug_id)
        return bug

    @router.add_route('/bugparty/$project/$pagenum/bugs', ['GET'])
    def list_bug_ids(self, project, pagenum):
        # print(pagenum)
        timestamp = Project(project).get_timestamp(Project.BUGS_TIMESTAMP)
        if self.should_304(modified=timestamp):
            return respond_with_304(modified=timestamp)

        some_bugs, total_num_bugs = ElasticSearch(project).get_some_bugids(pagenum)

        bugids = BugListFormatter(project).format_with_total(({
                'bugid': i
            } for i in some_bugs), total_num_bugs)

        return json_response(bugids).last_modified(timestamp)


    @router.add_route('/bugparty/$project/bugs', ['POST'])
    def add_new_bug(self, project):
        """create a new bug"""
        content_type = self.request.headers['content-type']
        if content_type.startswith('multipart/form-data'):
            self.add_mult_new_bugs(project)

        else:
            self.add_bug_to_db(project, json.loads(self.request.body))

    def add_mult_new_bugs(self, project):
        multipart_request = HttpMultipartRequest(self.request.headers,
                                                 self.request.body)
        multipart_request.parse_body()
        new_bugs = json.loads(multipart_request.data['file']['payload'])

        bug_attributes = multipart_request.data['bug']['payload']

        if bug_attributes != 'undefined':
            verifier = BugVerifier()
            bug_attributes = json.loads(bug_attributes)
            new_bugs = verifier.change_all_attributes(new_bugs, bug_attributes)

        for bug in new_bugs:
            self.add_bug_to_db(project, bug)

    def add_bug_to_db(self, project, bug):
        """create a new bug"""
        bug_data = bug
        bug_id = str(bug_data['bugid'])
        Bug.clean_bug_data(bug_data)
        db = ElasticSearch(project).connect_to_db()
        db[bug_id] = bug_data

        print 'Bug %s added to %s' %(bug_id, project)
        return HTTPResponse(201, bug_id);

    @router.add_route('/bugparty/$project/bugs/$bug_id')
    def display_bug(self, project, bug_id):
        """retrieve a bug"""
        bug = self.get_bug_or_404(bug_id)

        if '_id' in bug: del bug['_id']
        if '_rev' in bug: del bug['_rev']

        # HATEOAS
        bug['location'] = urls.bug(project, bug_id)
        bug['similar_by_topics'] = urls.bug_topic_similar(project, bug_id)
        bug['similar_text'] = urls.bug_text_similar(project, bug_id)
        return bug

    def interleave_text_search(self, title, desc, limit=None):
        by_title = list(self.es_bug_query('title', title, limit))
        # etag = by_title['etag']
        # if self.should_304(etag=etag):
        #    return respond_with_304(etag=etag)

        by_description = list(self.es_bug_query('description', desc, limit))

        scores = {bug['_id']: {'title_score': bug['_score']}
                    for bug in by_title}
        for bug in by_description:
            s = scores.get(bug['_id'], {})
            s['desc_score'] = bug['_score']
            scores[bug['_id']] = s

        # get interleaved ranking
        zipped = izip_longest(by_title, by_description)
        bug_ranking = (b['_id'] for b in chain.from_iterable(zipped) if b)

        # now filter out the dupes
        bugs_seen = set()
        filtered_ranking = []

        for bug_id in ifilterfalse(bugs_seen.__contains__, bug_ranking):
            bugs_seen.add(bug_id)
            filtered_ranking.append(bug_id)

        result = BugListFormatter(self.project).format({
            'bugid': bug_id
            ,'title_score': scores[bug_id].get('title_score')
            ,'desc_score': scores[bug_id].get('desc_score')
        } for bug_id in filtered_ranking)

        bugs_timestamp = Project(self.project) \
                            .get_timestamp(Project.BUGS_TIMESTAMP)
        #if extras:
        #    for k in extras:
        #        result[k] = extras[k]
        return json_response(result) \
                .last_modified(bugs_timestamp)
                # .etag(etag)
                
    @router.add_route('/bugparty/$project/similar_text/$limit/$bug_id')
    @router.add_route('/bugparty/$project/similar_text/$bug_id')
    def similarText(self, project, bug_id, limit=None):
        """search for bugs with similar title and description"""
        db = ElasticSearch(project).connect_to_db()
        bug = self.get_bug_or_404(bug_id)
        path = '/bugparty/%s/similar_text/%s' % (project, bug_id)
        return self.interleave_text_search(bug['title'], bug['description'],limit)

    @router.add_route('/bugparty/$project/similar_title/$bug_id')
    def similarTitle(self, project, bug_id):
        """ Builds a query from the bug's title, and
            returns similar bugs from the database."""
        bug = self.get_bug_or_404(bug_id)
        searchterm = self.clean_query(bug['title'])
        return self.bug_query('title', searchterm)

    @router.add_route('/bugparty/$project/similar_description/$bug_id')
    def similarDescription(self, project, bug_id):
        """builds a query from the bug's description, and
            returns similar bugs from the database"""
        bug = self.get_bug_or_404(bug_id)
        searchterm = self.clean_query(bug['description'])
        return self.bug_query('description', searchterm)

    @router.add_route('/bugparty/$project/similar_description_context/$context/$bug_id')
    def similarDescriptionContext(self, project, context, bug_id):
        """builds a query from the bug's description, and
            returns similar bugs from the database"""
        bug = self.get_bug_or_404(bug_id)
        if (not 'contextMap' in bug):
            return self.similarDescription(project, bug_id)
        searchterm = self.clean_query(bug['description'])
        if (not context in bug['contextMap']):
            return self.similarDescription(project, bug_id)
        cv = bug['contextMap'][context]
        return self.context_bug_query('description', searchterm,context,cv)

    @router.add_route('/bugparty/$project/search_title/$searchterm')
    def searchTitle(self, project, searchterm):
        """search bugs by title"""
        return self.bug_query('title', searchterm)

    @router.add_route('/bugparty/$project/search_description/$searchterm')
    @router.add_route('/bugparty/$project/search_description')
    def searchDescription(self, project, searchterm=None):
        """search bugs by description"""
        query_string = self.request.headers.get('QUERY', '')
        searchterm = urlparse.parse_qs(query_string) \
                        .get('q', [searchterm])[0]
        if not (searchterm or '').strip():
            return json_response([])
        return self.bug_query('description', searchterm)

    @router.add_route('/bugparty/$project/search_text/$limit/$searchterm')
    @router.add_route('/bugparty/$project/search_text/$searchterm')
    @router.add_route('/bugparty/$project/search_text')
    def searchText(self, project, searchterm=None,limit=None):
        query_string = self.request.headers.get('QUERY', '')
        searchterm = urlparse.parse_qs(query_string) \
                        .get('q', [searchterm])[0]
        if not (searchterm or '').strip():
            return json_response([])
        pos = self.make_feedback_uri("positive", project, self.request.headers.get('URI'))
        neg = self.make_feedback_uri("negative", project, self.request.headers.get('URI'))    
        return self.interleave_text_search(searchterm, searchterm, limit)

    def es_bug_query(self, query_field, query_data, limit=None):
        ## Modify query data if it contains tags or paths
        # Tags: Escape braces and quote as a phrase
        def replace_braces(tag):
            new_tag = '"\\' + tag.group(0)[:-1] + '\\]"'
            return new_tag
        query_data = re.sub("\[.*?\]", replace_braces, query_data, re.I)

        # Paths: Split paths and add extra tokens
        for path in re.findall("((?:[a-zA-Z.]*\/+[a-zA-Z.]*)+)", query_data, re.I):
            path_words = path.strip('/').split('/')
            for pw in path_words:
                query_data = query_data + ' ' + pw
        # print query_data

        db = ElasticSearch(self.project).connect_to_db()
        myquery = {
            'fuzzy_like_this_field': {
                query_field: {
                    'like_text': query_data
                    ,'max_query_terms': 250
                }
            }
        }            
        if limit:
            my_filter = "missing"
            if limit == "closed":
                my_filter = "exists"
            # note we are hard coding the closedOn field here
            myquery = {
                "filtered": {
                    'query': myquery,
                    "filter":{ my_filter : { "field" : "closedOn" } }
                }
            }
            
        result = db.search({
            'query': myquery
        }, size=25)
        return result

    def es_context_bug_query(self, query_field, query_data, context, cv):
        db = ElasticSearch(self.project).connect_to_db()
        result = db.search({
            'query': {
                "function_score": {
                    'query': {
                        'fuzzy_like_this_field': {
                            query_field: {
                                'like_text': query_data
                                ,'max_query_terms': 250
                            }
                        }
                    },
                    "functions": [
                      {"script_score": {
                          "script": "nfrscore",
                          "params": {
                              "context":"NFR",
                              "cv":cv,
                              "discount": 1.0
                          }
                        }
                      }
                    ]
                }
            }
        }, size=25)
        return result

    def get_bug_or_404(self, bug_id):
        try:
            bug = ElasticSearch(self.project).connect_to_db().get(bug_id)
            if bug is None:
                self.abort(404, 'bug {} not found'.format(bug_id))
            return bug
        except ElasticSearch.Error404:
            self.abort(404, 'project {} does not exist'.format(project))

    def bug_query(self, field, data):
        result = self.es_bug_query(field, data)
        # TODO: what caching info does elasticsearch give us?
        # etag = result['etag']
        # if self.should_304(etag=etag):
        #   return respond_with_304(etag=etag)

        formatted = BugListFormatter(self.project).format({
            'score': r['_score']
            ,'bugid': r['_id']
        } for r in result)
        return json_response(formatted)

    def context_bug_query(self, field, data, context, cv):
        result = self.es_context_bug_query(field, data, context, cv)
        formatted = BugListFormatter(self.project).format({
            'score': r['_score']
            ,'bugid': r['_id']
        } for r in result)
        return json_response(formatted)

    def make_feedback_uri(self,feedback_type, project, path):
        return '{scheme}://{host}/bugparty/feedback/{feedback}/{project}/{path}'.format(
            scheme=self.request.headers.get('URL_SCHEME'),
            host=self.request.headers.get('host'),
            feedback=feedback_type,
            project=project,
            path=path
        )
    
    @router.add_route('/bugparty/submit_feedback/$feedback/$project/$path',['POST','GET'])
    @router.add_route('/bugparty/submit_feedback/$feedback/$project/',['POST','GET'])
    def submit_feedback(self, feedback, project, path=None):
        query_string = self.request.headers.get('QUERY', '')
        query = query_string
        path = (path or '').strip()
        postbody = self.request.body
        # print json.dumps(self.request.headers)
	name = FeedbackController().create_feedback(feedback, project, path, query, postbody)
        return json_response(
            {"msg":"Feedback recorded",
             "uri":"{scheme}://{host}/bugparty/feedback/{name}".format(
                 scheme=self.request.headers.get("URL_SCHEME"),
                 host=self.request.headers.get("host"),
                 name=name
             ),
             "name":name
            })
    
    @router.add_route('/bugparty/feedback/$name',['GET'])
    def show_feedback(self, name):
        return json_response(
            FeedbackController().get_feedback(name).get()
        )

    @router.add_route('/bugparty/feedback/positive',['GET'])
    def list_positive_feedback(self):
        return json_response(
            FeedbackController().list_feedback()
        )

    @router.add_route('/bugparty/feedback/negative',['GET'])
    def list_negative_feedback(self):
        return json_response(
            FeedbackController().list_feedback()
        )


    

BugPartyController.router = router
