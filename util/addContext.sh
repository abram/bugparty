# break on errors
set -e

PROCESSOR=${1:-"android"}
BUG_DEDUP_ROOT=${2:-"$HOME/bug-deduplication"}

FILE="$PROCESSOR.csv"

echo "$INCREMENTAL"

cd $BUG_DEDUP_ROOT

export CLASSPATH=./:./NFR.EXP2:./lib/*:/usr/share/java/junit-4.11.jar:.:/usr/share/java/hamcrest-core-1.3.jar:.:./issues:./tests

bash util/runJustContext.sh "$PROCESSOR"
