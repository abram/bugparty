# break on errors
set -e

PROJECT=${1:-"android"}
BUG_DEDUP_ROOT=${2:-"$HOME/bug-deduplication"}
FASTREP_ROOT=${3:-"$HOME/fastrep"}
DATA_ROOT = ${4:-"/csv_files/"}

cd $BUG_DEDUP_ROOT

export CLASSPATH=./:./NFR.EXP2:./lib/*:/usr/share/java/junit-4.11.jar:.:/usr/share/java/hamcrest-core-1.3.jar:.:./issues:./tests

java InsertSimsOneUpdate "$PROJECT"
