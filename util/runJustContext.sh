# break on errors
set -e

PROCESSOR=${1:-"android"}
CSV_FILES=${2:-"/csv_files"}
INCREMENTAL=${3:-"False"}

FILE="$PROCESSOR.csv"
BUG_DEDUP_ROOT="/src/bugparty/bug-deduplication"

make

cd $BUG_DEDUP_ROOT

export CLASSPATH=./:./NFR.EXP2:./lib/*:/usr/share/java/junit-4.11.jar:.:/usr/share/java/hamcrest-core-1.3.jar:.:./issues:./tests

if [ "$INCREMENTAL" = "True" ]; then
  echo "Calculate just context, incrementally"
  java -Xmx2048m -ea Main -j --project="$PROCESSOR" --output=$CSV_FILES 1
else
  echo "Calling bug-dedup no sample and no comparison"
  java -Xmx2048m -ea Main -j --project="$PROCESSOR" --output=$CSV_FILES
fi
