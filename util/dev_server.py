# usage: python util/dev_server.py <front_controller options>
# this restarts front controller every time a .py file changes
#
# based partly on https://github.com/joh/when-changed
# (which is BSD licensed)
#
# here is the license for that:
#Copyright (c) 2011, Johannes H. Jensen.
#All rights reserved.
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions
#are met:
#    * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#    * The names of the contributors may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
#            TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
#            OR
#            PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
#    OF
#    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#            NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import os, pyinotify, subprocess, sys


class PyWatcher(pyinotify.ProcessEvent):
    COMMAND = 'python front_controller.py'.split() + sys.argv[1:]

    def __init__(self):
        self.process = None

    def process_IN_CLOSE_WRITE(self, event):
        if os.path.splitext(event.pathname)[1] == '.py':
            self.run()

    def process_IN_MOVED_TO(self, event):
        if os.path.splitext(event.pathname)[1] == '.py':
            self.run()

    def run(self):
        if self.process:
            print 'killing front_controller'
            self.process.terminate()
            self.process.wait()
        self.process = subprocess.Popen(self.COMMAND)
        print self.COMMAND, '->', self.process.pid

    def start(self):
        self.run()

        wm = pyinotify.WatchManager()
        notifier = pyinotify.Notifier(wm, self)

        wm.add_watch(os.getcwd(),
                pyinotify.IN_CLOSE_WRITE | pyinotify.IN_MOVED_TO)
        notifier.loop()

PyWatcher().start()
