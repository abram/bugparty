cd /tmp
mkdir android-jsons
cd android-jsons
curl http://softwareprocess.es/a/android-bugs-jsons.tar.gz -o android-bugs-jsons.tar.gz 
tar zxvf android-bugs-jsons.tar.gz 
cd android-bugs-jsons
# make the DB 
curl -X PUT http://localhost:5984/android/
for file in *.json; do ID=`echo $file | sed -e 's/.json//'`; curl -X PUT -d @${ID}.json http://localhost:5984/android/$ID; done
cd ..
python util/clean_bugs.py android
curl -X PUT http://localhost:5984/bugparty_internal
curl -X PUT http://localhost:5984/bugparty_internal/projects -d '{"projects":[{"name":"android"}]}'
