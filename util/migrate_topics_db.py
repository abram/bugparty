from databases import CouchDB
from couchdb import Server
import re

topic_doc = re.compile('lda-topic-\d+')
topic_summary_doc = re.compile('lda-topic-summary-\d+')

# renames lda documents and stuff as we transition away from LDA-only

def rename_for_project(project):
    topics_db = CouchDB().connect_to_topics_db(project)

    bug_ids = []
    topic_docs = []

    skip_docs = ['lda-words', 'lda-topics', 'lda-similar-documents']

    for doc_id in topics_db:
        if topic_doc.match(doc_id) or topic_summary_doc.match(doc_id):
            topic_docs.append(doc_id)
        elif doc_id in skip_docs:
            pass
        elif not doc_id.startswith('_'):
            # this is a bug
            bug_ids.append(doc_id)

    topics_db['lda-analyzed-bugs'] = {'bug_ids': bug_ids}

    for doc_id in topic_docs:
        update_and_move(topics_db, doc_id, method='lda')

    print 'all topics updated'

    for doc in ['lda-topic-map', 'lda-topic-matrix', 'lda-topic-summary']:
        move(topics_db, doc)

def move(db, doc_id):
    update_and_move(db, doc_id)

def update_and_move(db, doc_id, **kwargs):
    doc = db[doc_id]
    new_name = doc_id[3:] # eg lda-topic-0 -> -topic-0
    print 'updating ', doc_id, ' -> ', new_name
    doc.update(kwargs)

    db[new_name] = doc
    del db[doc_id]


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
            description='migrate db for lda/bm25f topics'
        )
    parser.add_argument('project', help='project name')
    args = parser.parse_args()

    rename_for_project(args.project)
