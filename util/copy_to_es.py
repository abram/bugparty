import json, os, sys
import requests
import datetime
sys.path = [os.getcwd()] + sys.path

from bug import Bug
from common import fix_date_str
from config import Config
import couchdb

class CouchDB(object):
    # databases
    MAIN =  0
    TOPICS = 1
    BUCKETS = 2
    GENERATED = 3

    BUGPARTY_DB_NAME = 'bugparty_internal'

    def __init__(self, project, location='http://localhost:5984'):
        self.project = project
        self.location = location

    def put(self, data, **kwargs):
        kwargs['data'] = data
        request = self.make_request(**kwargs)
        request.get_method = lambda : 'PUT'
        return json.loads(urllib2.urlopen(request).read())

    def get(self, **kwargs):
        if 'data' in kwargs:
            raise ValueError('cannot send data with GET')
        request = self.make_request(**kwargs)
        return json.loads(urllib2.urlopen(request).read())

    def make_request(self, data=None, query=None, db=None, id=None):
        db = db or CouchDB.MAIN
        if type(db) != str:
            db = self.db_name(self.project, db)
        path = [self.location, db, id]
        path = self.join_url(path, filter_nones=True)

        path += '?' + urllib.urlencode(query if query is not None else '')
        if data is not None:
            return urllib2.Request(path, json.dumps(data))
        return urllib2.Request(path)

    @staticmethod
    def db_name(project, db):
        return project + {
            CouchDB.MAIN: ''
            ,CouchDB.TOPICS: '_topics'
            ,CouchDB.BUCKETS: '_buckets'
            ,CouchDB.GENERATED: '_generated'
        }[db]

    @staticmethod
    def join_url(parts, filter_nones=False):
        return "/".join([part.strip('/') for part in parts \
                            if (not filter_nones or part is not None)])

    def get_all_bugids(self):
        filter_design = lambda x: not x.startswith('_')
        return filter(filter_design, self.connect(CouchDB.MAIN))

    def get_all_revs(self, db=None):
        db = db or CouchDB.MAIN
        data = self.get(db=db, query={
            'include_docs':'false'
        }, id="_all_docs")
        return {
            row["id"]: row["value"]["rev"]
            for row in data["rows"]
                if row["id"][0] != "_"
        }

    def get_all_docs(self, db=None):
        db or CouchDB.MAIN
        data = self.get(db=db, query={
                    'include_docs':'true'
                }, id="_all_docs")
        return {
            row["id"]: row["doc"]
            for row in data["rows"]
                if row["id"][0] != "_"
        }

    def connect(self, db):
        couch = couchdb.Server()
        return couch[CouchDB.db_name(self.project, db)]
       
    def connect_to_db(self):
        return self.connect(CouchDB.MAIN)

    def connect_to_topics_db(self):
        return self.connect(CouchDB.TOPICS)

    def connect_to_buckets_db(self):
        return self.connect(CouchDB.BUCKETS)

    def connect_to_generated_db(self):
        return self.connect(CouchDB.GENERATED)

    def buckets_url(self):
        return 'http://localhost:5984/{}_buckets'.format(self.project)

    @staticmethod
    def connect_to_bugparty_db():
        couch = couchdb.Server()
        if CouchDB.BUGPARTY_DB_NAME not in couch:
            return couch.create(CouchDB.BUGPARTY_DB_NAME)
        return couch[CouchDB.BUGPARTY_DB_NAME]


class Copier(object):
    def __init__(self, project, es_index, es_type):
        self.project = project
        self.es_index = es_index
        self.es_type = es_type
        self.location = "http://localhost:9200"

    @staticmethod
    def join_url(parts, filter_nones=False):
        return "/".join([part.strip('/') for part in parts \
                            if (not filter_nones or part is not None)])

    def copy_bugs(self, count):
        """count == -1 -> copy all"""
        couch = CouchDB(self.project)
        db = couch.connect_to_db()

        actions = []
        all_ids = couch.get_all_bugids()
        if count > 0:
            all_ids = all_ids[:count]
        for bug_id in all_ids:
            location = self.join_url([self.location, self.es_index,
                self.es_type, bug_id])

            bug = None
            try:
                bug = db[bug_id]
            except:
                # failed to get bug
                print 'Failed to get bug', bug_id
                continue
            
            # CHECK FOR BUGID NOT EXISTING
            if 'bugid' not in bug:
                break

            Bug.clean_bug_data(bug)
            Bug.fix_nulls(bug)

            actions.append({ "index": { "_id": bug_id } })
            actions.append(bug)

            if len(actions) == 400:
                self.bulk_actions(actions)
                actions = []
        if actions:
            self.bulk_actions(actions)

    def bulk_actions(self, actions):
        location = self.join_url([self.location, self.es_index,
            self.es_type, '_bulk'])
        r = requests.put(location, '\n'.join(map(json.dumps, actions)) + '\n')
        print 'bulk update of ', len(actions) / 2, 'performed'
        print r.status_code

    def valid_bug(self, bug):
        if bug['bugid'] == 0:
            return false
        if bug['status'] == 'duplicate':
            if bug.get('mergeID', 100000000) == 0:
                return False
        return True
            

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
            description='migrate db from couchdb to elasticsearch'
        )
    parser.add_argument('project', help='project name')
    parser.add_argument('es_index', help='elastic search index name')
    parser.add_argument('es_type', help='elastic search type name')
    parser.add_argument('count', help='number of documents to move',
                            nargs='?', type=int, default=-1)
    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args, '../bugparty.ini')

    Copier(args.project, args.es_index, args.es_type).copy_bugs(args.count)
