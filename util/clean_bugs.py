import os, sys
sys.path = [os.getcwd()] + sys.path # allow import from databases

from databases import CouchDB
from common import fix_date_str
from config import Config


class BugCleaner(object):
    def __init__(self, project):
        self.project = project

    def clean_bugs(self):
        couch = CouchDB(self.project)
        db = couch.connect_to_db()

        for bug_id in couch.get_all_bugids():
            bug = db[bug_id]
            self.fix_nulls(bug)
            self.fix_status(bug)
            self.fix_bugid(bug)

            if 'openedDate' in bug:
                bug['openedDate'] = fix_date_str(bug['openedDate'])

            db[bug_id] = bug

            print 'cleaned', bug_id

    @staticmethod
    def fix_nulls(bug):
        ''' recursively replace the value 'null' with real nulls '''
        for k, v in bug.iteritems():
            if bug[k] == 'null' or bug[k] == {}:
                bug[k] = None
            elif type(bug[k]) == dict:
                BugCleaner.fix_nulls(bug[k])

    @staticmethod
    def fix_status(bug):
        ''' lower case the bug status '''
        bug['status'] = bug['status'].lower()

    @staticmethod
    def fix_bugid(bug):
        ''' turn bugid into an int, if possible '''
        try:
            bug['bugid'] = int(bug['bugid'])
        except:
            pass


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
            description='fix some strangeness in bug attributes'
        )
    parser.add_argument('project', help='project name')

    Config.add_args(parser)
    args = parser.parse_args()
    config = Config.build(args)

    BugCleaner(args.project).clean_bugs()
