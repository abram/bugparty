import urllib2
import pdb
import random
import json
from logging import warn
project = "android"
prefix = "http://localhost:8080/bugparty/%s/" % project
warn("what")
premature_optimization = dict()
def jsonget( uri, retries=3 ):
	if not uri in premature_optimization:
		try:
	        	req = urllib2.urlopen(uri, None, 100)
		except urllib2.HTTPError as e:
			if retries > 0 and (e.getcode() == 503):
				return jsonget(uri, retries=retries-1)
			else:
				raise e
		j = json.loads(req.read())
		premature_optimization[uri] = j
	return premature_optimization[uri]

def getmvel( bugid ):
	uri = "%ssimilar_description_context/NFR/%s" % (prefix, bugid)
	#warn(uri)
	try:
		j = jsonget( uri )
		return [x["bugid"] for x in j if x["bugid"] != bugid]
	except urllib2.HTTPError as e:
		warn(e)
		return list()	

def gettopic( bugid ):
	uri = "%sbugs/%s" % (prefix, bugid)
	j = jsonget( uri )
	return [x["id"] for x in j["similar"] if x["id"] != bugid]
#urihttp://bugpartyhost:8080/bugparty/project/bugs/10010 
#"similar": [{"i": 0, "r": 0.0, "id": "10010"}, {"i": 1, "r": 53.326763065158936

def getlucene( bugid ):
	uri = "%ssimilar_text/%s" % (prefix, bugid)
	try:
		j = jsonget( uri )
		return [x["bugid"] for x in j if x["bugid"] != bugid]
	except urllib2.HTTPError as e:
		warn(e)
		return list()	

def getbugids( project ):
	uri = "%sbugs/" % (prefix)
	j = jsonget(uri)
	bugs = [x["bugid"] for x in j]
	return bugs

def empty_bug(bugid):
	return {"bugid":bugid,"similar":list()}

def get_bug(bugid):
	uri = "%sbugs/%s" % (prefix, bugid)
	try:
		j = jsonget( uri )
		return j
	except urllib2.HTTPError as e:
		return empty_bug(bugid)

warn("Get bugs")
bugs = getbugids( project )
#warn("Sample bugs")
#bugs = [bugs[random.randint(0,len(bugs))] for i in range(0,100)]
posid = set()
warn("Pos bugs")
for bugid in bugs:
	bug = get_bug(bugid)
	if "mergeID" in bug:
		if len(str(bug["mergeID"])) > 0:
			posid.add(bug["bugid"])
			#posid.add(bug["mergeID"])

posbugs = [get_bug(bugid) for bugid in posid]

def rank_query( bug, query ):
	bugid = bug["bugid"]
	res = query( bugid )
	res = [str(r) for r in res]
	if not "mergeID" in bug:
		return 0
	mergeID = str(bug["mergeID"])
	if mergeID=="":
		return 0
	try:
		return 1 + res.index(mergeID)
	except ValueError:
		return 0

def MRR( bugs, query ):
	ranks = [rank_query(x, query) for x in bugs]
	rranks = [1.0/rank for rank in ranks if rank != 0]
	mrr = sum(rranks)/float(len(ranks))
	rank = sum(ranks)/float(len(ranks))
	return (mrr,rank)

def bucket(bug):
	if "bucket" in bug:
		return bug["bucket"]
	if not "mergeID" in bug or bug["mergeID"]=="":
		bug["bucket"] = bug["bugid"]
	else:
		try:
			bug["bucket"] = bucket(get_bug(bug["mergeID"]))
		except urllib2.HTTPError:
			bug["bucket"] = bug["bugid"]
	return bug["bucket"]

def topk(bugs, query, k=10):
	for bug in bugs:
		bucket(bug)
	precs = list()
	seen = 0
	for bug in bugs:
		res = query(bug["bugid"])
		res = res[:k] # cut off
		mybucket = bucket(bug)
		buckets = [bucket(get_bug(bugid)) for bugid in res]
		subs = [x for x in buckets if x == mybucket]
		if len(subs) > 0:
			seen += 1
	return seen / float(len(bugs))

def top1(bugs, query):
	return topk(bugs, query, k=1)
def top5(bugs, query):
	return topk(bugs, query, k=1)
def top10(bugs, query):
	return topk(bugs, query, k=1)

def precisionat(bugs, query, n=10):
	for bug in bugs:
		bucket(bug)
	precs = list()
	for bug in bugs:
		res = query(bug["bugid"])
		res = res[:n] # cut off
		mybucket = bucket(bug)
		buckets = [bucket(get_bug(bugid)) for bugid in res]
		subs = [x for x in buckets if x == mybucket]
		prec = len(subs) / float(n)
		precs.append(prec)
	return sum(precs) / float(len(precs))

def p10(bugs, query):
	return precisionat(bugs, query, n=10)
def p5(bugs, query):
	return precisionat(bugs, query, n=5)
def p3(bugs, query):
	return precisionat(bugs, query, n=3)
def p1(bugs, query):
	return precisionat(bugs, query, n=1)



def MAP( bugs, query ):
	# bucket em
	for bug in bugs:
		bucket(bug)
	aps = list()
	for bug in bugs:
		res = query(bug["bugid"])
		mbucket = bucket(bug)
		seen = 0
		mysum = 0;
		for i in range(0,len(res)):
			bbug = get_bug(res[i])		
			bbucket = bucket(bbug)
			if (bbucket == mbucket):
				seen += 1
				mysum += seen / (i + 1)
		if (len(res) == 0):
			ap = 0
		else:
			ap = mysum / float(len(res))
		aps.append(ap)
	mapmap = sum(aps)/len(aps)
	return mapmap

tests = {"MRR":MRR, "MAP":MAP, "P5":p5, "P10":p10, "P1":p1,"Top1":top1, "Top5":top5, "top10":top10}
#funcs = {"Topic":gettopic, "Lucene":getlucene, "MVEL":getmvel}
funcs = {"Topic":gettopic, "Lucene":getlucene, "MVEL":getmvel}
#funcs = { "MVEL":getmvel}

for test in tests:
	for func in funcs:
		print ("%s,%s,%s" % (test,func,str(tests[test]( posbugs, funcs[func]))))

#warn("MRR")
#print MRR(posbugs, gettopic)
#print MRR(posbugs, getlucene)
#warn("MAP")
#print MAP(posbugs, gettopic)
#print MAP(posbugs, getlucene)
