#  Copyright (C) 2014 Alex Wilson
#  Copyright (C) 2012-14 Abram Hindle
#  
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os, subprocess
import common, task_queue


DOCS_SRC_FOLDER = os.path.join(os.path.dirname(__file__), 'docs')
DOCS_OUT_FOLDER = os.path.join(os.path.dirname(__file__), 'static', 'docs')


class DocsTask(task_queue.Task):
    '''Task that makes sure that docs are up to date and built when
        we start the server'''
    recoverable = True

    def __init__(self):
        pass

    def run(self, *args):
        with common.ChDir(DOCS_SRC_FOLDER):
            subprocess.check_call(['make', 'BUILDDIR=' + DOCS_OUT_FOLDER, 'html'])


def add_startup_tasks_to_queue(queue):
    queue.add_task(DocsTask())
