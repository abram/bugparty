#!bin/bash
FILE=$1
BUGID=$2
LIMIT=${3:-10}

awk -v id="$BUGID" -F, '{if ($1==id) {print $2 "," $10} if ($2==id) {print $1 "," $10}}' $FILE | head -n$LIMIT