angular.module('topicsApp', [])
.controller('TopicsCtrl', function($scope, $http, $timeout) {
	$scope.project = window.location.pathname.split('/')[2];
	$scope.topics_url = '/bugparty/' + $scope.project + '/topics';

	$scope.flash = {
		success: ''
		,error: ''

		,show: function(msg, key) {
			$scope.flash[key] = msg;
			$timeout(function() {
				$scope.flash[key] = '';
			}, 3000);
		}
	};
	$scope.create_topic = {
		shown: false
		,name: ''
		,words: ''

		,show: function() {
			this.name = '';
			this.words = '';
			this.shown = true;
		}
		,hide: function() {
			this.shown = false;
		}
		,submit: function() {
			$http.post($scope.topics_url, {
				name: this.name,
				words: this.words,
			})
			.success(function(data, status, headers, config) {
				$scope.topics.addTopic(data);
				$scope.flash.show('topic created',
					'success');
				$scope.create_topic.shown = false;
			})
			.error(function(data, status, headers, config) {
				$scope.flash.show('topic creation failed',
					'error');
			});
		}
	};

	$scope.topics = {
		list: {
			lda: {
				name: 'Automatically Inferred Topics'
				,custom: false
				,reverse: false // for sorting
				,topics: []
			}	
			,BM25F: {
				name: 'User-Created Topics'
				,custom: true
				,reverse: false // for sorting
				,topics: []
			}
		}
		,sort: function(list, key) {
			this.list[list].topics.sort(function(a, b) {
				return a[key] < b[key] ? 1 : -1;
			});
			if (this.list[list].reverse) {
				this.list[list].topics.reverse();
			}
			this.list[list].reverse = !this.list[list].reverse;
		}	
		,addTopic: function(topic) {
			this.list[topic.method].topics.push(topic);
		}
	};

	$scope.loading = true;
	$http.get($scope.topics_url)
		.success(function(data, status, headers, config) {
			angular.forEach(data, function(topic) {
				$scope.topics.addTopic(topic);
			});
			$scope.loading = false;
		});

	$scope.updateTopicName = function(topic, form) {
		$http({method: 'PATCH', url: topic.location, data:{
			'name': topic.name
		}}).success(function(data, status, headers, config) {
			form.$setPristine();
			$scope.flash.show('topic renamed', 'success');
		}).error(function(data, status, headers, config) {
			$scope.flash.show('topic rename failed', 'error');
		});
	};
});
