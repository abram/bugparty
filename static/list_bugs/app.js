angular.module('bugSimilaritiesApp', [])
.controller('BugCtrl', function($scope, $http, $timeout, $interpolate){
	$scope.project = window.location.pathname.split('/')[2];
	$scope.pagenum = window.location.pathname.split('/')[3];
	$scope.bugs_url = '/bugparty/' + $scope.project + '/' + $scope.pagenum + '/bugs';
	// $scope.bugs_url = $interpolate('/bugparty/' + $scope.project + '/{{pagenum}}/bugs.html');
	$scope.search_url = '/bugparty/' + $scope.project + '/search_text';
	$scope.bug_ui_url = $interpolate('/bugparty/' + $scope.project +
	'/bugs/{{bugid}}/similar.html')

	$scope.num_bugs = 0;

	$scope.all_bugs = [];
	$scope.bugs = [];

	$scope.load_all_ids = function() {
		$scope.loading = true;
		$http.get($scope.bugs_url)
		.success(function(data) {
			$scope.all_bugs = data;
			$scope.loading = false;
		})
		.error(function(data, status, headers, config) {
			$scope.flash.show('failed to get bugs', 'error');
		});
	};

	$scope.search = {
		text: ''
		,do_search: function() {
			if ($scope.search.text.trim() === '') {
				$scope.load_all_ids();
				return;
			}
			$scope.loading = true;
			$http.get($scope.search_url, {params: {q: $scope.search.text}})
			.success(function(data, status, headers, config) {
				// make sure that we will show the correct data
				// even if our responses aren't in order
				if ($scope.loading !== true) { return; }
				if (config.params.q == $scope.search.text) {
					$scope.all_bugs = data;
					$scope.paginate.first();
					$scope.loading = false;
				}
			})
			.error(function(data, status, headers, config) {
				$scope.flash.show('failed to get bugs', 'error');
			});
		}
		,search_as_you_type: _.throttle(function() {
			$scope.search.do_search();
		}, 1000, {leading: false, trailing: true})
	};

	$scope.$watch('search.text', function() {
		$scope.search.search_as_you_type();
	});


	$scope.paginate = {
		pagesize: 100
		,update: function() {
			var start = this.page * this.pagesize,
			end = start + this.pagesize
			;

			$scope.bugs = $scope.all_bugs;
			if (angular.isArray($scope.all_bugs)) {
				$scope.num_bugs = $scope.all_bugs[0].num_bugs;
			}
			this.max_page = Math.ceil($scope.num_bugs /
				this.pagesize) - 1;
			this.range = this.calc_range();

			if (angular.isUndefined(this.page)) {
				this.page = $scope.pagenum;
			} else {
				$scope.pagenum = this.page;
			}

			$scope.bugs_url = '/bugparty/' + $scope.project + '/' + $scope.pagenum + '/bugs.html';
		}
		,first: function() {
			this.page = 0;
			$scope.pagenum = 0;
			this.update();
		}
		,last: function() {
			this.page = this.max_page;
			this.update();
		}
		,calc_range: function() {
			var left = Math.max(0, this.page - 5)
			,right = Math.min(left + 10, this.max_page)
			,range = []
			;
			for (;left <= right; left++) {
				range.push(left);
			}
			return range;
		}
	};
	$scope.$watchGroup(['paginate.page', 'paginate.pagesize',
	'all_bugs'], function() {
		$scope.paginate.update();
	});

	$scope.load_bug = function(bug) {
		if (!angular.isUndefined(bug.description)) { return; }

		$http.get(bug.location)
		.success(function(data) {
			angular.forEach(data, function(value, key) {
				bug[key] = value;
			});
		})
		.error(function() {
			$scope.flash.show('failed to get bug ' + $scope.bugid,
			'error');
		})

	};

	$scope.flash = {
		success: ''
		,error: ''

		,show: function(msg, key) {
			$scope.flash[key] = msg;
			$timeout(function() {
				$scope.flash[key] = '';
			}, 3000);
		}
	};

	$scope.load_all_ids();
});
