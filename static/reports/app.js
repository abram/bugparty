angular.module('reportsApp', [])
.controller('reportsCtrl', function($scope, $http, $timeout, $interpolate){
	$scope.project = window.location.pathname.split('/')[2];
    $scope.reports_url = '/bugparty/' + $scope.project + '/reports/';
    $scope.cluster_url = $scope.reports_url + 'cluster';

    $scope.flash = {
        success: ''
        ,error: ''
        
        ,show: function(msg, key) {
        	$scope.flash[key] = msg;
        	$timeout(function() {
        		$scope.flash[key] = '';
        	}, 3000);
	    }
    };

    $scope.reports = [];

    $scope.refresh_reports = function() {
        $http.get($scope.reports_url)
            .success(function(data, status, headers, config) {
                $scope.reports = data;
            })
            .error(function(data, status, headers, config) {
                $scope.flash.show('failed to query reports', 'error');
            });
    };
    $scope.refresh_reports();

    $scope.do_cluster = {
        clusters: 10 
        ,submit: function() {
            if (!$scope.cluster_form.$valid) { return; }
            $http.post($scope.cluster_url, {
                    'clusters': $scope.do_cluster.clusters
                })
                .success(function(data, status, headers, config) {
                    $scope.flash.show('cluster task initiated', 'success');
                    $scope.reports.unshift(data);
                })
                .error(function(data, status, headers, config) {
                    $scope.flash.show('failed to start cluster task',
                        'error');
                });
        }
    };
});
