// source: https://github.com/danialfarid/angular-file-upload

angular.module('newMultBugApp', ['angularFileUpload'])
  .controller('NewMultBugCtrl', function($scope, $http, $timeout, $upload) {
    $scope.project = window.location.pathname.split('/')[2];
    $scope.newBug_url = '/bugparty/' + $scope.project + '/bugs';
    $scope.selectedFiles = [];
    $scope.flash = {
      success: ''
      ,error: ''

      ,show: function(msg, key) {
	$scope.flash[key] = msg;
	$timeout(function() {
	  $scope.flash[key] = '';
	}, 3000);
      }
    };

    $scope.clear_form = function() {
      $scope.bug_fields = null;
      $scope.bug_fields_form.$setPristine();
    };

    $scope.addFileToList = function($files) {
      $scope.selectedFiles.push($files);
    }

    $scope.start = function() {
      for (var i = 0; i < $scope.selectedFiles.length; i++) {
        var file = $scope.selectedFiles[i];
        $scope.upload = $upload.upload({
          url: $scope.newBug_url, 
          method: 'POST',
          data: {bug: $scope.bug_fields},
          file: file
        }).progress(function(evt) {
          console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
        })
	  .success(function(data, status, headers, config) {
	      $scope.clear_form();
	      $scope.flash.show('bugs added', 'success');
	    })
	  .error(function(data, status, headers, config) {
	    $scope.flash.show(data, 'error');
	  });
      }
      $scope.selectedFiles = [];
    };
  });
