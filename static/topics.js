// load multiple json objects and call a callback with all of the
// data once it is done
function multi_json(urls, callback) {
    var loaded = new Array(urls.length),
    count = 0;
    urls.forEach(function(url, i) {
        d3.json(url, function(data) {
            loaded[i] = data;
            count += 1;

            console.log('loaded: ', url);
            if (count == urls.length) {
                callback.apply(window, loaded);
            }
        });
    });
};


// Various formatters.
var formatNumber = d3.format(",d"),
formatChange = d3.format(".3g"),
formatDate = d3.time.format("%B %d, %Y"),
formatTime = d3.time.format("%I:%M %p");

// A nest operator, for grouping the bug list.
var nestByDate = d3.nest()
    .key(function(d) { return d3.time.day(d.date); });


TopicsCrossfilter = function(bug) {
    var bucketGrouper = function(d) {
            return Math.floor(d / 10) * 10;
        }
        ,clampedRelevancy = function(name) { return function(d) {
	        return Math.max(-60, Math.min(149, d[name]));
        }}
    ;

    this.bug = bug;

    this.gui = {
    };
    this.dimensions = {
	    first: bug.dimension(clampedRelevancy('firstRelevancy'))
        ,second: bug.dimension(clampedRelevancy('secondRelevancy'))
        ,third: bug.dimension(clampedRelevancy('thirdRelevancy'))
        ,date: bug.dimension(function(d) { return d3.time.day(d.date); })
    };
    this.groups = {
        first: this.dimensions.first.group(bucketGrouper)
        ,second: this.dimensions.second.group(bucketGrouper)
        ,third: this.dimensions.third.group(bucketGrouper)
        ,date: this.dimensions.date.group()
        ,all: bug.groupAll()
    }
};

TopicsCrossfilter.prototype.update_topic_info = function(topics) { 
	d3.selectAll(".topic-words")
		.data(topics)
		.text(function(d) { return d.words.join(" ") + '...'; })
	;

};


TopicsCrossfilter.prototype.update_charts = function() {
    d3.selectAll(".chart").selectAll("svg").remove();
        charts = this.getCharts()
        ,self = this
        ,rerender = function() { self.renderAll(); };

    // Given our array of charts, which we assume are in the same order as the
    // .chart elements in the DOM, bind the charts to the DOM and render them.
    // We also listen to the chart's brush events to update the display.
    this.gui.charts = d3.selectAll(".chart")
    	.data(charts)
    	.each(function(chart) { 
        	chart.on("brush", rerender)
		         .on("brushend", rerender);
	    })
    	.each(render);
};

TopicsCrossfilter.prototype.update_list = function(selectedTopicNames) {
    d3.selectAll(".bug").remove();
    d3.selectAll(".bugInfoClass").remove();

    // Render the initial lists.
    var list = d3.selectAll(".list")
	            .data([this.bugList(selectedTopicNames)]);
    list.each(render);
    this.gui.list = list;
}


TopicsCrossfilter.prototype.renderAll = function() {
    d3.selectAll(".bugInfoClass").remove();
    d3.select("#active").text(formatNumber(this.groups.all.value()));
    d3.selectAll("#total").text(formatNumber(this.bug.size()));
    this.gui.charts.each(render);
    this.gui.list.each(render);
};


// Initializes the chart objects
TopicsCrossfilter.prototype.getCharts = function() {
    var dimensions = this.dimensions
        ,groups = this.groups
        ,all_dates = dimensions.date.top(Infinity);
    this.charts = [
	barChart()
            .dimension(dimensions.first)
            .group(groups.first)
            .x(d3.scale.linear()
               .domain([0, dimensions.first.top(1)[0].firstRelevancy * 1.1])
               .rangeRound([0, 10 * 21]))
            .axis(d3.svg.axis()
		        .orient("bottom")
		        .ticks(7))
            .y(d3.scale.log()
               .domain([1, groups.first.top(1)[0].value])
               .range([100, 0])),
	
	barChart()
            .dimension(dimensions.second)
            .group(groups.second)
            .x(d3.scale.linear()
               .domain([0, dimensions.second.top(1)[0].secondRelevancy * 1.1])
               .rangeRound([0, 10 * 21]))
            .axis(d3.svg.axis()
		  .orient("bottom")
		  .ticks(7))
            .y(d3.scale.log()
               .domain([1, groups.second.top(1)[0].value])
               .range([100, 0])),
	
	barChart()
            .dimension(dimensions.third)
            .group(groups.third)
            .x(d3.scale.linear()
               .domain([0, dimensions.third.top(1)[0].thirdRelevancy * 1.1])
               .rangeRound([0, 10 * 21]))
            .axis(d3.svg.axis()
		  .orient("bottom")
		  .ticks(7))
            .y(d3.scale.log()
               .domain([1, groups.third.top(1)[0].value])
               .range([100,0])),

	barChart()
            .dimension(dimensions.date)
            .group(groups.date)
            .round(d3.time.day.round)
            .x(d3.time.scale()
               .domain([all_dates[all_dates.length - 1].date, 
			        new Date(all_dates[0].date.getFullYear(), 
				        all_dates[0].date.getMonth() + 1, 
				        all_dates[0].date.getDate())])
               .rangeRound([0, 10 * 90]))
            .axis(d3.svg.axis()
		        .orient("bottom"))
            .y(d3.scale.linear()
                .domain([0, groups.date.top(1)[0].value])
                .range([100, 0])),
    ];

    return this.charts;
}

// defines the barChart object
function barChart() {
    if (!barChart.id) barChart.id = 0;

    var margin = {top: 10, right: 10, bottom: 20, left: 10},
    x,
    y,
    id = barChart.id++,
    axis,
    brush = d3.svg.brush(),
    brushDirty,
    dimension,
    group,
    round;

    function chart(div) {
	var width = x.range()[1],
        height = y.range()[0];

	div.each(function() {
       var div = d3.select(this),
           g = div.select("g");

            // Create the skeletal chart.
        if (g.empty()) {
		    div.select(".title").append("a")
                .attr("href", "javascript:reset(" + id + ")")
                .attr("class", "reset")
                .text("reset")
                .style("display", "none");

            g = div.append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," 
                  + margin.top + ")");

            g.append("clipPath")
                .attr("id", "clip-" + id)
                .append("rect")
                .attr("width", width)
                .attr("height", height);

            g.selectAll(".bar")
                .data(["background", "foreground"])
                .enter().append("path")
                .attr("class", function(d) { return d + " bar"; })
                .datum(group.all());

            g.selectAll(".foreground.bar")
                .attr("clip-path", "url(#clip-" + id + ")");

            g.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(0," + height + ")")
                .call(axis);

            // Initialize the brush component with pretty resize handles.
            var gBrush = g.append("g")
                            .attr("class", "brush")
                            .call(brush);
            gBrush.selectAll("rect").attr("height", height);
            gBrush.selectAll(".resize")
                .append("path")
                .attr("d", resizePath);
        }

        // Only redraw the brush if set externally.
        if (brushDirty) {
            brushDirty = false;
            g.selectAll(".brush").call(brush);
            div.select(".title a").style("display", brush.empty() ? 
                             "none" : null);
            if (brush.empty()) {
                g.selectAll("#clip-" + id + " rect")
                    .attr("x", 0)
                    .attr("width", width);
            } else {
                var extent = brush.extent();
                g.selectAll("#clip-" + id + " rect")
                    .attr("x", x(extent[0]))
                    .attr("width", x(extent[1]) - x(extent[0]));
            }
        }
        g.selectAll(".bar").attr("d", barPath);
	});

	function barPath(groups) {
        var path = [],
        i = -1,
        n = groups.length,
        d;
        while (++i < n) {
            d = groups[i];
            path.push("M", x(d.key), ",", height, "V", y(d.value), 
                  "h9V", height);
        }
        return path.join("");
	}

	function resizePath(d) {
            var e = +(d == "e"),
            x = e ? 1 : -1,
            y = height / 3;
            return "M" + (.5 * x) + "," + y
		+ "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6)
		+ "V" + (2 * y - 6)
		+ "A6,6 0 0 " + e + " " + (.5 * x) + "," + (2 * y)
		+ "Z"
		+ "M" + (2.5 * x) + "," + (y + 8)
		+ "V" + (2 * y - 8)
		+ "M" + (4.5 * x) + "," + (y + 8)
		+ "V" + (2 * y - 8);
	}
    }

    brush.on("brushstart.chart", function() {
	    var div = d3.select(this.parentNode.parentNode.parentNode);
	    div.select(".title a")
            .style("display", null);
    });

    brush.on("brush.chart", function() {
	    var g = d3.select(this.parentNode),
            extent = brush.extent();
	    if (round) {
            g.select(".brush")
                .call(brush.extent(extent = extent.map(round)))
                .selectAll(".resize")
                .style("display", null);
        }
	    g.select("#clip-" + id + " rect")
            .attr("x", x(extent[0]))
            .attr("width", x(extent[1]) - x(extent[0]));
	    dimension.filterRange(extent);
    });

    brush.on("brushend.chart", function() {
	    if (brush.empty()) {
            var div = d3.select(this.parentNode.parentNode.parentNode);
            div.select(".title a").style("display", "none");
            div.select("#clip-" + id + " rect")
                .attr("x", null)
		        .attr("width", "100%");
            dimension.filterAll();
	    }
    });

    chart.margin = function(_) {
	    if (!arguments.length) return margin;
	    margin = _;
	    return chart;
    };

    chart.x = function(_) {
	    if (!arguments.length) return x;
	    x = _;
	    brush.x(x);
	    return chart;
    };

    chart.axis = function(_) {
	    if (!arguments.length) return axis;
	    axis = _;
	    axis.scale(x);
	    return chart;
    };
    
    chart.y = function(_) {
        if (!arguments.length) return y;
        y = _;
        return chart;
    };

    chart.dimension = function(_) {
        if (!arguments.length) return dimension;
        dimension = _;
        return chart;
    };

    chart.filter = function(_) {
        if (_) {
                brush.extent(_);
                dimension.filterRange(_);
        } else {
                brush.clear();
                dimension.filterAll();
        }
        brushDirty = true;
        return chart;
    };

    chart.group = function(_) {
        if (!arguments.length) return group;
        group = _;
        return chart;
    };

    chart.round = function(_) {
        if (!arguments.length) return round;
        round = _;
        return chart;
    };

    return d3.rebind(chart, brush, "on");
}

// Renders the specified chart or list.
function render(method) {
    d3.select(this).call(method);
}


// Like d3.time.format, but faster.
function parseDate(d) {
    return new Date(
        d.substring(0, 4),
        d.substring(4, 6) - 1,
        d.substring(6, 8));
}

// we need a function to pass to d3 that takes a div
// but we need to give it the topic names, so...
TopicsCrossfilter.prototype.bugList = function(selectedTopicNames) {
    var self = this;

    return function(div) {
	var bugsByDate = nestByDate.entries(self.dimensions.date.top(40));

	div.each(function() {
	    var date = d3.select(this).selectAll(".date")
		            .data(bugsByDate, function(d) {
                        return d.key;
                    });

	    date.enter()
            .append("div")
		    .attr("class", "date")
		    .append("div")
		    .attr("class", "day")
		    .text(function(d) { return formatDate(d.values[0].date); });

	    date.exit()
            .remove();

	    var bug = date.order().selectAll(".bug")
		    .data(function(d) { 
		        return d.values;
            }, function(d) {
                return d.firstRelevancy;
            });

	    var bugEnter = bug.enter()
                        .append("div")
		                .attr("class", "bug")
		                .style("cursor", "pointer")
		                .on("click", function(d) {
	                        //creates/removes a div below the current one 
                            return bug_onClick(this, d.bugID);
                        });

	    bugEnter.append("div")
            .attr("class", "bugID")
            .text("bugID: ")
            .append("a")
                .attr("href", function(d) {return bug_url(d.bugID); })
                .text(function(d) { return d.bugID;})
                .attr("target", "_blank")
            
	    bugEnter.append("div")
		    .attr("class", "firstRelevancy")
		    .text(function(d) { return selectedTopicNames[0] + ': ' + 
				    formatChange(d.firstRelevancy); })

	    bugEnter.append("div")
		    .attr("class", "secondRelevancy")
		    .text(function(d) { return selectedTopicNames[1] + ': ' +
				    formatChange(d.secondRelevancy); });

	    bugEnter.append("div")
		    .attr("class", "thirdRelevancy")
		    .text(function(d) { return selectedTopicNames[2] + ': ' +
				    formatChange(d.thirdRelevancy); });

	    bug.exit()
            .remove();

	    bug.order();
	});
    }
}
