angular.module('projectsApp', [])
.controller('ProjectsCtrl', function($scope, $http, $timeout, $interpolate){
    $scope.projects_url = '/bugparty/projects';
    $scope.project_url = $interpolate('/bugparty/{{name}}');
    $scope.update_dupes_url = $interpolate('/bugparty/{{name}}/dupes/update');
    $scope.redo_dupes_url = $interpolate('/bugparty/{{name}}/dupes/recreate');
    $scope.update_url = $interpolate('/bugparty/{{name}}/topics/update');
    $scope.recreate_url = $interpolate('/bugparty/{{name}}/topics/recreate_lda');
    $scope.redo_context_url = $interpolate('/bugparty/{{name}}/context/recreate');
    $scope.update_context_url = $interpolate('/bugparty/{{name}}/context/update');

    $scope.flash = {
        success: ''
        ,error: ''

        ,show: function(msg, key) {
        	$scope.flash[key] = msg;
        	$timeout(function() {
        		$scope.flash[key] = '';
        	}, 3000);
	}
    };

    $scope.projects = null;
    $scope.refresh_projects = function() { $http.get($scope.projects_url)
            .success(function(data, status, headers, config) {
                $scope.projects = data;
            })
            .error(function(data, status, headers, config) {
                $scope.flash.show('failed to query projects', 'error');
            });
    };

    $scope.create_project = {
        name: ''
        ,hide_form: true
        ,submit: function() {
            if (!$scope.project_form.$valid) { return; }
            $http.post($scope.project_url($scope.create_project), '')
                .success(function(data, status, headers, config) {
                    $scope.refresh_projects();
                    $scope.create_project.hide();
                    $scope.flash.show('project created', 'success');
                })
                .error(function(data, status, headers, config) {
                    $scope.flash.show('failed to create project', 'error');
                });
        }
        ,hide: function() {
            this.hide_form = true;
            this.name = '';
        }
    };

    $scope.delete_project = function(project) {
        if (!confirm("delete project '" + project.name + "'")) {
            return;
        }
        $http.delete($scope.project_url(project), '')
            .success(function(data, status, headers, config) {
                $scope.refresh_projects();
                $scope.flash.show('project deleted', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.flash.show('failed to delete project', 'error');
            });
    };

    var project_action = function(url, success) {
        return function(project) {
            $http.post(url(project), '')
            .success(function(data, status, headers, config) {
                $scope.flash.show(success, 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.flash.show('failed to schedule task', 'error');
            });
        };
    };

    $scope.recompute = project_action($scope.recreate_url,
                                        'LDA analysis scheduled');
    $scope.update = project_action($scope.update_url,
                                        'topic analysis scheduled');
    $scope.update_dupes = project_action($scope.update_dupes_url,
                                        'deduplication update scheduled');
    $scope.redo_dupes = project_action($scope.redo_dupes_url,
                                        'deduplication update scheduled');
    $scope.redo_context = project_action($scope.redo_context_url,
                                        'context update scheduled');
    $scope.update_context = project_action($scope.update_context_url,
                                        'context update scheduled');
    $scope.refresh_projects();
});
