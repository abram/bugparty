angular.module('newBugApp', [])
    .controller('NewBugCtrl', function($scope, $http, $timeout) {
	$scope.project = window.location.pathname.split('/')[2];
	$scope.newBug_url = '/bugparty/' + $scope.project + '/bugs';

	$scope.flash = {
		success: ''
		,error: ''

		,show: function(msg, key) {
			$scope.flash[key] = msg;
			$timeout(function() {
				$scope.flash[key] = '';
			}, 3000);
		}
	};

	$scope.clear_form = function() {
	    $scope.new_bug = null;
	    $scope.new_bug_form.$setPristine();
	};

	$scope.create_bug = {
	    submit: function() {
		$http.post($scope.newBug_url, {
		    bugid: $scope.new_bug.bugID,
		    mergeID: $scope.new_bug.mergeID,
		    openedDate: $scope.new_bug.openDate,
		    closeDate: $scope.new_bug.closeDate,
		    title: $scope.new_bug.title,
		    description: $scope.new_bug.description,
		    status: $scope.new_bug.status,
		    component: $scope.new_bug.component,
		    version: $scope.new_bug.version,
		    priority: $scope.new_bug.priority,
		    type: $scope.new_bug.type
		})
	        .success(function(data, status, headers, config) {
	          $scope.clear_form();
	          $scope.flash.show('bugs added', 'success');
	        })
	        .error(function(data, status, headers, config) {
	          $scope.flash.show(data, 'error');
	        });
	    }
	}
    }
);
