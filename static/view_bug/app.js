angular.module('bugSimilaritiesApp', [])
.controller('BugCtrl', function($scope, $http, $timeout, $interpolate){
	$scope.project = window.location.pathname.split('/')[2];
	$scope.bugid = window.location.pathname.split('/')[4];
	$scope.bug_url = $interpolate('/bugparty/' + $scope.project +
								  '/bugs/{{bugid}}');
	$scope.bug_ui_url = $interpolate('/bugparty/' + $scope.project +
									'/bugs/{{bugid}}/similar.html')

	$scope.similarities = [];
	$scope.bug = {'bugid': $scope.bugid};
	$http.get($scope.bug_url({'bugid': $scope.bugid}))
		.success(function(data) {
			$scope.bug = data;
			$scope.load_columns();
		})
        .error(function(data, status, headers, config) {
        	$scope.flash.show('failed to get bug ' + $scope.bugid, 'error');
        });

	$scope.columns = [
		{
			name: 'text'
			,url: function(bug) { return bug['similar_text']; }
		}
		,{
			name: 'topic scores'
			,url: function(bug) { return bug['similar_by_topics']; }
		}
		,{
			name: 'deduper (REP)'
			,url: $interpolate('/bugparty/' + $scope.project +
					'/fastrep/matches/{{bugid}}/rep/1/10')
		}
		,{
			name: 'deduper (all)'
			,url: $interpolate('/bugparty/' + $scope.project +
					'/fastrep/matches/{{bugid}}/all/1/10')
		}
		,{
			name: 'es NFR'
			,url: $interpolate('/bugparty/' + $scope.project +
					'/similar_description_context/NFR/{{bugid}}')
		}
	];

	angular.forEach($scope.columns, function(c) {
		c.error = false;
		c.loading = true;
	});

    $scope.load_bug = function(bug) {
        if (!angular.isUndefined(bug.description)) { return; }

        $http.get(bug.location)
               .success(function(data) {
                angular.forEach(data, function(value, key) {
                    bug[key] = value;
                });
            })
            .error(function() {
                $scope.flash.show('failed to get bug ' + $scope.bugid,
                        'error');
            })

    };

	$scope.load_columns = function() {
		angular.forEach($scope.columns, function(column) {
			return $http.get(column.url($scope.bug))
				.success(function(data) {
					if (!angular.isUndefined(data['Error'])) {
						$scope.column_failed(column);
						return;
					}
					column.bugs = data;
					column.loading = false;
					column.error = false;
				})
				.error(function() {
					$scope.column_failed(column);
				});
		});
	};

	$scope.column_failed = function(column) {
		$scope.flash.show('failed to get ' + column.name 
							+ ' results', 'error');
		column.loading = false;
		column.error = true;
	};

    $scope.flash = {
        success: ''
        ,error: ''
        
        ,show: function(msg, key) {
        	$scope.flash[key] = msg;
        	$timeout(function() {
        		$scope.flash[key] = '';
        	}, 3000);
	}
    };
});
