angular.module('similaritiesApp', [])
.controller('FastREPCtrl', function($scope, $http, $timeout, $interpolate){
  $scope.input = {};
  $scope.project = window.location.pathname.split('/')[2];
  $scope.bug_ui_url = $interpolate('/bugparty/' + $scope.project +
					 '/bugs/{{bugid}}/similar.html')
  $scope.bug_url = $interpolate('/bugparty/' + $scope.project +
                                '/bugs/{{bugid}}');
  $scope.similarities = [];

  $scope.flash = {
    success: ''
    ,error: ''
        
    ,show: function(msg, key) {
      $scope.flash[key] = msg;
      $timeout(function() {
        $scope.flash[key] = '';
      }, 3000);
    }
  };

  $scope.init = function() {
    $scope.input = {
      bugid: '1',
      context: 'rep',
      rep: '1',
      limit: 5
    }
    $scope.updateBug();
    $scope.updateMatchingList();
  }

  $scope.updateBug = function() {
    $scope.bug = {'bugid': $scope.input.bugid};
    $http.get($scope.bug_url({'bugid': $scope.input.bugid}))
      .success(function(data) {
        $scope.bug = data;
      })
      .error(function(data, status, headers, config) {
        $scope.flash.show('failed to get bug ' + $scope.input.bugid, 'error');
      });
  }

  $scope.updateMatchingList = function() {
    matches_url = '/bugparty/' + $scope.project + '/fastrep/matches/' +
      $scope.input.bugid + "/" + $scope.input.context + "/" + $scope.input.rep + "/"
      + $scope.input.limit;

    $http.get(matches_url)
      .success(function(data) {
        $scope.similarities = data;
      })
      .error(function(data, status, headers, config) {
        $scope.similarities = [];
	$scope.flash.show('failed to fetch similar bugs', 'error');
      });
  };

  $scope.updateMatchingListAndBug = function() {
    $scope.updateBug();
    $scope.updateMatchingList();
  }

  $scope.load_bug = function(sim) {
    if (!angular.isUndefined(sim.description)) { return; }

    $http.get(sim.location)
      .success(function(data) {
        angular.forEach(data, function(value, key) {
          sim[key] = value;
        });
      })
      .error(function() {
        $scope.flash.show('failed to get bug ' + bug.bugid,
                          'error');
      })
    };

});