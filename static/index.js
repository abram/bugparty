angular.module('indexApp', [])
.controller('IndexCtrl', function($scope, $http, $timeout, $interpolate){
  $scope.urls = {
    projects: '/bugparty/projects'
    ,overview_dash: $interpolate('/kibana/#/dashboard/file/overview.json/{{name}}')
    ,fastrep: $interpolate('/bugparty/{{name}}/fastrep/matches.html')
    ,topics: $interpolate('/bugparty/{{name}}/topics/topics.html')
    ,list_bugs: $interpolate('/bugparty/{{name}}/0/bugs.html')
    ,reports: $interpolate('/bugparty/{{name}}/reports.html')
    ,new_bug: $interpolate('/bugparty/{{name}}/bugs/new.html')
    ,mult_new_bug: $interpolate('/bugparty/{{name}}/bugs/newMult.html')
    ,similar_bugs: $interpolate('/bugparty/{{name}}/bugs/' +
					 '{{view_similar_bugid}}/similar.html')
  };

    $scope.flash = {
        success: ''
        ,error: ''

        ,show: function(msg, key) {
        	$scope.flash[key] = msg;
        	$timeout(function() {
        		$scope.flash[key] = '';
        	}, 3000);
	}
    };

    $scope.projects = null;
    $scope.refresh_projects = function() { $http.get($scope.urls.projects)
            .success(function(data, status, headers, config) {
                $scope.projects = data;
            })
            .error(function(data, status, headers, config) {
                $scope.flash.show('failed to query projects', 'error');
            });
    };

    $scope.refresh_projects();
});
