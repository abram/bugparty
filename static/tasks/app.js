angular.module('tasksApp', [])
.directive('showTask', function() {return {
		'restrict': 'E',
		'scope': {
				'task': '='
				,'failedAt': '='
		}
		,'templateUrl': '/static/tasks/task_template.html'
};})
.controller('TasksCtrl', function($scope, $http, $timeout, $interval) {
    $scope.topics_url = '/bugparty/tasks'
    $scope.errors_url = '/bugparty/tasks/errors'
    $scope.clear_topics_url = '/bugparty/tasks/clear'
    $scope.clear_errors_url = '/bugparty/tasks/errors/clear'

    $scope.flash = {
        success: ''
        ,error: ''
        
        ,show: function(msg, key) {
        	$scope.flash[key] = msg;
        	$timeout(function() {
        		$scope.flash[key] = '';
        	}, 3000);
	}
    };

    $scope.tasks = null;
    $scope.errors = null;

	$scope.clearQueue = function() {
		$http.post($scope.clear_topics_url, {});
	};
	$scope.clearErrors = function() {
		$http.post($scope.clear_errors_url, {});
	};

	$scope.update = function() {
		$http.get($scope.errors_url)
			.success(function(data, status, headers, config) {
				$scope.errors = data;
			})
			.error(function(data, status, headers, config) {
				$scope.flash.show('failed to query errors', 'error');
			});

		$http.get($scope.topics_url)
			.success(function(data, status, headers, config) {
				$scope.tasks = data;
			})
			.error(function(data, status, headers, config) {
				$scope.flash.show('failed to query tasks', 'error');
			});
	};

	$scope.update();
    $interval(function () {$scope.update()}, 9000);
});
