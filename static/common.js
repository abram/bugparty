// get url params
// (eg. localhost:8080/bugparty/topics/index/topics_index.html?project=example
//      -> "params['project'] -> example" )
function getParams(search) {
    var params = []
    search = search.slice(1).split('&');
    var i = 0;
    for (i = 0; i < search.length; i += 1) {
        var token = search[i].split('=')
        ,key = decodeURIComponent(token[0])
        ,value = decodeURIComponent(token[1]);
	params[key] = value;
    }
    return params
};


// (eg. localhost:8080/bugparty/topics/index/topics_index.html
//  -> ['bugparty', 'topics', 'index', topics_index.html']
function pathname_parts() {
    var parts = window.location.pathname.split('/');
    parts.shift(); //discard empty first part
    return parts;
};

bug_url = function(bug_id) {
    return "/bugparty/" + project + "/display/" + bug_id;
}

function bug_onClick(parentDiv, bugID) {
    var path = bug_url(bugID);
    if (parentDiv.nextSibling == null || 
	parentDiv.nextSibling.className != "bugInfoClass") {

	bugInfoDiv = document.createElement("div");
	bugInfoDiv.className = "bugInfoClass";
	parentDiv.parentNode.insertBefore(bugInfoDiv, 
					  parentDiv.nextSibling);
	
	d3.json(path, function(bugInfo) {
	    
	    bugTitle = bugInfo.title;
	    bugReportedBy = bugInfo.reportedBy; 
	    
	    titleDiv = document.createElement("div");
	    titleDiv.innerHTML = "<b>Title:</b> " + bugTitle;
	    bugInfoDiv.appendChild(titleDiv);
	    
	    reportedByDiv = document.createElement("div");
	    reportedByDiv.innerHTML = "<b> Reported by:</b> " + 
		bugReportedBy;
	    bugInfoDiv.appendChild(reportedByDiv);
	});
    }
    else {
	parentDiv.parentNode.removeChild(parentDiv.nextSibling);
    }
}
