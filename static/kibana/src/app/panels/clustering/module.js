/** @scratch /panels/5
 *
 * include::panels/hits.asciidoc[]
 */

/** @scratch /panels/hits/0
 *
 * == Hits
 * Status: *Stable*
 *
 * The hits panel displays the number of hits for each of the queries on the dashboard in a
 * configurable format specified by the `chart' property.
 *
 */
define([
  'angular',
  'app',
  'lodash',
  'jquery',
  'kbn',
], function (angular, app, _, $, kbn) {
  'use strict';

  var module = angular.module('kibana.panels.clustering', []);
  app.useModule(module);

  module.controller('clustering', function($scope, querySrv, dashboard, filterSrv, bugpartySrv) {
    $scope.panelMeta = {
      modals : [
        {
          description: "Inspect",
          icon: "icon-info-sign",
          partial: "app/partials/inspector.html",
          show: $scope.panel.spyable
        }
      ],
      editorTabs : [
        {title:'Queries', src:'app/partials/querySelect.html'}
      ],
      status  : "Stable",
      description : "The total hits for a query or set of queries. Can be a pie chart, bar chart, "+
        "list, or absolute total of all queries combined"
    };

    // Set and populate defaults
    var _d = {
      /** @scratch /panels/hits/3
       * spyable:: Setting spyable to false disables the inspect icon.
       */
      spyable : true,
      /** @scratch /panels/hits/5
       *
       * ==== Queries
       * queries object:: This object describes the queries to use on this panel.
       * queries.mode::: Of the queries available, which to use. Options: +all, pinned, unpinned, selected+
       * queries.ids::: In +selected+ mode, which query ids are selected.
       */
      queries     : {
        mode        : 'all',
        ids         : []
      },
    };
    _.defaults($scope.panel,_d);

    $scope.init = function () {
      $scope.original_queries = [];
      $scope.reports = [];
      $scope.cluster_count = 10;
      $scope.hits = 0;

      $scope.$on('refresh',function(){
        $scope.get_data();
      });
      $scope.get_data();

    };

    $scope.get_data = function(segment,query_id) {
      delete $scope.panel.error;
      $scope.panelMeta.loading = true;

      // Make sure we have everything for the request to complete
      if(dashboard.indices.length === 0) {
        return;
      }

      var _segment = _.isUndefined(segment) ? 0 : segment;
      var request = $scope.ejs.Request().indices(dashboard.indices[_segment]);

      $scope.panel.queries.ids = querySrv.idsByMode($scope.panel.queries);
      var queries = querySrv.getQueryObjs($scope.panel.queries.ids);

      // Build the question part of the query
      $scope.original_queries = _.map(queries, function(q) {
        return {
          label: q.alias || q.query
          ,color: q.color
          ,query: $scope.ejs.FilteredQuery(
                    querySrv.toEjsObj(q),
                    filterSrv.getBoolFilter(filterSrv.ids()))
        };
      });

      _.each(queries, function(q) {
        var _q = $scope.ejs.FilteredQuery(
                  querySrv.toEjsObj(q),
                  filterSrv.getBoolFilter(filterSrv.ids()));

        request = request
          .facet($scope.ejs.QueryFacet(q.id)
            .query(_q)
          ).size(0);
      });

      // Populate the inspector panel
      $scope.inspector = angular.toJson(JSON.parse(request.toString()),true);

      // Then run it
      var results = request.doSearch();

      // Populate scope when we have results
      results.then(function(results) {
        $scope.panelMeta.loading = false;
        if(_segment === 0) {
          $scope.hits = 0;
          $scope.data = [];
          query_id = $scope.query_id = new Date().getTime();
        }

        // Check for error and abort if found
        if(!(_.isUndefined(results.error))) {
          $scope.panel.error = $scope.parse_error(results.error);
          return;
        }

        // Make sure we're still on the same query/queries
        if($scope.query_id === query_id) {
          var i = 0;
          _.each(queries, function(q) {
            var v = results.facets[q.id];
            var hits = _.isUndefined($scope.data[i]) || _segment === 0 ?
              v.count : $scope.data[i].hits+v.count;
            $scope.hits += v.count;

            $scope.original_queries[i].hits = hits;

            // Create series
            $scope.data[i] = {
              info: q,
              id: q.id,
              hits: hits,
              data: [[i,hits]]
            };
            i++;
          });
          if (_segment < dashboard.indices.length-1) {
            $scope.get_data(_segment+1,query_id);
          }

        }
      });
    };

    $scope.set_refresh = function (state) {
      $scope.refresh = state;
    };

    $scope.close_edit = function() {
      if($scope.refresh) {
        $scope.get_data();
      }
      $scope.refresh =  false;
    };

    $scope.do_clustering = function(query_i) {
      var request = $scope.ejs.Request().indices(dashboard.indices[0])
          ,original = $scope.original_queries[query_i] 
      ;
      request.query(original.query)
            .fields([])
            .size(original.hits)
      ;
      request.doSearch().then(function(results) {
        var ids = _.pluck(results.hits.hits, '_id')
            ,report = bugpartySrv.do_clustering(dashboard.indices[0],
                        $scope.cluster_count, ids)
            ;
        report.original_query = original;
        $scope.reports.unshift(report);
      });
    };
  });

});
