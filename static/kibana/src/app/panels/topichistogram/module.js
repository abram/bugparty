/** @scratch /panels/5
 *
 * include::panels/topichistogram.asciidoc[]
 *
 * this was modified from Kibana's histogram panel by Alex Wilson 2014
 */
/** @scratch /panels/topichistogram/0
 *
 * == TopicHistogram
 * Status: *Custom and Buggy*
 *
 * This histogram panel allows for the display of histogram topic score charts.
 */
define([
    'angular',
    'app',
    'jquery',
    'lodash',
    'kbn',
    'moment',
    'numeral',
    'jquery.flot',
    'jquery.flot.events',
    'jquery.flot.selection',
    'jquery.flot.time',
    'jquery.flot.byte',
    'jquery.flot.stack',
    'jquery.flot.stackpercent'
  ],
function (angular, app, $, _, kbn, moment, numeral) {

  'use strict';

  var module = angular.module('kibana.panels.topichistogram', []);
  app.useModule(module);

  module.controller('topichistogram', function ($scope, $http, querySrv, dashboard, filterSrv) {
    $scope.topic = {'id': 0, 'name': 'topic 0'};
    $scope.topics = [$scope.topic];
    $http.get('http://localhost:8080/bugparty/' + dashboard.indices[0] + '/topics')
      .success(function(data, status, headers, config) {
          $scope.topics = data;
          $scope.topic = $scope.topics[0];
      });

    $scope.panelMeta = {
      modals: [{
        description: "Inspect",
        icon: "icon-info-sign",
        partial: "app/partials/inspector.html",
        show: $scope.panel.spyable
      }],
      editorTabs: [{
        title: 'Style',
        src: 'app/panels/topichistogram/styleEditor.html'
      }, {
        title: 'Queries',
        src: 'app/panels/topichistogram/queriesEditor.html'
      }, ],
      status: "Custom and Buggy",
      description: "A bucketed score chart of the current query or queries. Uses the " +
        "Elasticsearch topichistogram facet."
    };

    $scope.$watch('topic.id', function() {
      if (_.isUndefined($scope.topic)) { return; }

      if (!_.isUndefined($scope.panel.zoom_filter)) {
        filterSrv.remove($scope.panel.zoom_filter);
        $scope.panel.zoom_filter = undefined;
      }
      $scope.$emit('refresh');
    });


    // Set and populate defaults
    var _d = {
      /** @scratch /panels/histogram/3
       * x-axis:: Show the x-axis
       */
      'x-axis': true,
      /** @scratch /panels/histogram/3
       * y-axis:: Show the y-axis
       */
      'y-axis': true,
      /** @scratch /panels/histogram/3
       * scale:: Scale the y-axis by this factor
       */
      grid: {
        max: 250,
        min: 0
      },
      /** @scratch /panels/histogram/5
       *
       * ==== Queries
       * queries object:: This object describes the queries to use on this panel.
       * queries.mode::: Of the queries available, which to use. Options: +all, pinned, unpinned, selected+
       * queries.ids::: In +selected+ mode, which query ids are selected.
       */
      queries: {
        mode: 'all',
        ids: []
      },
      /** @scratch /panels/histogram/3
       * interval:: range of each bucket
       */
      interval: '10',
      /** @scratch /panels/histogram/3
       * ==== Drawing options
       * lines:: Show line chart
       */
      lines: false,
      /** @scratch /panels/histogram/3
       * fill:: Area fill factor for line charts, 1-10
       */
      fill: 0,
      /** @scratch /panels/histogram/3
       * linewidth:: Weight of lines in pixels
       */
      linewidth: 3,
      /** @scratch /panels/histogram/3
       * points:: Show points on chart
       */
      points: false,
      /** @scratch /panels/histogram/3
       * pointradius:: Size of points in pixels
       */
      pointradius: 5,
      /** @scratch /panels/histogram/3
       * bars:: Show bars on chart
       */
      bars: true,
      /** @scratch /panels/histogram/3
       * stack:: Stack multiple series
       */
      stack: true,
      /** @scratch /panels/histogram/3
       * spyable:: Show inspect icon
       */
      spyable: true,
      /** @scratch /panels/histogram/3
       * zoomlinks:: Show `Zoom Out' link
       */
      zoomlinks: true,
      /** @scratch /panels/histogram/3
       * options:: Show quick view options section
       */
      options: true,
      /** @scratch /panels/histogram/3
       * legend:: Display the legond
       */
      legend: true,
      /** @scratch /panels/histogram/3
       * show_query:: If no alias is set, should the query be displayed?
       */
      show_query: true,
      /** @scratch /panels/histogram/3
       * interactive:: Enable click-and-drag to zoom functionality
       */
      interactive: true,
      /** @scratch /panels/histogram/3
       * legend_counts:: Show counts in legend
       */
      legend_counts: true,
      /** @scratch /panels/histogram/3
       * ==== Transformations
       * timezone:: Correct for browser timezone?. Valid values: browser, utc
       */
      timezone: 'browser', // browser or utc
      /** @scratch /panels/histogram/3
       * percentage:: Show the y-axis as a percentage of the axis total. Only makes sense for multiple
       * queries
       */
      percentage: false,
      /** @scratch /panels/histogram/3
       * zerofill:: Improves the accuracy of line charts at a small performance cost.
       */
      tooltip: {
        value_type: 'cumulative',
        query_as_alias: true
      }
    };

    _.defaults($scope.panel, _d);
    _.defaults($scope.panel.tooltip, _d.tooltip);
    _.defaults($scope.panel.grid, _d.grid);

    $scope.init = function () {
      // Hide view options by default
      $scope.options = false;

      // Always show the query if an alias isn't set. Users can set an alias if the query is too
      // long
      $scope.panel.tooltip.query_as_alias = true;
      $scope.get_data();
    };

    $scope.range = {
      from: null,
      to: null
    };
    $scope.get_interval = function () {
      return parseFloat($scope.panel.interval);
    };

    $scope.get_field = function () {
      return 'topics.' + ($scope.topic.id || 0);
    };

    /**
     * Fetch the data for a chunk of a queries results. Multiple segments occur when several indicies
     * need to be consulted (like timestamped logstash indicies)
     *
     * The results of this function are stored on the scope's data property. This property will be an
     * array of objects with the properties info, time_series, and hits. These objects are used in the
     * render_panel function to create the historgram.
     *
     * @param {number} segment   The segment count, (0 based)
     * @param {number} query_id  The id of the query, generated on the first run and passed back when
     *              this call is made recursively for more segments
     */
    $scope.get_data = function (data, segment, query_id) {
      var request, queries, results;

      if (_.isUndefined(segment)) {
        segment = 0;
      }
      delete $scope.panel.error;

      // Make sure we have everything for the request to complete
      if (dashboard.indices.length === 0) {
        return;
      }

      $scope.panelMeta.loading = true;
      request = $scope.ejs.Request().indices(dashboard.indices[segment]);
      request.searchType("count");

      $scope.panel.queries.ids = querySrv.idsByMode($scope.panel.queries);

      queries = querySrv.getQueryObjs($scope.panel.queries.ids);

      // Build the query
      _.each(queries, function (q) {
        var query = $scope.ejs.FilteredQuery(
          querySrv.toEjsObj(q),
          filterSrv.getBoolFilter(filterSrv.ids())
        );

        var facet = $scope.ejs.HistogramFacet(q.id)
                      .field($scope.get_field())
                      .global(true)
                      .interval($scope.get_interval())
                      .facetFilter($scope.ejs.QueryFilter(query));
        request = request.facet(facet);
      });

      // Populate the inspector panel
      $scope.populate_modal(request);

      // Then run it
      results = request.doSearch();

      // Populate scope when we have results
      return results.then(function (results) {
        $scope.panelMeta.loading = false;
        if (segment === 0) {
          $scope.legend = [];
          $scope.hits = 0;
          data = [];
          query_id = $scope.query_id = new Date().getTime();
        }

        // Check for error and abort if found
        if (!(_.isUndefined(results.error))) {
          $scope.panel.error = $scope.parse_error(results.error);
        }
        // Make sure we're still on the same query/queries
        else if ($scope.query_id === query_id) {

          var hits,
            counters; // Stores the bucketed hit counts.

          _.each(queries, function (q, i) {
            var query_results = results.facets[q.id];
            // we need to initialize the data variable on the first run,
            // and when we are working on the first segment of the data.
            if (_.isUndefined(data[i]) || segment === 0) {
              hits = 0;
              counters = {};
            } else {
              hits = data[i].hits;
              counters = data[i].counters;
            }

            // increment counters
            _.each(query_results.entries, function (entry) {
              hits += entry.count; // The series level hits counter
              $scope.hits += entry.count; // Entire dataset level hits counter
              counters[entry.key] = (counters[entry.key] || 0) + entry.count;
            });

            $scope.legend[i] = {
              query: q,
              hits: hits
            };

            data[i] = {
              info: q,
              hits: hits,
              counters: counters
            };

          });

        }

        // Tell the histogram directive to render.
        $scope.$emit('render', data);

        // If we still have segments left, get them
        if (segment < dashboard.indices.length - 1) {
          $scope.get_data(data, segment + 1, query_id);
        }
      });
    };

    $scope.zoom_to = function (from, to) {
      var interval = $scope.get_interval();
      $scope.range.from = Math.round(from / interval) * interval;
      $scope.range.to = Math.round(to / interval) * interval;

      $scope.panel.zoom_filter = filterSrv.set({
        type: 'range',
        from: $scope.range.from,
        to: $scope.range.to,
        field: $scope.get_field()
      }, $scope.panel.zoom_filter);
    };

    // function $scope.zoom
    // factor :: Zoom factor, so 0.5 = cuts timespan in half, 2 doubles timespan
    //
    $scope.zoom = function (factor) {
      if ($scope.range.to == null || $scope.range.from == null) {
        return;
      }
      var span = $scope.range.to - $scope.range.from,
        center = $scope.range.from + span / 2;

      var to = (center + (span * factor) / 2);
      var from = (center - (span * factor) / 2);

      $scope.zoom_to(from, to);
    };

    $scope.populate_modal = function (request) {
      $scope.inspector = angular.toJson(JSON.parse(request.toString()), true);
    };

    $scope.set_refresh = function (state) {
      $scope.refresh = state;
    };

    $scope.close_edit = function () {
      if ($scope.refresh) {
        $scope.get_data();
      }
      $scope.refresh = false;
      $scope.$emit('render');
    };

    $scope.render = function () {
      $scope.$emit('render');
    };

  });

  module.directive('topicHistogramChart', function (dashboard, filterSrv) {
    return {
      restrict: 'A',
      template: '<div></div>',
      link: function (scope, elem) {
        var data, plot;

        scope.$on('refresh', function () {
          scope.get_data();
        });

        // Receive render events
        scope.$on('render', function (event, d) {
          data = d || data;
          render_panel(data);
        });

        // Function for rendering panel
        function render_panel(data) {
          // IE doesn't work without this
          try {
            elem.css({
              height: scope.panel.height || scope.row.height
            });
          } catch (e) {
            console.log(e);
            return;
          }

          // Populate from the query service
          try {
            _.each(data, function (series) {
              series.label = series.info.alias;
              series.color = series.info.color;
            });
          } catch (e) {
            return;
          }

          var stack = scope.panel.stack ? true : null;

          // Populate element
          try {
            var options = {
              legend: {
                show: false
              },
              series: {
                stackpercent: scope.panel.stack ? scope.panel.percentage : false,
                stack: scope.panel.percentage ? null : stack,
                lines: {
                  show: scope.panel.lines,
                  // Silly, but fixes bug in stacked percentages
                  fill: scope.panel.fill === 0 ? 0.001 : scope.panel.fill / 10,
                  lineWidth: scope.panel.linewidth,
                  steps: false
                },
                bars: {
                  show: scope.panel.bars,
                  fill: 1,
                  barWidth: scope.get_interval(),
                  zero: false,
                  lineWidth: 0
                },
                points: {
                  show: scope.panel.points,
                  fill: 1,
                  fillColor: false,
                  radius: scope.panel.pointradius
                },
                shadowSize: 1
              },
              yaxis: {
                mode: null,
                show: scope.panel['y-axis'],
                min: scope.panel.grid.min,
                max: scope.panel.percentage && scope.panel.stack ? 100 : scope.panel.grid.max
              },
              xaxis: {
                mode: null,
                show: scope.panel['x-axis'],
                label: "Topic Scores",
                minTickSize: scope.get_interval() || 10
              },
              grid: {
                backgroundColor: null,
                borderWidth: 0,
                hoverable: true,
                color: '#c8c8c8'
              }
            };

            if (scope.panel.interactive) {
              options.selection = {
                mode: "x",
                color: '#666'
              };
            }

            // when rendering stacked bars, we need to ensure each point that has data is zero-filled
            // so that the stacking happens in the proper order

            // start by getting all of the buckets that will be used
            var bucket_values = [];
            if (data.length > 0) {
              // flatmap bucket keys as floats
              bucket_values = _.chain(data)
                              .map(function(query) {
                                  return _.keys(query.counters);
                              })
                              .flatten()
                              .map(parseFloat)
                              .sort(function (a, b) {
                                // decending numeric sort
                                return a - b;
                              })
                              .uniq(true)
                              .value();
            }


            // now for each counter, make data[i].data be the
            // list of counts for all buckets that we are plotting
            for (var i = 0; i < data.length; i++) {
              data[i].data =
                _.map(bucket_values, function (b) {
                  return [b, parseFloat(data[i].counters[b]) || 0];
                });
            }

            plot = $.plot(elem, data, options);
          } catch (e) {
            // Nothing to do here
            console.log(e);
          }
        }

        elem.bind("plotselected", function (event, ranges) {
          scope.zoom_to(ranges.xaxis.from, ranges.xaxis.to);
        });
      }
    };
  });

});
