/*

  ## ProjectPicker

  ### Parameters
*/
define([
  'angular',
  'app',
  'lodash',
  'moment',
  'kbn'
],
function (angular, app, _, moment, kbn) {
  'use strict';

  var module = angular.module('kibana.panels.projectpicker', []);
  app.useModule(module);

  module.controller('projectpicker', function($scope, $modal, $q, filterSrv, bugpartySrv, dashboard) {
    $scope.panelMeta = {
      status  : "Amazing",
      description : "A panel for picking bugparty projects."
    };


    // Set and populate defaults
    var _d = {
      status        : "Amazing",
      projects      : [],
      current       : dashboard.indices[0],
      dashboard     : dashboard
    };
    _.defaults($scope.panel,_d);

    $scope.filterSrv = filterSrv;
    $scope.$on('refresh', function(){$scope.init();});

    $scope.init = function() {
      bugpartySrv.get_projects()
        .success(function(projects) {$scope.projects = projects;});
    };

    $scope.selectProject = function(project) {
      $scope.current = project.name;
      dashboard.set_index(project.name);
    };

    $scope.$watch('dashboard.indices[0]', function() {
      $scope.current = dashboard.indices[0];
    });

  });
});
