define([
  'angular',
  'lodash'
],
function (angular, _) {
  'use strict';

  var module = angular.module('kibana.services');

  var ClusterReport = function(deferred, cluster_count, service) {
    this.cluster_count = cluster_count;
    this.base_url = service.base_url;
    this.deferred = deferred;
    this._interval = null;
    this.service = service;
    this.$http = service.$http;
    this.$scope = service.$rootScope;
  };

  _.extend(ClusterReport.prototype, {
    get_url: function(member_name) {
      return this.base_url + this[member_name];
    }

    ,update: function(data, status) {
      _.extend(this, data);
      if (this.status === 'success') {
        this.deferred.resolve(this);
        this.cancel_polling();
      } else if (this.status === 'failed') {
        this.error();
        this.cancel_polling();
        return;
      }

      var self = this;
      if (this._interval === null) {
        this._interval = window.setInterval(function() {
          self.$scope.$apply(function() {
            self.$http.get(self.get_url('location'))
              .success(function(data, status) { self.update(data); })
              .error(function() { self.error(); });
          });
        }, 1000);
      }
    }
    ,error: function() {
        this.status = 'failure';
        this.deferred.reject(this);
    }
    ,cancel_polling: function() {
        window.clearInterval(this._interval);
        this._interval = undefined;
    }
  });

  module.service('bugpartySrv', function($http, $q, $interpolate, $rootScope) {
    this.base_url = 'http://localhost:8080';
    this.project_url = $interpolate(this.base_url + '/bugparty/{{name}}');
    this.cluster_url = $interpolate(this.base_url + '/bugparty/{{name}}/reports/cluster');
    this.$http = $http;
    this.$rootScope = $rootScope;

    _.extend(this, {
      get_topics: function(projectName) {
        return $http.get(this.base_url + projectName + '/topics');
      }

      ,get_projects: function() {
        return $http.get(this.base_url + '/bugparty/projects');
      }

      ,do_clustering: function(projectName, cluster_count, ids) {
        var req = {'bug_ids': ids, 'clusters': cluster_count}
            ,report = new ClusterReport($q.defer(), cluster_count, this)
        ;
        $http.post(this.cluster_url({name: projectName}), req)
          .success(function(data, status) { report.update(data, status); })
          .error(function(data) { report.error(); })
        ;

        return report;
      }
    });

  });
});
