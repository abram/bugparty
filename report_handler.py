#  Copyright (C) 2014 Alex Wilson
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import base64, json, os, urls

from databases import ElasticSearch
from common import *
from cluster import ClusterTask
from http import *

import urls

base_route = '/bugparty/$project/reports/'
router = SimpleRouter()
HandlerControllerBase.static_routes(router, {
    '/bugparty/$project/reports.html': 'reports/index.html'
})
class ReportHandler(HandlerControllerBase):
    def __init__(self, request, task_queue):
        super(ReportHandler, self).__init__(request, task_queue)

    def route_resolved(self, view_func, project, report_id=None, **kwargs):
        self.project = project
        self.report_id = report_id

    @router.add_route(base_route)
    def list_reports(self, project):
        reports = ElasticSearch(self.project) \
                    .get_all_docs(ElasticSearch.GENERATED)
        reports = [self.make_report_doc(report) for report in reports.values()]

        response = json_response(reports) 
        timestamps = filter(None, (parse_date_str(report['when'])
                                        for report in reports))
        if len(timestamps) > 0:
            response.last_modified(max(timestamps))
        return response

    @router.add_route(base_route + 'cluster', ['POST'])
    def start_clustering(self, project):
        data = json.loads(self.request.body)
        task = ClusterTask(project, int(data.get('clusters', 10)),
                                    data.get('bug_ids'))
        self.task_queue.add_task(task)

        initial_doc = task.get_initial_doc()
        client = ElasticSearch(self.project).connect_to_generated_db()
        client[initial_doc['id']] = initial_doc
        cluster_doc = self.make_cluster_doc(initial_doc)
        return json_response(cluster_doc, 202)

    @router.add_route(base_route + 'cluster/$report_id', ['GET'])
    def show_cluster(self, project, report_id):
        client = ElasticSearch(self.project).connect_to_generated_db()
        try:
            return self.make_cluster_doc(client[report_id], report_id)
        except ElasticSearch.Error404:
            self.abort(404, 'report does not exist')

    def make_report_doc(self, doc):
        generator = doc['generator']

        if generator == 'lda-cluster':
            return self.make_cluster_doc(doc)
        elif generator == 'lda-run':
            return self.make_lda_doc(doc)

    def make_lda_doc(self, doc):
        doc_id = doc.get('id', doc.get('doc_id', doc.get('_id')))
        data = {
            'id': doc_id
            ,'when': doc.get('when')
            ,'generator': doc['generator']
            ,'topics': doc['topics']
            ,'location': urls.lda_report(self.project, doc_id)
        }

        # add the files
        for key in doc.keys():
            if '.' in key:
                data[key] = urls.lda_report_file(self.project, doc_id, key)

        return data
        
    def make_cluster_doc(self, doc, report_id = None):
        doc_id = report_id or str(doc.get('id', doc.get('_id')))
        return {
            'id': doc_id
            ,'when': doc.get('when')
            ,'generator': doc['generator']
            ,'clusters': doc['clusters']
            ,'status': doc['status']
            ,'location': urls.cluster_report(self.project, doc_id)
            ,'cluster.csv': urls.cluster_csv(self.project, doc_id)
            ,'silhouette.svg': urls.cluster_svg(self.project, doc_id)
        }

    @router.add_route(base_route + 'cluster/$report_id/cluster.csv')
    def show_cluster_csv(self, project, report_id):
        csv, when = self.get_doc_field_or_die('lda-cluster', 'cluster.csv')

        if self.should_304(modified=when):
            return respond_with_304(modified=when)
        return csv_response(csv).last_modified(when)

    @router.add_route(base_route + 'cluster/$report_id/silhouette.svg')
    def show_cluster_svg(self, project, report_id):
        svg, when = self.get_doc_field_or_die('lda-cluster', 'silhouette.svg')

        if self.should_304(modified=when):
            return respond_with_304(modified=when)
        return svg_response(svg).last_modified(when)

    @router.add_route(base_route + 'lda/$report_id/$document_name')
    def show_lda_file(self, project, report_id, document_name):
        return self.serve_doc_field('lda-run', document_name)

    def serve_doc_field(self, generator, field):
        data, when = self.get_doc_field_or_die(generator, field)

        if self.should_304(modified=when):
            return respond_with_304(modified=when)

        def serve_tarball(data, **kw):
            return tar_gz_response(base64.standard_b64decode(data), **kw)

        response_func = {
            '.svg': svg_response
            ,'.csv': csv_response
            ,'.txt': text_response
            ,'.gz': serve_tarball # .tar.gz
        }.get(os.path.splitext(field)[1], base64_response)

        return response_func(data).last_modified(when)

    def get_doc_field_or_die(self, generator, field):
        db = ElasticSearch(self.project).connect_to_generated_db()
        report = db.get(self.report_id)
        if not report or field not in report \
                or report.get('generator') != generator:
            self.abort(404, 'report does not exist')

        return report[field], parse_date_str(report['when'])

ReportHandler.router = router
