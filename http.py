#  Copyright (C) 2014 Alex Wilson
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import datetime, json, os
from dateutil.tz import tzutc
from requests.structures import CaseInsensitiveDict

import common


def json_response(data, code=200):
    return HTTPResponse(code, json.dumps(data, indent=2)) \
                .mime_type('application/json')

def svg_response(data, code=200):
    return HTTPResponse(code, data) \
            .mime_type('image/svg+xml')

def csv_response(data, code=200):
    return HTTPResponse(code, data) \
            .mime_type('text/csv')

def text_response(data, code=200):
    return HTTPResponse(code, data) \
            .mime_type('text/plain')

def tar_gz_response(data, code=200):
    '''data should be binary tarball data'''
    return HTTPResponse(code, data) \
            .mime_type('application/x-gzip-compressed')

def base64_response(data, code=200):
    return HTTPResponse(code, data) \
            .mime_type('application/octet-stream') \
            .transfer_encoding('base64')

def respond_with_304(modified=None, etag=None):
    return HTTPResponse(304, '').last_modified(modified)

STATIC_FOLDER = os.path.join(os.getcwd(), 'static')

def serve_static(handler, filename):
    filename = os.path.join(STATIC_FOLDER, filename)
    mtime = common.file_last_modified(filename)

    response = None
    if handler.should_304(modified=mtime):
        response = respond_with_304(modified=mtime)
    else:
        response = HTTPResponse(200, open(filename).read())

    return response \
            .mime_type('text/html') \
            .last_modified(mtime)

def serve_static_for_endpoint(filename):
    '''useful for getting around lexical scoping issues :( '''
    def load(handler, *args, **kwargs):
        return serve_static(handler, filename)
    fullpath = os.path.join(STATIC_FOLDER, filename)
    load.__doc__ = 'serve {}'.format(fullpath)
    return load


class HTTPError(BaseException):
    def __init__(self, code, message):
        self.code = code
        self.message = message

        super(HTTPError, self).__init__(self.message)

    def as_response(self):
        '''turns this HTTPError into an HTTPResponse'''
        return HTTPResponse(self.code, '{}: {}'.format(self.code, self.message))


def should_304(request, etag=None, modified=None):
    '''determine whether, based on client request headers and our internal
        data, we should respond with a 304 (content not modified)'''
    req_headers = request.headers
    if modified and 'if-modified-since' in req_headers:
        last_seen = common.parse_date_str(req_headers['if-modified-since'])
        # ignore microseconds, since they are not supported by http dates
        if last_seen >= modified.replace(microsecond=0):
            return True
    if etag and 'if-none-match' in req_headers:
        last_seen = req_headers['if-none-match']
        if last_seen == etag:
            return True
    return False


class HTTPResponse(object):
    '''An HTTP response that can be sent back to mongrel.

       Various methods can be used in method chaining style for setting
       http headers.

       .. code-block::python

            >> HTTPResponse(200, my_response_body) \
            ..      .mime_type('text/html') \
            ..      .last_modified(my_datetime_timestamp)
    '''
    CORS_HEADERS = CaseInsensitiveDict({
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': \
            'Origin, x-requested-with, content-type, accept'
    })

    def __init__(self, code, body):
        self.code = code
        self.body = body
        self.content_type = "text/html"
        self.headers = HTTPResponse.CORS_HEADERS.copy()

    def reply(self, connection, request):
        '''send a mongrel response'''
        response = self.body
        if type(self.body) is unicode:
            response = self.body.encode('utf-8')
        connection.reply_http(request, response,
		    headers=self.headers, code=self.code,
           	status=HTTPResponse.CODE_STATUS.get(self.code, 'OK'))

    def mime_type(self, mime):
        '''sets the Content-Type HTTP header.

        :returns: self
        '''
        self.headers['Content-Type'] = mime
        return self

    def transfer_encoding(self, mime):
        '''set the Content-Transfer-Encoding HTTP header.

        :returns: self
        '''
        self.headers['Content-Transfer-Encoding'] = mime
        return self

    def last_modified(self, modified):
        '''set the Last-Modified HTTP header.

        :returns: self
        '''
        if modified:
            self.headers['Last-Modified'] = self.http_date_format(modified)
        elif 'Last-Modified' in self.headers:
            del self.headers['Last-Modified']
        return self

    def etag(self, etag):
        '''sets the etag HTTP header.

        :returns: self
        '''
        if etag:
            self.headers['etag'] = '"{}"'.format(etag)
        elif 'etag' in self.headers:
            del self.headers['etag']
        return self

    def allow_methods(self, methods):
        '''sets the Access-Control-Allow-Methods HTTP header

        :returns: self
        '''
        methods = methods or []
        self.headers['Access-Control-Allow-Methods'] = ', '.join(methods)
        return self

    def expires_in(self, **kwargs):
        '''Set the Expires HTTP header.
        takes same args as datetime.timedelta

        :returns: self
        '''
        expiry = datetime.datetime.utcnow() + datetime.timedelta(**kwargs)
        self.headers['Expires'] = self.http_date_format(expiry)
        return self

    def http_date_format(self, when):
        '''format an http date like the RFC'''
        if type(when) is str:
            when = common.parse_date_str(when)
        return when.astimezone(tzutc()) \
                .strftime('%a, %d %b %Y %H:%M:%S GMT')

    CODE_STATUS = {
        200: 'OK'
        ,201: 'CREATED'
        ,202: 'ACCEPTED'
        ,400: 'BAD REQUEST'
        ,404: 'NOT FOUND'
        ,409: 'CONFLICT'
        ,503: 'INTERNAL SERVER ERROR'
        ,500: 'INTERNAL SERVER ERROR'
    }

    @staticmethod
    def wrap(data):
        if type(data) is str:
            return HTTPResponse(200, data)
        elif isinstance(data, HTTPResponse):
            return data
        elif isinstance(data, HTTPError):
            return data.as_response()
        else:
            return HTTPResponse(200, json.dumps(data))

# Source: https://github.com/j2labs/brubeck/blob/master/brubeck/request.py
# Copyright 2012 J2 Labs LLC. All rights reserved.
class HttpMultipartRequest(object):
    def __init__(self, headers, body):
        self.headers = headers
        self.body = body.decode('utf-8')
        self.data = {}

    def parse_body(self):
        boundary = self.get_value_from_content_type(
            self.headers['content-type'], 'boundary')
        if self.body.endswith('\r\n'):
            footer_length = len(boundary) + 6
        else:
            footer_length = len(boundary) + 4
        parts = self.body[:-footer_length].split('--' + boundary + '\r\n')

        for part in parts:
            if not part:
                continue
            end_of_headers = part.find('\r\n\r\n')
            headers = self.parse_headers(part, end_of_headers)

            content_disposition = headers['Content-Disposition']
            name = self.get_value_from_content_type(content_disposition, 'name')

            payload = part[end_of_headers + 4:-2]

            self.data[str(name)] = {'headers': headers, 'payload': payload}


    def get_value_from_content_type (self, content_type, desired_key):
        fields = content_type.split(';')
        for field in fields:
            key, sep, value = field.strip().partition('=')
            if key == desired_key and value:
                return value.strip("\"")
        return ''

    def parse_headers(self, part, end_of_headers):
        header_string = part[:end_of_headers].decode('utf-8')
        headers = {}

        for line in header_string.splitlines():
            if line[0].isspace():
                # continuation of a multi-line header
                new_part = ' ' + line.lstrip()
                headers[last_key] += new_part
            else:
                name, value = line.split(':', 1)
                headers[name] = value.strip()
        return headers
